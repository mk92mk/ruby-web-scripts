#7ec52dc8c0e8c49e7cf3e6588dae7772cdec4c078b1d8b0807bc3517fb522145 for Build Base
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'

class BuildBase
  
  def get_stores
    allStoresIds = []
    stores_url = "http://www.buildbase.co.uk/storefinder"
    mechanize = Mechanize.new
    stores_page = mechanize.get(stores_url)
    puts "GOT STORES PAGE BUILD BASE"
    stores_script_txt = stores_page.search("//script[contains(text(), 'storeLocator.initialize(')]")[0].text
    stores_jsons = stores_script_txt.split("storeLocator.initialize( $.parseJSON('{\\\"all\\\":")[-1].split(",\\\"selected\\\":0}') ,\tfalse);\n        });")[0]
    stores_jsons = stores_jsons.gsub('\"', '"').gsub(/<\/?[^>]*>/, '').gsub(/\n\n+/, "\n").gsub(/^\n|\n$/, '')
    stores_list = JSON.parse(stores_jsons)
    stores_list.each do |store_item|
      store_name = store_item["LocationName"]
      store_address = store_item["Address1"]
      store_city = store_item["LocationName"]
      store_zipcode = store_item["postcode"]
      store_phone = store_item["Phone"]
      store_latitude = store_item["Lat"]
      store_longitude = store_item["Lng"]
      store_times = store_item["SmallTimes"]
      puts "ON #{store_address} NOW"
      opening_hours = []
      opening_hours = parse_hours(store_times) if store_times
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_url}"
      puts "******"
      allStoresIds << store.id
    end
    allStoresIds
  end
  
  def parse_hours(store_times)
    week_hours = []
    store_times.each do |store_time|
      opening_days = store_time["DayOfWeek"].downcase
      opening_time = store_time["OpeningAt"].downcase
      closing_time = store_time["ClosingAt"].downcase
      puts "GOT #{opening_days} AND TIMES ARE #{opening_time} TO #{closing_time}"
      days = opening_days.split("-")
      days = opening_days.split("–") if opening_days.include?("–")
      days = opening_days.split("to") if opening_days.include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_am_pm = get_am_pm(opening_time) if !(opening_time.include?("closed")  || !opening_time.match(/\d+/))
      close_am_pm = get_am_pm(closing_time) if !(closing_time.include?("closed")  || !closing_time.match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{opening_time}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{opening_time}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{closing_time}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{closing_time}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if opening_time.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{opening_time}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{opening_time}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{closing_time}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{closing_time}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if opening_time.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{opening_time}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{opening_time}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{closing_time}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{closing_time}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if opening_time.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end

  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    leaflets_list_page = mechanize.get("http://www.buildbase.co.uk/brochures")
    leaflet_form_values = leaflets_list_page.search("//li[contains(@class,'brochureItem')]//input/@value")
    form_data = {}
    leaflet_form_values.each_with_index do |form_value, index|
      leaflet_form_values.each_with_index do |form_value, index|
        form_data["brochures[#{index}][name]"]="brochures[]"
        form_data["brochures[#{index}][value]"]=form_value.value
        form_data["deliverymethods[#{index}][name]"]="download[]"
        form_data["deliverymethods[#{index}][value]"]=form_value.value
      end
      form_data["personaldetails[0][name]"]="fname"
      form_data["personaldetails[0][value]"]="dk"
      form_data["personaldetails[1][name]"]="lname"
      form_data["personaldetails[1][value]"]="mk"
      form_data["personaldetails[9][name]"]="source"
      form_data["personaldetails[9][value]"]="TV+Advert"
    end
    sleep(1)
    mechanize = Mechanize.new
    leaflets_pdf_page = mechanize.post("http://www.buildbase.co.uk/brochures/processrequest/", form_data)
    pdfs_list_page = JSON.parse(leaflets_pdf_page.body)
    pdfs_list = pdfs_list_page["pdf"]
    pdfs_list.each do |leaflet_pdf|
      leaflet_pdf_url = ""
      leaflet_sect = Mechanize::Page.new(nil,{'content-type'=>'text/html'},leaflet_pdf[-1],nil,mechanize)
      leaflet_name = leaflet_sect.search("//a/@title")[0].value
      leaflet_pdf_url = leaflet_sect.search("//a/@href")[0].value
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.url = leaflet_pdf_url
        leaflet.name = leaflet_name
        leaflet.store_ids = store_ids
      end
      # leaflet.save
      puts "*******************LEAFLET"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      allStoresLeafletsIds << leaflet.id
    end
    allStoresLeafletsIds
  end

  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '7ec52dc8c0e8c49e7cf3e6588dae7772cdec4c078b1d8b0807bc3517fb522145'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end
end