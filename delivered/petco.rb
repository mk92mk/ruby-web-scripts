#b6088cbbc39f77bea0732f9249d1c854151fa951b26333fa10972081cebfe2f0 for PETCOs
require 'pqsdk'
require 'mechanize'
require 'uri'

class Petco
  
  def initialize
    @report = Logger.new("Petco#{Time.now.to_s}.log")
  end
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    states_page = mechanize.get(states_url)
    puts "GOT STORES STATES PAGE #{states_url}"
    @report.info "GOT STORES STATES PAGE #{states_url}"
    if states_page.uri.to_s.start_with?("http://anchorfree.us/rdr.php")
      states_url = URI.extract(states_page.body)[0].chomp("');")
      states_page = mechanize.get(states_url)
    end
    states_hrefs = states_page.search("//div[@class='rio-listItem']//a/@href")
    states_hrefs.each do |state_href|
#      mechanize = Mechanize.new
      sleep(2)
      cities_page = mechanize.get(state_href.value)
      puts "GOT STATES LIST PAGE #{state_href.value}"
      @report.info "GOT STATES LIST PAGE #{state_href.value}"
      cities_hrefs = cities_page.search("//div[@class='rio-listItem']//a/@href")
      cities_hrefs.each do |city_href|
#        mechanize = Mechanize.new
        sleep(2)
        stores_page = mechanize.get(city_href.value)
        puts "GOT STORES_LIST PAGE #{city_href.value}"
        @report.info "GOT STORES_LIST PAGE #{city_href.value}"
        stores_details_hrefs = stores_page.search("//a[@class='gaq-link'][contains(@data-gaq, 
                                                    'List, View Details')]/@href")
        stores_details_hrefs.each do |store_href|
#          mechanize = Mechanize.new
          sleep(4)
          store_details_page = mechanize.get(store_href.value)
          puts "GOT STORE DETAILS PAGE #{store_href.value}"
          @report.info "GOT STORE DETAILS PAGE #{store_href.value}"
          store_info_div = store_details_page.search("//div[@id='rio-col-2']")
          store_name = store_details_page.search("//span[@id='last-crumb']//span[@itemprop='name']")[0]
                                                                                         .text.strip
          store_address = store_info_div.search(".//span[@itemprop='streetAddress']")[0].text.strip
          store_city = store_info_div.search(".//span[@itemprop='addressLocality']")[0].text.strip
          store_zipcode = store_info_div.search(".//span[@itemprop='postalCode']")[0].text.strip
          store_phone = store_info_div.search(".//span[@itemprop='telephone']")[0].text.strip
          store_latitude_node = store_details_page.search("//div[@itemprop='geo']//meta[@itemprop='latitude']
                                                  /@content")[0]
          if store_latitude_node
            store_latitude = store_latitude_node.value
          else
            store_latitude = store_details_page.search("//span[@itemprop='geo']
                                                      //meta[@itemprop='latitude']/@content")[0].value
          end
          store_longitude_node = store_details_page.search("//div[@itemprop='geo']
                                                        //meta[@itemprop='longitude']/@content")[0]
          if store_longitude_node
            store_longitude = store_longitude_node.value
          else
            store_longitude = store_details_page.search("//span[@itemprop='geo']
                                                      //meta[@itemprop='longitude']/@content")[0].value
          end
          div_hours_rows = store_info_div.search(".//div[@class='day-hour-row']")
          opening_hours = parse_hours(div_hours_rows)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude
            store.longitude = store_longitude
            store.phone = store_phone
            store.origin = store_href.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}"
          puts  "STORE LATITUDE #{store_latitude}"
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{store_href.value}"
          @report.info  "*************STORE*********************"
          @report.info  "STORE NAME IS #{store_name}"
          @report.info  "STORE CITY IS #{store_city}"
          @report.info  "STORE ZIP CODE IS #{store_zipcode}"
          @report.info  "STORE ADDRESS IS #{store_address}"
          @report.info  "STORE LONGITUDE #{store_longitude}"
          @report.info  "STORE LATITUDE #{store_latitude}"
          @report.info  "STORE PHONE #{store_phone}"
          @report.info  "STORE HOURS #{opening_hours}"
          @report.info  "STORE URL #{store_href.value}"
          allStoresIds << store.id
          leaflet_url = store_details_page.search("//a[@data-gaq='Conversion, Local Ad']/@href")[0].value
          mechanize = Mechanize.new
          mechanize.redirect_ok = false
          sleep(3)
          leaflet_page = mechanize.get(leaflet_url)
          page_code = leaflet_page.code
          leaflet_page = get_redirected_page(leaflet_page, page_code, mechanize)
          store_leaflet_url = leaflet_page.uri.to_s
#          store_leaflet_id =  leaflet_page.uri.to_s.scan(/storeid=\d+&/)[0].scan(/\d+/).first

          store_leaflets = get_leaflets(store_zipcode, store_leaflet_url, store.id)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds 
  end
  
  def get_leaflets(store_leaflet_id, store_leaflet_url, store_id)  
    leaflets = []
    leaflet_images_url = "http://api2.shoplocal.com/retail/d2e91bc0e23ae47a/2013.1/json/PromotionPages?citystatezip=#{store_leaflet_id}&promotioncode=Petco-160327&preferredlanguageid=1&callback=shoplocal.apiCallBacks[%22-1016710278%22].processCallbacks"
    leaflet_dates_url = "http://api2.shoplocal.com/retail/d2e91bc0e23ae47a/2013.1/json/PromotionPageHotSpots?citystatezip=#{store_leaflet_id}&promotioncode=Petco-160327&promotionpageid=5258178&preferredlanguageid=1&callback=shoplocal.apiCallBacks%5B%221993886023%22%5D.processCallbacks"
    leaflet_dates_url = "http://api2.shoplocal.com/retail/d2e91bc0e23ae47a/2013.1/json/Promotions?citystatezip=#{store_leaflet_id}&preferredlanguageid=1&promotionsortmode=3&promotioncount=999&callback=shoplocal.apiCallBacks[%221656805061%22].processCallbacks"

    mechanize = Mechanize.new
    sleep(2)
    leaflet_images_page = mechanize.get(leaflet_images_url)
    images_urls = URI.extract(leaflet_images_page.body)
    if images_urls.empty?
      puts "NO LEAFLETS FOUND ON #{store_leaflet_url}"
      @report.info "NO LEAFLETS FOUND ON #{store_leaflet_url}"
      return leaflets
    end
    mechanize = Mechanize.new
    sleep(3)
    leaflet_dates_page = mechanize.get(leaflet_dates_url)
    store_leaflet_name = leaflet_dates_page.body.scan(/\"Identifier\":\"[A-Za-z]+\s[A-Za-z]+/)
                         .first.scan(/:\"[A-Za-z]+\s[A-Za-z]+/).first.gsub('"', '').gsub(':','')
    start_date_string = leaflet_dates_page.body.scan(/\"SaleStartDateString\":\"\d+\/\d+\/\d+ \d+:\d+:\d+\s[A-Za-z]+/).first
    leaflet_start_date = start_date_string.scan(/\d+\/\d+\/\d+ \d+:\d+:\d+\s[A-Za-z]+/).first
    end_date_string = leaflet_dates_page.body.scan(/\"SaleEndDateString\":\"\d+\/\d+\/\d+ \d+:\d+:\d+\s[A-Za-z]+/).first
    leaflet_end_date = end_date_string.scan(/\d+\/\d+\/\d+ \d+:\d+:\d+\s[A-Za-z]+/).first
    images_urls = images_urls.map{|iu| iu.gsub("200.0.90.0", "980.0.80.0")}
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.image_urls = images_urls
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.image_urls
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    @report.info leaflet.start_date
    @report.info leaflet.end_date
    @report.info leaflet.image_urls
    leaflets << leaflet
    leaflets
  end
  
  def get_redirected_page(redirected_page, code, mechanize)
    base_url = "http://petco.shoplocal.com"
    begin
      if code.eql?("302") || code.eql?("301")
        redirect_url = redirected_page.response['location']
        mechanize.redirect_ok = false
        sleep(1)
        redirected_page = mechanize.get(redirect_url)
        code = redirected_page.code
      end
    end while !code.eql?("200")
    redirected_page
  end
  
  def parse_hours(hours_rows)
    week_hours = []
    hours_rows.each do |hour_row|
      puts "GOT #{hour_row.text} TO PARSE"
      @report.info "GOT #{hour_row.text} TO PARSE"
      temp_days = hour_row.search(".//span[@class='daypart']")[0].text.strip.downcase
      open_hours = hour_row.search(".//span[@class='time']")[0].text.strip.downcase
      days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
        !temp_days.include?("to") && !temp_days.include?("–")
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days = days.map{|d| d.strip}
      days.delete("")
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
      if days.length == 2 # > 1(before)
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".", "")) if  hours[0]
            .include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
            .include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".", "")) if hours[1]
            .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
            .include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".","")) if  hours[0]
            .include?("am")||hours[0].include?("a.m")||hours[0].include?("a.m.")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
            .include?("pm")||hours[0].include?("p.m.")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".","")) if hours[1]
            .include?("am")||hours[1].include?("a.m")||hours[1].include?("a.m.")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
            .include?("pm")||hours[1].include?("p.m.")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m", "").sub(".","")) if  hours[0]
        .include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.", "")) if hours[0]
        .include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m", "").sub(".","")) if hours[1]
        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end   
    end
    week_hours 
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_mechanize
    mechanize = Mechanize.new do |agent|
      agent.set_proxy("189.219.211.124", 10000)
    end
    mechanize
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '13c3499c90af2777e20834e2d346b13f8253d2fbeaf331467781f4d632ebcebe'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://stores.petco.com/")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
  
end