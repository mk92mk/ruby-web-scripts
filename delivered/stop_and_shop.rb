# b832ebf9a795d9ba062e5c3fc5067a7b44609c500bb20c3c1464c345cad9be58 for stop and shop
require 'pqsdk'
require 'mechanize'
require 'time'
require 'json'

class StopAndShop
  
  def initialize
    @report = Logger.new("StopAndShop#{Time.now.to_s}.log")
  end
  
  def get_stores(start_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://stopandshop.com/store/%s/?storeid=%s"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(start_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_options = stores_list_page.search("//select[@id='storeid']//option/@value")
    stores_options.each do |option_value|
      next if option_value.value.empty?
      mechanize = Mechanize.new
      store_url = "#{base_url}" %[option_value.value, option_value.value]
      store_details_page = mechanize.get(store_url)
      puts "GOT STORE DETAILS PAGE ON #{store_url}"
      @report.info "GOT STORE DETAILS PAGE ON #{store_url}"
      store_name = store_details_page.search("//div[@class='store-details']//h1")[0].text
      store_address = store_details_page.search("//div[@class='location']//h3")[0].text
      store_city_info = store_details_page.search("//div[@class='location']//p/text()")[0].text.strip
      if store_city_info.include?(",") && store_city_info.scan(/\d+/).first.to_s.length >= 4
        store_city = store_city_info.split(',')[0].strip
        store_zipcode = store_city_info.split(',')[1].strip.scan(/\d+/).first
      else
        store_city_info = store_details_page.search("//div[@class='location']//p/text()")[1].text.strip
        store_city = store_city_info.split(',')[0].strip
        store_zipcode = store_city_info.split(',')[1].strip.scan(/\d+/).first
      end
      store_phone = store_details_page.search("//div[@class='location']
                                              //li[contains(.//h3/text(), 'store phone')]//p")[0]
                                                                                .text.strip
      store_latitude = store_details_page.search("//div[contains(@data-title,'Stop & Shop')]
                                                  /@data-position-lat")[0].value
      store_longitude = store_details_page.search("//div[contains(@data-title,'Stop & Shop')]
                                                  /@data-position-lng")[0].value
      hours_items = store_details_page.search("//div[contains(.//h3/text(),'store hours')]//li")
      opening_hours = parse_hours(hours_items)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_url}"
      store_leaflets = get_leaflets(store_name, store.id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      allStoresIds << store.id
      sleep(5)
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_name, store_id)
    leaflets = []
    store_shopid = store_name.scan(/\d+/).first
    store_leaflet_url = "https://coupons.stopandshop.com/weeklyad/?store=0#{store_shopid}&s="
    images_urls = []
    catalog_store_id = store_shopid
    leaflets_imgs_url_format = "https://circulars-prod.cpnscdn.com/lightbox/vc2/_padolib/%s/zoom/%s.jpg?v=%s"
    catalogs_url = "https://circulars-prod.cpnscdn.com/lightbox/vc2/_padolib/stopandshop/catalogs.json"
    leaflet_pages_url_format = "https://circulars-prod.cpnscdn.com/lightbox/vc2/_padolib/%s/store/%s/issue.xml?"
    mechanize = Mechanize.new
    catalogs_json = mechanize.get(catalogs_url).body
    catalogs_parsed = JSON.parse(catalogs_json)
    catalogs_list = catalogs_parsed['catalogs']
    catalogs_list.map{|catalog| catalogs_list.delete(catalog) unless catalog.key?("start")}
    catalogs_list = catalogs_list.sort_by{|hash| Time.parse(hash['start'])}.reverse
    leaflet_start_date = ''
    leaflet_end_date = ''
    full_issue_key = ''
    catalogs_list.each do |catalog_item|
      if catalog_item['stores'].include?(store_shopid.to_i.to_s)
        full_issue_key = catalog_item["fullIssueKey"]
        leaflet_start_date = catalog_item['start']
        leaflet_end_date = catalog_item['end']
        break
      end
    end
    if full_issue_key.empty? 
      puts "NO LEAFLET FOUND FOR STORE #{store_name}"
      @report.info "NO LEAFLET FOUND FOR STORE #{store_name}"
      return leaflets
    end
    leaflet_pages_url = leaflet_pages_url_format %[full_issue_key, store_shopid.to_i.to_s]
    mechanize = Mechanize.new
    sleep(2)
    leaflet_details_page = mechanize.get(leaflet_pages_url)
    leaflet_image_pages = leaflet_details_page.search("//catalog-issue//@pages")[0].value.split(",")
    leaflet_images_id = leaflet_details_page.search("//catalog-issue//@modified")[0].value
    leaflet_image_pages.each do |image_page|
      image_url = leaflets_imgs_url_format %[full_issue_key, image_page, leaflet_images_id]
      images_urls << image_url
      puts "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      @report.info "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
    end
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
      leaflet.image_urls = images_urls
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.start_date
    @report.info leaflet.end_date
    @report.info leaflet.store_ids
#    puts leaflet.image_urls
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(hours_items)
    week_hours = []
    hours_items.each do |hour_item|
      day_hours = {}
      day_text = hour_item.search(".//span[@class='time-span']")[0].text.strip.downcase
      hours_text = hour_item.search(".//span[@class='time-open']")[0].text.strip.downcase
      puts "GOING TO PROCESS #{day_text} #{hours_text} FOR HOURS"
      @report.info "GOING TO PROCESS #{day_text} #{hours_text} FOR HOURS"
      day_index = get_day_index(day_text)
      day_hours['weekday'] = day_index
      if hours_text.include?("24 hours")
        day_hours["open_am"] = "00:00"
        day_hours["close_pm"] = "23:59"
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
        next
      else
        hours = hours_text.split("-").map{|ht| ht.strip}
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a", "")) if  hours[0]
                                                                   .include?("am")||hours[0].include?("a")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p", "")) if hours[0]
                                                                   .include?("pm")||hours[0].include?("p")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a", "")) if hours[1]
                                                                   .include?("am")||hours[1].include?("a")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p", "")) if hours[1]
                                                                   .include?("pm")||hours[1].include?("p")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'b832ebf9a795d9ba062e5c3fc5067a7b44609c500bb20c3c1464c345cad9be58'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://stopandshop.com/store/locator/")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end