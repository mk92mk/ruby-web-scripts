# 71667d9aa5a7743fbb94ed9406b2b35bcc18bba168407c86d64cb3e16da77de8 for Corgi UK
require 'pqsdk'
require 'byebug'
require 'mechanize'


class Crawler::Uk::Corgi < Crawler::Base

  def get_stores
    allStoresIds = []
    cities = PQSDK::City.all.map{|c| c.name.strip.downcase}
    current_page_url = "http://www.corgi.co.uk/stockists/england"
    begin
      mechanize = Mechanize.new
      sleep(10)
      current_page = mechanize.get(current_page_url)
      stores_list = current_page.search("//div[contains(@id,'stockist')]")
      stores_list.each do |store_item|
        store_name = store_item.search(".//h3").text
        store_address = store_item.search(".//address").text.strip
        store_phone = store_item.search(".//span[contains(text(),
                                           'Phone')]/parent::*/text()").text
        address_parts = store_address.split(",")
        store_zipcode = address_parts[-1].strip
        store_city = ""
        stores_url = current_page_url
        opening_hours = []
        address_parts.reverse.each do |address_part|
          if cities.include?(address_part.strip.downcase)
            store_city = address_part.strip
            break
          end
        end
        if store_city.empty?
          store_city = address_parts[-3].strip
        end
        store_address = store_address.gsub("#{store_city},", "") # For avoiding duplicate replacements 5A Harpur Street, Bedford, Bedfordshire
        store_address = store_address.gsub(store_zipcode, "")
        store_address = store_address.gsub("UK", "")

        temp_address = store_address.split(",").map{|a| a.strip} if store_address.count(",") >= 2
        if temp_address
          temp_address.delete("")
          store_address = temp_address.join(",")
        end

        javascripts = current_page.search("//script")
        coord_script = javascripts[48].text if javascripts[48].text.include?("LatLngList.push")
        if coord_script && coord_script.include?(store_name)
          start_index = coord_script.index(store_name)
          end_index = start_index + 490
          target_text = coord_script[start_index..end_index]
          marker_index = target_text.index("new google.maps.LatLng")
          actual_string = target_text[marker_index..-1]
          coord_line = actual_string.lines.first.strip
          if coord_line.start_with?("new google.maps.LatLng (", "") && coord_line.end_with?("));")
            temp1 = coord_line.gsub("new google.maps.LatLng (", "")
            temp2 = temp1.gsub("));", "")
            coords = temp2.split(",")
            store_latitude = coords[0]
            store_longitude = coords[1]
          else
            @report.info << "UNUSUAL CASE IN THE JAVASCRIPT. NOT GETTING SIMILAR FOR COORDS"
          end
        end
        unless store_latitude.nil? && store_longitude.nil?
          if store_latitude.to_f < -90 || store_latitude.to_f > 90 || 
              store_longitude.to_f > 180 || store_longitude.to_f < -180
            store_latitude = "0"
            store_longitude = "0"
          end
        end        
        store_zipcode = "00000" if store_zipcode.empty?
        store_latitude = "0" if store_latitude.nil? || store_latitude.empty?
        store_longitude = "0" if store_longitude.nil? || store_longitude.empty?
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        store.save
        allStoresIds << store.id        
        @report.info <<  "*************STORE*********************"
        @report.info <<  "STORE NAME IS #{store_name}"
        @report.info <<  "STORE CITY IS #{store_city}"
        @report.info <<  "STORE ZIP CODE IS #{store_zipcode}"
        @report.info <<  "STORE ADDRESS IS #{store_address}"
        @report.info <<  "STORE LONGITUDE #{store_longitude}"
        @report.info <<  "STORE LATITUDE #{store_latitude}"
        @report.info <<  "STORE PHONE #{store_phone}"
        @report.info <<  "STORE HOURS #{opening_hours}"
        @report.info <<  "STORE URL #{stores_url}"
      end
      next_page_href = current_page.search("//a[contains(@title, 'Next')]/@href")[0]
      next_page_url = next_page_href.value if next_page_href
      current_page_url = next_page_href.nil? ? "" : next_page_url
      @report.info << "GOING TO NEXT PAGE ON #{next_page_url} " if next_page_href
    end while !current_page_url.empty?
    allStoresIds
  end
  
  def get_leaflets(store_ids, leaflets_url)
    allLeaflets = []
    mechanize = Mechanize.new
    sleep(10)
    leaflets_page = mechanize.get(leaflets_url)
    pdf_url = leaflets_page.search("//figure[contains(@class,'image-container full_width')]
                                    //a[contains(@href, '.pdf')]/@href")[0].value
    @report.info << "Download leaflet from #{pdf_url}"
    leaflet_name = pdf_url.split("/")[-1].gsub(".pdf", "_leaflet")
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = leaflet_name
      leaflet.url = pdf_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = Time.now.to_s        
      leaflet.store_ids = store_ids
      leaflet.save     
      @report.info << "********LEAFLET******"
      @report.info << leaflet.name
      @report.info << leaflet.url
      @report.info << "********LEAFLET END********"
      allLeaflets << leaflet
    end
    allLeaflets
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '71667d9aa5a7743fbb94ed9406b2b35bcc18bba168407c86d64cb3e16da77de8'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.corgi.co.uk/shop/corgi-direct.html")
    @report.info << "JUST ENDED THE SITE DATA"
  end

end