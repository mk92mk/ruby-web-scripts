#b1987c400df567f67ab7c5cfde9050180898b9d65a9fcc2bc4ae77ccc27861d6
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class Elenterriano
  
  def get_stores
    allStoresIds = []
#    base_url = "http://www.libertytravel.com"
    stores_list_url = "http://www.bazarelentrerriano.com.ar/site/sucursales"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE"
    stores_items = stores_list_page.search("//div[@class='sucursal']")
    stores_items.each do |store_item|
      store_name = store_item.search(".//h3[@class='titulo_detalle']")[0].text.strip
      store_address = store_item.search(".//div[@class='direccion']/text()")[0].text.strip
                                .chomp("-").sub(":","").strip
      store_city = store_item.search(".//preceding-sibling::h2[@class='titulo_sucursal']")[0].text
      store_zipcode = "00000"
      store_phone = store_item.search(".//div[@class='direccion'][1]/text()")[1].text
                                  .sub(":", "").strip
      store_coords = store_item.search(".//a[contains(text(), 'Ver mapa completo')]/@href")[0].value
                               .split("&ll=")[-1].split("&spn")[0].split(",")
      if store_coords[0].include?("bing.com/maps/")
        store_coords = store_coords[0].split("cp=")[-1].split("&st=")[0].split(",")
      end
      store_latitude = store_coords[0]
      store_longitude = store_coords[1]
      store_hours = []
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_list_url
      end
      store.opening_hours = store_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE IS #{store_longitude}"
      puts  "STORE LATITUDE IS #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{store_hours}"
      puts  "STORE URL #{stores_list_url}"
      puts "******"
      allStoresIds << store.id
    end
    allStoresIds
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    leaflet_url = "http://www.bazarelentrerriano.com.ar/site/catalogo-online/"
    mechanize = Mechanize.new
    leaflet_page = mechanize.get(leaflet_url)
    puts "GOT LEAFLET PAGE"    
    leaflet_pdf_href = leaflet_page.search("//a[@class='download']/@href")[0].value
    leaflet_pdf_url = "#{leaflet_url}#{leaflet_pdf_href}"
    store_leaflet_name = "El Enterriano_Leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
      leaflet.store_ids = store_ids
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts "**********"
    allStoresLeafletsIds << leaflet.id
    allStoresLeafletsIds
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'b1987c400df567f67ab7c5cfde9050180898b9d65a9fcc2bc4ae77ccc27861d6'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end   
  
end