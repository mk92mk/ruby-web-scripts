#9674287959e2cfd0cbbb125d28f42be2f53ead007c2f49d7c0b425b90525485f for whole foods market
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class WholeFoodsMarket
  
  def get_stores(stores_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    sleep(1)
    stores_list_page = mechanize.get(stores_url)
    stores_list = JSON.parse(stores_list_page.body)
    stores_list.each_with_index do |store_item, index|
      #for testing
#      store_item = stores_list["1312111"]
#      store_leaflet_key = store_item["tlc"] 
#      store_info = store_item
      #for testing
      store_leaflet_key = store_item[1]["tlc"]
      store_info = store_item[1] ##
      store_name = store_info["storename"]
      next unless store_info["location"]["country"].eql?("US")
      store_address = store_info["location"]["street"]
      store_city = store_info["location"]["city"]
      store_phone = store_info["phone"]
      store_zipcode = store_info["location"]["zip"]
      store_latitude = store_info["location"]["lat"]
      store_longitude = store_info["location"]["lon"]
      store_hours = store_info["hours"]
      puts "ON #{index} STORE NOW .  #{store_address}"
      opening_hours = parse_hours(store_hours)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_url}"
      puts "******"
      allStoresIds << store.id
      store_leaflets = get_leaflets(store.id, store_leaflet_key, store_name)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_leaflet_key, store_name)
    leaflets = []
    leaflet_pdf_url = "http://www2.wholefoodsmarket.com/storespecials/#{store_leaflet_key}_specials.pdf"
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    #leaflet.save 
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(opening_times)
    puts "GOT #{opening_times} TO PARSE"
    week_hours = []
    opening_times = opening_times.downcase
    if opening_times.include?("; saturday")
      opening_times = opening_times.sub("; saturday", ", saturday")
    elsif opening_times.include?("saturday & sunday")
      opening_times = opening_times.sub("saturday & sunday", ",saturday & sunday")
    elsif opening_times.include?("& sunday")
      opening_times = opening_times.sub("& sunday", ", sunday")
    end
    if opening_times.include?("8am to 9pm sunday through thursday.  8am to 10pm friday and saturday")
      opening_times = "monday through thursday 8am to 9pm,sunday 8am to 9pm, friday 8am to 10pm, saturday 8am to 10pm"
    end
    if opening_times.include?("store open 8am-10pm") && opening_times
                    .start_with?("coffee bar open") && !opening_times.include?(",")
      opening_times = "mon-sun 8am-10pm"
    end
    if opening_times.start_with?("join us every day,")
      opening_times = opening_times.sub("join us every day,", "")
    end
    if opening_times.include?("sunday-thursday: 8 am-9 pm friday & saturday: 8 am-10 pm")
      opening_times = "monday-thursday: 8am-9pm, sunday 8am-9pm, friday 8 am-10 pm, saturday: 8 am-10 pm"
    end
    opening_times = opening_times.strip.downcase.split("|")[0].split(";")[0].split("(")[0]
                                  .split("the coffee bar")[0]
                                  .split("coffee")[0].split("rock-it")[0].split("accessible via")[0]
                                  .split("steep brew")[0].split("somerset tap")[0]
                                  .split("the social")[0].split("whole body")[0].split("cafe hours")[0]
                                  .split("follow")[0].split("wine")[0].split("back bay")[0]
                                  .split("our merchant")[0].split("see below")[0].split("join")[0]
                                  .split("the eight")[0].split("• the river room")[0]
                                  .squeeze(" ").sub("store open ", "").sub("operating hours are", "")
                                  .sub("now open!  business hours:", "")
                                  .sub("store:", "").sub("regular hours", "").gsub("store hours:", "")
                                  .sub("regular store hours", "").sub("join us every day,", "")
                                  .sub("new expanded hours starting monday 25th may:", "").sub("main", "")
                                  .sub("business hours", "").sub("store hours", "").sub("hours:", "")
                                  .sub("a.m.", "am").sub("p.m.", "pm").gsub(".", ":")
                                  .gsub("7 days a week", "").sub("regular", "").sub("7 days/wk", "")
                                  .sub("regular hours:", "").sub("we are open", "").sub("!", "")
                                  .sub("a:m:", "am").sub("p:m", "pm").sub("a:m", "am").sub("p:m:", "pm")
                                  .sub("daily", "").sub("now open", "").sub("every day", "")
                                  .strip.chomp(":").strip.chomp(":").strip.chomp(":")
                                  .strip
    opening_times[0] = "" if opening_times[0].eql?(":")
    return week_hours if opening_times.include?("opening") || opening_times.include?("permanently closed")
    unless opening_times.include?("am") || opening_times.include?("pm")
      if opening_times.include?("a") || opening_times.include?("p")
        opening_times = opening_times.sub("a", "am").sub("p", "pm")
      end
    end
    if opening_times.include?(",")
      store_hours = opening_times.split(",")
      store_hours.each_with_index do |store_hour, index|
        store_hour = store_hour.strip
        first_digit = /\d+/.match(store_hour).to_s
        next if first_digit.empty?
        breakpoint = store_hour.index(first_digit)
        if breakpoint == 0 && (store_hour.include?("mon") || store_hour.include?("tue") ||store_hour
            .include?("wed") ||store_hour.include?("thu") || store_hour
            .include?("fri") ||store_hour.include?("sat") || store_hour
            .include?("sun") )
          temp_indexes = []
          mon_index = temp_indexes << store_hour.index("mon") if store_hour.index("mon")
          tue_index = temp_indexes << store_hour.index("tue") if store_hour.index("tue")
          wed_index = temp_indexes << store_hour.index("wed") if store_hour.index("wed")
          thu_index = temp_indexes << store_hour.index("thu") if store_hour.index("thu")
          fri_index = temp_indexes << store_hour.index("fri") if store_hour.index("fri")
          sat_index = temp_indexes << store_hour.index("sat") if store_hour.index("sat")
          sun_index = temp_indexes << store_hour.index("sun") if store_hour.index("sun")
          temp_indexes = temp_indexes.sort                  
          store_hour = "#{store_hour[temp_indexes[0]..-1]} #{store_hour[0..temp_indexes[0]-1]}"
          store_hours[index] = store_hour
        elsif breakpoint == 0 && !(store_hour.include?("mon") || store_hour.include?("tue") ||store_hour
            .include?("wed") ||store_hour.include?("thu") || store_hour
            .include?("fri") ||store_hour.include?("sat") || store_hour
            .include?("sun") )
          store_hour = "mon-sun #{store_hour.gsub("seven days a week", "").gsub("open daily", "")
          .gsub("daily", "").gsub("every day", "").gsub("open", "")}"
          store_hours[index] = store_hour
        elsif store_hour.start_with?("open")
          store_hour = store_hour.gsub("open daily", "mon-sun").gsub("open", "mon-sun").gsub("daily", "")
          .strip
          store_hours[index] = store_hour
        end
      end
    else
      unless opening_times.include?(",")
        store_hours = []
        if opening_times.start_with?("store: m-")
          opening_times = opening_times.sub("store:", "").sub("m-f", "mon-fri").sub("m-s", "mon-sun")
          .split("pharmacy")[0]
        elsif opening_times.start_with?("store: 6am") && opening_times.include?("everyday")
          opening_times = opening_times.sub("store:", "mon-sun").split("pharmacy")[0].sub("everyday", "")
        elsif opening_times.start_with?("everyday ")
          opening_times = opening_times.sub("everyday", "mon-sun")
        end
        hours_text = opening_times.strip
        first_digit = /\d+/.match(hours_text).to_s
#        next if first_digit.empty?
        breakpoint = hours_text.index(first_digit)
        if breakpoint == 0 && (hours_text.include?("mon") || hours_text.include?("tue") ||hours_text
            .include?("wed") ||hours_text.include?("thu") || hours_text
            .include?("fri") ||hours_text.include?("sat") || hours_text
            .include?("sun") )
          temp_indexes = []
          mon_index = temp_indexes << hours_text.index("mon") if hours_text.index("mon")
          tue_index = temp_indexes << hours_text.index("tue") if hours_text.index("tue")
          wed_index = temp_indexes << hours_text.index("wed") if hours_text.index("wed")
          thu_index = temp_indexes << hours_text.index("thu") if hours_text.index("thu")
          fri_index = temp_indexes << hours_text.index("fri") if hours_text.index("fri")
          sat_index = temp_indexes << hours_text.index("sat") if hours_text.index("sat")
          sun_index = temp_indexes << hours_text.index("sun") if hours_text.index("sun")
          temp_indexes = temp_indexes.sort                  
          hours_text = "#{hours_text[temp_indexes[0]..-1]} #{hours_text[0..temp_indexes[0]-1]}"
        elsif breakpoint == 0 && !(hours_text.include?("mon") || hours_text.include?("tue") ||hours_text
            .include?("wed") ||hours_text.include?("thu") || hours_text
            .include?("fri") ||hours_text.include?("sat") || hours_text
            .include?("sun") )
          hours_text = "mon-sun #{hours_text.gsub("seven days a week", "").gsub("open daily", "")
                                  .gsub("daily", "").gsub("every day", "").gsub("open", "")
                                  .gsub("everyday", "")}"
        elsif hours_text.start_with?("open")
          hours_text = hours_text.gsub("open daily", "mon-sun").gsub("open", "mon-sun").gsub("daily", "")
                                 .gsub("seven days a week", "").gsub("everyday", "").strip
        end
        first_digit = /\d+/.match(hours_text).to_s
        breakpoint = hours_text.index(first_digit)
        temp_days = hours_text[0..breakpoint-1]
        temp_mixed = hours_text[breakpoint..-1]
        temp_indexes = []
        if temp_mixed.downcase.include?("mon") || temp_mixed.downcase.include?("tue") || 
            temp_mixed.downcase.include?("wed") || temp_mixed.downcase.include?("thu") ||
            temp_mixed.downcase.include?("fri") || temp_mixed.downcase.include?("sat") ||
            temp_mixed.downcase.include?("sun")
          temp_mixed = temp_mixed.downcase
          mon_index = temp_indexes << temp_mixed.index("mon") if temp_mixed.index("mon")
          tue_index = temp_indexes << temp_mixed.index("tue") if temp_mixed.index("tue")
          wed_index = temp_indexes << temp_mixed.index("wed") if temp_mixed.index("wed")
          thu_index = temp_indexes << temp_mixed.index("thu") if temp_mixed.index("thu")
          fri_index = temp_indexes << temp_mixed.index("fri") if temp_mixed.index("fri")
          sat_index = temp_indexes << temp_mixed.index("sat") if temp_mixed.index("sat")
          sun_index = temp_indexes << temp_mixed.index("sun") if temp_mixed.index("sun")
          temp_indexes = temp_indexes.sort
          if temp_indexes.length == 1
            store_hours << temp_mixed[temp_indexes[0]..-1]
            store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
          elsif temp_indexes.length == 2
            store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
            if temp_indexes[1] - temp_indexes[0] > 6
              store_hours << temp_mixed[temp_indexes[0]..temp_indexes[1]-1]
              store_hours << temp_mixed[temp_indexes[1]..-1]
            end
            if temp_indexes[1] - temp_indexes[0] <= 6
              store_hours << temp_mixed[temp_indexes[0]..-1]
            end
          end
        else
          store_hours << hours_text
        end
      end
    end
    store_hours.each do |store_hour|
      puts "GOING TO PARSE #{store_hour} NOW"
      if store_hour.include?("&")
        days_splits = store_hour.split("&")
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        store_hours << "#{days_splits[0]} #{store_hour[breakpoint..-1]}"
        store_hours << days_splits[1].strip
        next
      end
      if store_hour.include?("8am to 9pm sunday through thursday.  8am to 10pm friday and saturday")
        store_hours << "thursday through sunday 8am to 9pm"
        store_hours << "friday 8am to 10pm"
        store_hours << "saturday 8am to 10pm"
        next
      end
      store_hour = store_hour.sub("thru", "-").sub("through", "-").sub("midnight", "11:59pm").split(";")[0]
                             .strip
                             .chomp(":").strip.chomp(":").strip.chomp(":")
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
              .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "").sub("-", "").sub(".", ":")}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
            .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
            .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '9674287959e2cfd0cbbb125d28f42be2f53ead007c2f49d7c0b425b90525485f'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids, leaflet_ids = get_stores("http://www.wholefoodsmarket.com/ajax/stores")
    puts "GOING TO COLLECT THE LEAFLETS"
    puts "JUST ENDED THE SITE DATA"
  end
end