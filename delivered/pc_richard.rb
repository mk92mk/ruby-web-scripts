#ab5449a49267d0f145a8bce348067e5430eed930eafeb213a7ce5e7539afd2f9 FOR PCRICHARD

require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class PcRichard
  
#  def initialize
#    @report = Logger.new("AARONS_#{Time.now.to_s}.log")
#  end
#  

  def get_stores(stores_list_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.pcrichard.com"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get("http://www.pcrichard.com/storelocator/store-landing.jsp")
    puts "GOT STORES LIST PAGE"
    stores_details_hrefs = stores_list_page.search("//div[@class='store-locations-1']//li//a/@href")
    stores_details_hrefs.each do |store_detail_href|
      store_details_url = "#{base_url}#{store_detail_href.value}"
      mechanize = Mechanize.new
      store_details_page = mechanize.get(store_details_url)
      puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
      store_name = store_details_page.search("//h1[@itemprop='name']")[0].text.strip
      store_address = store_details_page.search("//span[@itemprop='streetAddress']")[0].text.strip
      store_city = store_details_page.search("//span[@itemprop='addressLocality']")[0].text.strip
      store_zipcode = store_details_page.search("//span[@itemprop='postalCode']")[0].text.strip
      store_phone = store_details_page.search("//span[@itemprop='telephone']")[0].text.strip
      script_text = store_details_page.search("//script[contains(text(), 'var map')]")[0].text.strip
      store_latitude = script_text.match(/var lng = \d+.\d+|var lng = -\d+.\d+/).to_s
                                  .match(/\d+.\d+|-\d+.\d+/).to_s
      store_longitude = script_text.match(/var lat = \d+.\d+|var lat = -\d+.\d+/).to_s
                                   .match(/\d+.\d+|-\d+.\d+/).to_s
      store_hours_nodes = store_details_page.search("//meta[@itemprop='openingHours']/@datetime")
      opening_hours = parse_hours(store_hours_nodes)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_details_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_details_url}"
      puts "******"
      allStoresIds << store.id
      store_leaflets = get_leaflets(store.id, store_zipcode, store_name)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      sleep(2)
    end
    
  end
  
  def get_leaflets(store_id, store_zipcode, store_name)
    leaflets = []
    store_leaflet_url = "http://circulars.pcrichard.com/flyers/pcrichard?type=2&auto_store=true&force_store_selection=true&postal_code=#{store_zipcode}&filler=&is_postal_entry=true#!/flyers/pcrichard-circular?flyer_run_id=125510"
    sleep(1)
    mechanize = Mechanize.new
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
    script_text = store_leaflet_page.search("//script[contains(text(), 'pdf_url')]")[0].text
    pdf_text = script_text.match(/\"pdf_url\":\".+.pdf\"/).to_s
    leaflet_pdf_url = URI.extract(pdf_text)[0]
    leaflet_dates = store_leaflet_page.search("//title")[0].text.sub("P.C. Richard Current Ad -", "").strip
    leaflet_start_date = Time.parse(leaflet_dates.split("to")[0].strip)
    leaflet_end_date = Time.parse(leaflet_dates.split("to")[1].strip)
    puts "DOWNLOAD FROM #{leaflet_pdf_url}"
#    @report.info "DOWNLOAD FROM #{pdf_url}"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = leaflet_pdf_url
      leaflet.name = "#{store_name}_leaflet"
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    leaflets << leaflet
    puts "********"
    leaflets
  end
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      store_hour = store_hour_node.value.strip.downcase
      puts "GOING TO PARSE #{store_hour} NOW"
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
                                .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'ab5449a49267d0f145a8bce348067e5430eed930eafeb213a7ce5e7539afd2f9'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
#    @report.info "IN RUN METHOD"
#    @report.info "GOING TO COLLECT THE STORES"
    store_ids, leaflet_ids = get_stores("http://www.pcrichard.com/storelocator/store-landing.jsp")
    puts "JUST ENDED THE SITE DATA"
#    @report.info "GOING TO COLLECT THE LEAFLETS"
#    @report.info "JUST ENDED THE SITE DATA"
  end
end