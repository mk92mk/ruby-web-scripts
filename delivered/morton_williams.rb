#6eb982fc413dad3f47663752e709af85b5df91ce83e900f0c8c695357a60a0f7 for morton_williams
require 'pqsdk'
require 'mechanize'
require 'json'


class MortonWilliams
  
  def get_stores(stores_list_url, stores_leaflets_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    base_url = ""
    stores_list_json = mechanize.get(stores_list_url)
    puts "GOT STORES LIST JSON"
    stores_list_parsed = JSON.parse(stores_list_json.body)
    stores_list = stores_list_parsed["content"]["locations"]
    cities_stores = {}
    stores_list.each do |store_item|
      store_name = store_item["name"]
      store_address = store_item["address"]
      address_parts = store_address.split(",")
      store_city = address_parts[-3].strip
      store_zipcode = address_parts[-2].strip.scan(/\d+/).first
      store_latitude = store_item["lat"]
      store_longitude = store_item["lng"]
      store_address = "#{store_address} #{store_item["description"]}".gsub(store_city, "")
                                                                     .gsub(store_zipcode, "")
                                                                     .gsub("USA","")
                                                                     .gsub("NY","")
                                                                     .gsub(",","")
                                                                     .squeeze(" ")
      store_url = stores_list_url
      opening_hours = []
      store_phone = ""
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      cities_stores[store_city] = [] unless cities_stores[store_city]
      cities_stores[store_city] << store
      allStoresIds << store.id
    end
    mechanize = Mechanize.new
    leaflets_page = mechanize.get(stores_leaflets_url)
    store_leaflets = get_leaflets(cities_stores, stores_leaflets_url)
    store_leaflets.each do |store_leaflet|
      allStoresLeafletsIds << store_leaflet.id
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(cities_stores, stores_leaflets_url)
    leaflets = []
    base_url = "http://media.wix.com/ugd/"
    mechanize = Mechanize.new
    leaflets_page = mechanize.get(stores_leaflets_url)
    leaflets_json = JSON.parse(leaflets_page.body)
    document_data = leaflets_json["data"]["document_data"]
    cities_stores.each do |city, stores_list|
      pdf_url = ""
      leaflet_name = ""
      document_data.each do |data_key, data_value|
        next unless data_value["label"]
        link = ""
        if city.strip.eql?("New York")
          next if !(data_value["label"].include?("Manhattan") && data_value["label"].include?("pdf"))
          if data_value["label"].include?("Manhattan") && data_value["label"].include?("pdf")
            link = data_value["link"].gsub("#","")
          end
        else
          next if !(data_value["label"].include?(city) && data_value["label"].include?("pdf"))
          if data_value["label"].include?(city) && data_value["label"].include?("pdf")
            link = data_value["link"].gsub("#","")
          end
        end
        leaflet_name = document_data[link]["name"]
        doc_id = document_data[link]["docId"]
        pdf_url = "#{base_url}#{doc_id}"
        leaflet = PQSDK::Leaflet.find pdf_url
        if leaflet.nil?
          leaflet = PQSDK::Leaflet.new
          leaflet.url = pdf_url
          leaflet.name = leaflet_name
        end
        stores_list.each do |store|
          if !leaflet.store_ids.include?(store.id)
            leaflet.store_ids << store.id
          end
        end
        # leaflet.save
        puts "*******************LEAFLET"
        puts leaflet.name
        puts leaflet.url
        puts leaflet.store_ids
        leaflets << leaflet
        break
      end
    end
    leaflets
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '3776337d7d85e27915e30a2e98996c4301d0a2fb8513b0ff7d65ee7bcaf16b16'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("https://www.powr.io/plugins/map/wix_view.json?cacheKiller=1458137985057&compId=i607gyxz&deviceType=desktop&instance=6C-F_MDwIqCKbLhK9kIC5U6KKJ0fBtVnFRmUAuVdDUQ.eyJpbnN0YW5jZUlkIjoiMTNjMWI1YjItN2Y2Ni1hOWMzLTBmMjMtNDU5MjU3MjVlMGI2Iiwic2lnbkRhdGUiOiIyMDE2LTAzLTE2VDE0OjE5OjU0LjA0MloiLCJ1aWQiOm51bGwsInBlcm1pc3Npb25zIjpudWxsLCJpcEFuZFBvcnQiOiIxMTkuMTU3LjM1Ljg4LzU1OTUyIiwidmVuZG9yUHJvZHVjdElkIjpudWxsLCJkZW1vTW9kZSI6ZmFsc2UsImFpZCI6ImI2YmVkNDMwLWQ5MmQtNGUyMy04OGNjLTMzZWRlYzUxMmY3MSIsInNpdGVPd25lcklkIjoiNWM5ZDcyOTQtY2E5NC00NzFmLWEyZTgtYWI3MWU5ZmZiMTFjIn0&locale=en&viewMode=site&width=660",
                                        "https://static.wixstatic.com/sites/5c9d72_3cca2bb23fdcea593993ca06b1624baf_724.json.z?v=3")
    puts "JUST ENDED THE SITE DATA"
  end
end