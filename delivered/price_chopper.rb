#11e4cd59a679a9b17ec938cb256565fc19a099a40c3c172c0cf4a15bc689a967 for price chopper
require 'pqsdk'
require 'mechanize'
require 'json'

class PriceChopper
  
  def initialize
    @report = Logger.new("PriceChopper#{Time.now.to_s}.log")
  end
  
  def get_stores(locations_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://locations.pricechopper.com"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(locations_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_list_hrefs = stores_list_page.search("//ul[@class='store-list']//a/@href")
    stores_list_hrefs.each do |store_href|
      mechanize = Mechanize.new
      store_url = "#{base_url}#{store_href.value}"
      store_page = mechanize.get(store_url)
      puts "GOT STORE DETAILS PAGE ON #{store_url}"
      @report.info "GOT STORE DETAILS PAGE ON #{store_url}"
      store_name = store_page.search("//div[@itemprop='name']")[0].text
      store_address = store_page.search("//span[@itemprop='streetAddress']")[0].text.strip
      store_city = store_page.search("//span[@itemprop='addressLocality']")[0].text.strip
      store_zipcode = store_page.search("//span[@itemprop='postalCode']")[0].text.strip
      coords_script = store_page.search("//script[@type='application/ld+json']")[0].text
      script_parsed = JSON.parse(coords_script)
      store_latitude = script_parsed["geo"]["latitude"]
      store_longitude = script_parsed["geo"]["longitude"]
      store_phone =  store_page.search("//strong[@itemprop='telephone']")[0].text
      hours_script = store_page.search("//script[@id='hoursBlock']")[0].text
      opening_hours = parse_hours(hours_script)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_url}"
      leaflet_href = store_page.search("//a[@id='mobile']/@href")[0]
      if leaflet_href
        store_leaflet_url = leaflet_href.value
        store_leaflets = get_leaflets(store_leaflet_url, store.id)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      allStoresIds << store.id
      sleep(5)
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_leaflet_url, store_id)
    leaflets = []
    mechanize = Mechanize.new
    store_leaflet_page = mechanize.get(store_leaflet_url)
    store_leaflet_imgs = store_leaflet_page.search("//ul[@id='flip']//a/@data-page-img")
    return leaflets if store_leaflet_imgs.empty?
    store_leaflet_name = store_leaflet_page.search("//div[@class='grayCont']//h1/text()")[0].text.strip
    store_leaflet_dates = store_leaflet_page.search("//span[@class='futura blue26']")[0].text.strip
    leaflet_year = store_leaflet_dates.split(",")[-1]
    temp_month = store_leaflet_dates.split("-")[0].tr('^A-Za-z', '')
    leaflet_start_date = Time.parse("#{store_leaflet_dates.split("-")[0]} #{leaflet_year}").to_s
    temp_end_date = store_leaflet_dates.split("-")[1]
    temp_end_date = "#{temp_month} #{temp_end_date}" if temp_end_date.tr('^A-Za-z', '').empty?
    leaflet_end_date = Time.parse(temp_end_date).to_s
    images_urls = []
    store_leaflet_imgs.each do |leaflet_img|
      images_urls << leaflet_img.value
      puts "#{leaflet_img.value} ADDED IN THE IMAGE URLS ARRAY"
      @report.info "#{leaflet_img.value} ADDED IN THE IMAGE URLS ARRAY"
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
      leaflet.image_urls = images_urls
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
#    puts leaflet.image_urls
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    @report.info leaflet.start_date
    @report.info leaflet.end_date
#    @report.info leaflet.image_urls
    leaflets << leaflet
    leaflets
  end
  
  
  def parse_hours(hours_script)
    mechanize = Mechanize.new
    hours_page = Mechanize::Page.new(nil,{'content-type'=>'text/html'},hours_script,nil,mechanize)
    week_days = hours_page.search("//div[contains(@class,'fh-item')]")
    week_hours = []
    week_days.each do |day_item|
      day_hours = {}
      week_day = day_item.search(".//span[@itemprop='dayOfWeek']")[0].text.strip.downcase
      day_index = get_day_index(week_day)
      day_hours["weekday"] = day_index
      day_opening_tag = day_item.search(".//span[@itemprop='opens']")[0]
      day_opening_hour = day_opening_tag.text.strip.downcase if day_opening_tag
      day_closing_tag = day_item.search(".//span[@itemprop='closes']")[0]
      if day_opening_tag.nil? && day_closing_tag.nil?
        if day_item.text.downcase.include?("closed")
          day_hours["closed"] = true
          week_hours << day_hours
        end
        next
      end
      day_closing_hour = day_closing_tag.text.strip.downcase unless day_opening_hour
                                                                    .include?("open 24 hours")

      if day_opening_hour.include?("open 24 hours")
        day_hours["open_am"] = "00:00"
        day_hours["close_pm"] = "23:59"
      elsif day_opening_hour.include?("closed")
        day_hours["closed"] = true
      else
        formatted_time_open_am = convert_hours_am(day_opening_hour.sub("am", "")) if  day_opening_hour
                                                                                .include?("am")
        formatted_time_open_pm = convert_hours_pm(day_opening_hour.sub("pm", "")) if day_opening_hour
                                                                                .include?("pm")
        formatted_time_close_am = convert_hours_am(day_closing_hour.sub("am", "")) if day_closing_hour
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(day_closing_hour.sub("pm", "")) if day_closing_hour
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
      end
      week_hours << day_hours
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '11e4cd59a679a9b17ec938cb256565fc19a099a40c3c172c0cf4a15bc689a967'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://locations.pricechopper.com/sitemap.html")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end