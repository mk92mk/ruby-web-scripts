#088c0ead846c8e66bcc9e8e4ddf0daca2a338cc3a67720388398e04dd411da59 for TOYSRUS
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'

class Toysrus
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    cities_page = mechanize.get("http://api.promoqui.eu/v1/cities")
    puts "GOT PROMOQUI CITIES PAGE"
    cities_list = JSON.parse(cities_page.body)
    cities_list.each do |city|
      next unless (city["state"].eql?("confirmed") && city["country"].eql?("USA"))
      city_stores_url = "http://www.toysrus.com/isvc/trus-sl/search?substore=tru&locale=en_US&callback=jQuery19108658521680397875_1462969638464&storeTag=TOYSRUS&storeTag=BABIESRUS&height=655&width=727&address=#{city["name"]}&radius=200&latitude=#{city["latitude"]}&longitude=#{city["longitude"]}&_=1462969638465"
      sleep(1)
      mechanize = Mechanize.new
      city_stores_page = mechanize.get(city_stores_url)
      puts "GOT CITY STORES PAGE FOR #{city["name"]} AT #{city_stores_url}"
      store_jsons = city_stores_page.body.sub("jQuery19108658521680397875_1462969638464(", "").strip
                                    .chomp(")").strip
      if store_jsons.include?("RESULTS\" :\n[\n\n]")
        puts "NO STORE FOUND FOR #{city["name"]} AT #{city_stores_url}"
        next
      end
      city_stores_list = JSON.parse(store_jsons)["RESULTS"]
      city_stores_list.each do |city_store|
        store_item = city_store["store"]
        store_name = store_item["locationName"]
        store_address = store_item["address1"]
        store_city = store_item["city"]
        store_zipcode = store_item["postalCode"]
        store_phone = store_item["phoneNumber"]
        store_latitude = store_item["latitude"]
        store_longitude = store_item["longitude"]
        puts "AT #{store_address} NOW"
        opening_hours = parse_hours(store_item["hours"])
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude if store_latitude
          store.longitude = store_longitude if store_longitude
          store.phone = store_phone
          store.origin = city_stores_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}" if store_longitude
        puts  "STORE LATITUDE #{store_latitude}" if store_latitude
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{city_stores_url}"
        puts "******"
        allStoresIds << store.id
        store_leaflets = get_leaflets(store.id, store_zipcode)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      sleep(2)
      puts "GOING TO NEXT CITY"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode)
    leaflets = []
    base_url = "http://weeklyad.toysrus.com"
    leaflets_url = "http://weeklyad.toysrus.com/ToysRUs?NuepRequest=true&listingid=0&sneakpeek=N&pagenumber=1&CityStateZip=#{store_zipcode}"
    mechanize = Mechanize.new
    begin
      leaflets_page = mechanize.get(leaflets_url)
    rescue Mechanize::ResponseReadError => e
      puts "GOT RESPONSE READ EXCEPTION PAGE"
      leaflets_page = e.force_parse
    end
    puts "GOT LEAFLETS PAGE AT #{leaflets_page.uri.to_s}"
    leaflet_page_loc = leaflets_page.search("//a[contains(@class, 
                                            'action-open-changelocation')]/@href")[0].value
    unless leaflet_page_loc.include?(store_zipcode)
      puts "NO LEAFLET FOUND FOR #{store_zipcode}"
      return leaflets
    end
    leaflets_hrefs = leaflets_page.search("//a[contains(@class,'ViewAdLink')]/@href")
    visited = []
    leaflets_hrefs.each do |leaflet_href|
      store_leaflet_url = "#{base_url}#{leaflet_href.value}"
      next if visited.include?(leaflet_href.value)
      mechanize = Mechanize.new
      leaflet_page = mechanize.get(store_leaflet_url)
      puts "AT LEAFLET #{store_leaflet_url} NOW"
      store_leaflet_name = leaflet_page.search("//div[@id='TitleBar']//h1")[0].text.strip
      next if store_leaflet_name.downcase.include?("spanish")
      leaflet_dates = leaflet_page.search("//span[@class='validDatesContain']")[0].text.lines[-1]
      leaflet_start_date = Time.parse(leaflet_dates.split("-")[0].strip)
      leaflet_end_date = Time.parse(leaflet_dates.split("-")[1].strip)
      images_script_text = leaflet_page.search("//script[contains(text(), 
                                                'pageElementsArray.Results.push(')]")[0].text
      images_script_text2 = images_script_text.gsub("//akimages.shoplocal.com", 
                                                    "http://akimages.shoplocal.com")
      script_urls = URI.extract(images_script_text2)
      images_urls = []
      script_urls.map{|u| images_urls << u.sub("0.0.88.0", "550.0.88.0") if u
                                  .start_with?("http://akimages.shoplocal.com") && u.end_with?(".jpg")}
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.image_urls
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.store_ids
      puts "**********"
      leaflets << leaflet
      sleep(2)
      visited << leaflet_href.value
    end
    leaflets
  end
  
  def parse_hours(store_item_hours)
    week_hours = []
    puts "GOT STORE HOURS #{store_item_hours}"
    opening_time_sat = store_item_hours["openingTimeSat"]
    opening_time_sun = store_item_hours["openingTimeSun"]
    opening_time_mon = store_item_hours["openingTimeMon"]
    opening_time_tue = store_item_hours["openingTimeTue"]
    opening_time_wed = store_item_hours["openingTimeWed"]
    opening_time_thu = store_item_hours["openingTimeThu"]
    opening_time_fri = store_item_hours["openingTimeFri"]
    
    closing_time_sat = store_item_hours["closingTimeSat"]
    closing_time_sun = store_item_hours["closingTimeSun"]
    closing_time_mon = store_item_hours["closingTimeMon"]
    closing_time_tue = store_item_hours["closingTimeTue"]
    closing_time_wed = store_item_hours["closingTimeWed"]
    closing_time_thu = store_item_hours["closingTimeThu"]
    closing_time_fri = store_item_hours["closingTimeFri"]
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_mon.downcase.sub("am", "")).rjust(5, "0") if 
                                             opening_time_mon && opening_time_mon.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_mon.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_mon && opening_time_mon.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_mon.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_mon.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 0
      week_hours << store_hour
    end
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_tue.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_tue.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_tue.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_tue.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 1
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_wed.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_wed.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_wed.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_wed.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 2
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_thu.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_thu.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_thu.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_thu.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 3
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_fri.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_fri.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_fri.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_fri.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 4
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sat.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sat.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sat.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sat.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 5
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sun.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sun.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sun.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sun.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 6
      week_hours << store_hour
    end
    week_hours
  end
  
  def convert_hours_am(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '088c0ead846c8e66bcc9e8e4ddf0daca2a338cc3a67720388398e04dd411da59'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
  end
end