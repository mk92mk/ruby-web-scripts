#de32c552f1183f5fb73c9d67941761a4cbff4413cf546c16b2616524b74ba2f7 for Arhaus
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class Arhaus
  
  def get_stores(stores_list_url)
    allStoresIds = []
    base_url = "http://www.arhaus.com"
    mechanize = Mechanize.new
    stores_locations_page = mechanize.get(stores_list_url)
    puts "GOT STORES LOCATIONS PAGE"
    stores_details_hrefs = stores_locations_page.search("//div[@id='store-locations']//li//a/@href")
    stores_details_hrefs.each do |store_detail_href|
      store_details_url = "#{base_url}#{store_detail_href.value}"
      sleep(1)
      mechanize = Mechanize.new
      store_details_page = mechanize.get(store_details_url)
      puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
      store_name = store_details_page.search("//h1[@itemprop='name']")[0].text.strip
      store_address = store_details_page.search("//li[@itemprop='streetAddress']")[0].text.strip
      store_city = store_details_page.search("//span[@itemprop='addressLocality']")[0].text.strip
      store_zipcode = store_details_page.search("//span[@itemprop='postalCode']")[0].text.strip
      store_phone_node = store_details_page.search("//span[@itemprop='telephone']")[0]
      store_phone = store_phone_node.text.strip if store_phone_node
      script_text = store_details_page.search("//script[contains(text(), 'arh.currentStore')]")[0].text
      store_coords = script_text.scan(/\d+.\d+|-\d+.\d+/)
      store_latitude = store_coords[0]
      store_longitude = store_coords[1]
      store_hours_list = store_details_page.search("//ul[@id='hours']//li")
      puts "ON #{store_address} NOW"
      opening_hours = parse_hours(store_hours_list)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_details_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_details_url}"
      puts "******"
      allStoresIds << store.id
    end
    allStoresIds
  end
  
  def parse_hours(store_hours_list)
    week_hours = []
    store_hours_list.each do |store_hour_item|
      day_name = store_hour_item.search(".//span[@itemprop='name']")[0].text.downcase.strip
      open_hours = store_hour_item.search(".//span[@itemprop='opens']/@content")[0].value.downcase.strip
      puts "GOING TO PARSE #{day_name} AND #{open_hours} NOW"
      day_index = get_day_index(day_name)
      hours = open_hours.split("-")
      day_hours = {}
      day_hours["weekday"] = day_index
      formatted_time_open_am = convert_hours_am(hours[0].strip.sub("am", "")) if hours[0].include?("am")
      formatted_time_open_pm = convert_hours_pm(hours[0].strip.sub("pm", "")) if hours[0].include?("pm")
      formatted_time_close_am = convert_hours_am(hours[1].strip.sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
      formatted_time_close_pm = convert_hours_pm(hours[1].strip.sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
      day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
      day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
      day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
      day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
      day_hours["closed"] = true if open_hours.include?("closed")
      week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
    end
    week_hours
  end

  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end  
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    sleep(1)
    mechanize = Mechanize.new
    leaflets_page = mechanize.get("http://www.arhaus.com/corp/catalog/")
    puts "GOT STORE LEAFLETS LIST PAGE"
    leaflets_hrefs = leaflets_page.search("//div[@class='four columns']//a")
    leaflets_hrefs.each do |leaflet_href|
      store_leaflet_name = leaflet_href.search("./@title")[0].value
      store_leaflet_url = leaflet_href.search("./@href")[0].value
      images_urls = []
      page_number = 1
      temp = true
      begin
        leaflet_image_url = "#{store_leaflet_url.strip.split("contenturl=")[-1]
                                             .sub("/skins/&asset=", "/is/image/")}-#{page_number}"
        mechanize = Mechanize.new
        sleep(2)
        begin
          image_page = mechanize.get(leaflet_image_url)
          puts "GOT IMAGE PAGE AT #{leaflet_image_url}"
        rescue Exception => e
          case e.message
          when /403/ then puts "End of brochure reached "
            temp=false
            break
          end
        end
        status_code = image_page.code
        if status_code.eql?("200")
          images_urls << leaflet_image_url
          puts "#{leaflet_image_url} added in main array"
        end
        page_number = page_number + 1
      end while temp
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url    
        leaflet.image_urls = images_urls
        leaflet.store_ids = store_ids
        #      leaflet.save   
        allStoresLeafletsIds << leaflet.id    
        puts "************************LEAFLET***"
        puts leaflet.name
        puts leaflet.url
        puts leaflet.image_urls
        puts leaflet.start_date
        puts leaflet.end_date
        puts leaflet.store_ids
        puts "**********"
      end
    end
    allStoresLeafletsIds
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'de32c552f1183f5fb73c9d67941761a4cbff4413cf546c16b2616524b74ba2f7'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores("http://www.arhaus.com/store/storelist/")
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end
end