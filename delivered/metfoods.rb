#13c3499c90af2777e20834e2d346b13f8253d2fbeaf331467781f4d632ebcebe for Met Foods
require 'pqsdk'
require 'mechanize'
require 'json'
require 'uri'

class MetFoods
  
  def initialize
    @report = Logger.new("MetFoods#{Time.now.to_s}.log")
  end
  
  def get_stores(stores_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    leaflet_base_url = "http://www.metfoods.com/weekly-ad/?circularstoreidentifier="
    mechanize = Mechanize.new
    stores_page = mechanize.get(stores_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_script_text = stores_page.search("//script[contains(.//text(), 'var stores = [{')]")[0].text
    stores_index = stores_script_text.index("var stores = [{")
    stores_list_string = stores_script_text[stores_index+12..-1].split("// Map variables")[0]
                                                                                .strip.chomp(";")
    stores_json = JSON.parse(stores_list_string)
    stores_json.each do |store_item|
      store_name = store_item["name"]
      store_address = store_item["address1"]
      store_city = store_item["city"]
      store_zipcode = store_item["zipCode"]
      store_phone = store_item["phone"]
      store_latitude = store_item["latitude"]
      store_longitude = store_item["longitude"]
      opening_hours_text = store_item["hourInfo"]
      opening_hours = parse_hours(opening_hours_text)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_url}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{stores_url}"
      store_identifier = store_item["storeIdentifier"]
      store_leaflet_url = "#{leaflet_base_url}#{store_identifier}"
      store_leaflets = get_leaflets(store_leaflet_url, store.id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      allStoresIds << store.id
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_leaflet_url, store_id)
    leaflets = []
    mechanize = Mechanize.new
    store_leaflets_page = mechanize.get(store_leaflet_url)
    leaflets_script_text = store_leaflets_page.search("//script[contains(.//text(),
                                                      'circularPageDiv ')]")[0].text
    circular_index = leaflets_script_text.index("var circularPageDiv = ")
    leaflets_text = leaflets_script_text[circular_index+37..-1].split("var pageCount = ")[0]
    images_urls_text = URI.extract(leaflets_text)
    store_leaflet_name = store_leaflets_page.search("//meta[@property='og:title']/@content")[0].value
    leaflet_dates = store_leaflets_page.search("//div[@class='stc-main-content']//h2//div")[0].text
                                                                                .scan(/\d+\/\d+\/\d+/)
    leaflet_start_date = leaflet_dates[0]
    leaflet_end_date = leaflet_dates[1]
    images_urls = []
    store_leaflets_page.search("//div[@class='stc-main-content']//h2//div")[0].text.strip
    images_urls_text.each do |image_url|
      if image_url.start_with?("http://metfoodsdata.shoptocook.com/") &&
                                             image_url.end_with?(".jpg")
        images_urls << image_url 
        puts "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
        @report.info "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      else
        next
      end
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
      leaflet.image_urls = images_urls
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(hours_text)
    puts "GOT #{hours_text} TO PARSE"
    @report.info "GOT #{hours_text} TO PARSE"
    if hours_text.downcase.include?("hours of operation") && hours_text.downcase.include?("everyday")
       hours_text = hours_text.downcase.gsub("hours of operation", "mon-sun").gsub("everyday","").strip
    end
    hours_text = hours_text.gsub("Midnight", "11:59PM")
    hours_lines = hours_text.lines
    week_hours = []
    hours_lines.each do |store_hour|
      store_hour = store_hour.downcase   
      store_hour = "mon-sun 12am-11:59pm" if store_hour.include?("open 24 hours")
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
                                     !temp_days.include?("to") && !temp_days.include?("–")
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
      if days.length == 2 # > 1(before)
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".", "")) if  hours[0]
                          .include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
                          .include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".", "")) if hours[1]
                          .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                          .include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".","")) if  hours[0]
              .include?("am")||hours[0].include?("a.m")||hours[0].include?("a.m.")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
              .include?("pm")||hours[0].include?("p.m.")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".","")) if hours[1]
              .include?("am")||hours[1].include?("a.m")||hours[1].include?("a.m.")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
              .include?("pm")||hours[1].include?("p.m.")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m", "").sub(".","")) if  hours[0]
                        .include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.", "")) if hours[0]
                        .include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m", "").sub(".","")) if hours[1]
                        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end
    end
    week_hours  
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '13c3499c90af2777e20834e2d346b13f8253d2fbeaf331467781f4d632ebcebe'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.metfoods.com/find-your-store/")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end