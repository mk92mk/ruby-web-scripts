#89d97c4daee22c5fd2d3bbad75bd12f46bf3781f18b34f6e3ea90707b5c370ac for Kohls

require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class Kohls
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    states_list_page = mechanize.get(states_url)
    puts "GOT STATES PAGE "
    states_hrefs = states_list_page.search("//div[@class='tlsmap_list']//a[@class='regionlist']/@href")
    states_hrefs.each do |state_href|
      sleep(1)
      mechanize = Mechanize.new
      state_cities_page = mechanize.get(state_href.value)
      puts "GOT STATE CITIES PAGE #{state_href.value}"
      state_cities_hrefs = state_cities_page.search("//div[@class='tlsmap_list']
                                                     //a[@class='citylist']/@href")
      state_cities_hrefs.each do |state_city_href|
        sleep(1)
        mechanize = Mechanize.new
        city_stores_page = mechanize.get(state_city_href.value)
        puts "GOT CITY STORES PAGE AT #{state_city_href.value}"
        city_stores_hrefs = city_stores_page.search("//div[@class='side_location']
                                                    //span[@class='location-title']//a/@href")
        city_stores_hrefs.each do |city_store_href|
          sleep(2)
          mechanize = Mechanize.new
          store_details_page = mechanize.get(city_store_href.value)
          puts "GOT STORE DETAILS PAGE AT #{city_store_href.value}"
          store_name = store_details_page.search("//div[@class='breadcrumb']//span")[-1].text.strip
          store_address = store_details_page.search("//meta[@property='og:street-address']
                                                      /@content")[0].value.strip.chomp(",")
          store_city = store_details_page.search("//meta[@property='og:locality']/@content")[0].value
          store_zipcode = store_details_page.search("//meta[@property='og:postal-code']/@content")[0].value
          store_latitude = store_details_page.search("//meta[@property='og:latitude']/@content")[0].value
          store_longitude = store_details_page.search("//meta[@property='og:longitude']/@content")[0].value
          store_phone = store_details_page.search("//meta[@property='og:phone_number']/@content")[0].value
          store_hours = store_details_page.search("//meta[@itemprop='openingHours']/@content")
          puts "ON #{store_address} NOW"
          opening_hours = parse_hours(store_hours)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude
            store.longitude = store_longitude
            store.phone = store_phone
            store.origin = city_store_href.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}"
          puts  "STORE LATITUDE #{store_latitude}"
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{city_store_href.value}"
          puts "******"
          allStoresIds << store.id
          store_leaflets = get_leaflets(store.id, store_zipcode, store_address)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode, store_address)
    leaflets = []
    base_url = "http://kohls.shoplocal.com"
    stores_list_url = "#{base_url}/Kohls/StoreLocation/Index/?MapCityStateZip=#{store_zipcode}"#&ispartial=Y&fullscreen=N
    sleep(1)
    mechanize = Mechanize.new
    ads_page = mechanize.get("http://kohls.shoplocal.com/Kohls/Entry/Locations/")
    puts "GOT LOCATION ADS PAGE"
    match_address = [store_address.split[0],store_address.split[1][0]].join(" ")
    leaflets_home_href_node = ads_page.search("//div[contains(@class, 'seolocationblock')]
                    [contains(text(), '#{match_address}')]
                    //a[contains(@class, 'allLocationsLink')]/@href")[0]
    if leaflets_home_href_node
      leaflets_home_href = leaflets_home_href_node.value
    else
      leaflets_home_href_node = ads_page.search("//div[contains(@class, 'seolocationblock')]
                    [contains(text(), '#{store_address.split[0].strip}')]
                    //a[contains(@class, 'allLocationsLink')]/@href")[0]
      if leaflets_home_href_node
        leaflets_home_href = leaflets_home_href_node.value
      else
        puts "NO LEAFLET FOUND FOR #{store_address} #{store_zipcode}"
        return leaflets
      end
    end
    leaflets_home_url = "#{base_url}#{leaflets_home_href}"
    sleep(2)
    mechanize = Mechanize.new
    leaflets_home_page = mechanize.get(leaflets_home_url)
    puts "GOT LEAFLETS HOME PAGE AT #{leaflets_home_url}"
    store_leaflets_href = leaflets_home_page.search("//div[@class='globalNavLinksContain']
                                                    //a[@data-tracking-origin='home']/@href")[0].value
    store_leaflets_url = "#{base_url}#{store_leaflets_href}"
    mechanize = Mechanize.new
    sleep(1)
    store_leaflets_page = mechanize.get(store_leaflets_url)
    puts "GOT LEAFLETS LIST PAGE AT #{store_leaflets_url}"
    store_leaflets_list = store_leaflets_page.search("//div[@class='landingUnit']")
    store_leaflets_list.each do |store_leaflet_item|
      store_leaflet_name = store_leaflet_item.search(".//div[@class='landingSubUnitB']//p[1]")[0].text
                                                                                                .strip
      leaflet_dates = store_leaflet_item.search(".//div[@class='landingSubUnitB']
                                                  //p[@class='validDates']")[0].text.sub("Valid", "")
                                                .sub("(except as noted)", "").strip
      leaflet_start_date = Time.parse(leaflet_dates.split("-")[0]).to_s.strip
      leaflet_end_date = Time.parse(leaflet_dates.split("-")[1]).to_s.strip
      store_leaflet_href = store_leaflet_item.search(".//a[@class='textDecorationNone']/@href")[0].value
      store_leaflet_url = "#{base_url}#{store_leaflet_href}"
      sleep(1)
      mechanize = Mechanize.new
      store_leaflet_page = mechanize.get(store_leaflet_url)
      puts "GOT LEAFLET ITEM PAGE AT #{store_leaflet_url}"
      script_text = store_leaflet_page.search("//script[contains(text(), 'pageElementsArray')]")[0].text
      script_text = script_text.gsub("//akimages.shoplocal.com", "http://akimages.shoplocal.com")
      script_urls = URI.extract(script_text)
      images_urls = []
      script_urls.map{|url| images_urls << url if url
                                   .start_with?("http://akimages.shoplocal.com") && url.end_with?(".jpg")}
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.image_urls
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.store_ids
      puts "**********"
      leaflets << leaflet
    end
    leaflets
  end
  
  
  def parse_hours(store_hours)
    week_hours = []
    store_hours.each do |store_hour|
      store_hour = store_hour.value.downcase.strip
      puts "GOING TO PARSE #{store_hour} NOW"
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
                      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end

  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mo")
      index = 0
    elsif day.include?("tu")
      index = 1
    elsif day.include?("we")
      index = 2
    elsif day.include?("th")
      index = 3  
    elsif day.include?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '89d97c4daee22c5fd2d3bbad75bd12f46bf3781f18b34f6e3ea90707b5c370ac'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores("http://www.kohls.com/stores.shtml")
    puts "JUST ENDED THE SITE DATA"
  end
  
end