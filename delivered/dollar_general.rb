#9783c006191cd427b03822cc1a1f2cc5859c5beadfb1eda1b8701feacfe7e3f0 for Dollar General
require 'pqsdk'
require 'mechanize'
require 'geocoder'
require 'uri'
require 'json'
require 'byebug'

class DollarGeneral
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    data = {"request"=>{"appkey"=>"9E9DE426-8151-11E4-AEAC-765055A65BB0", 
            "formdata"=>{"geoip"=>false, "dataview"=>"store_default", "limit"=>100000, 
            "geolocs"=>{"geoloc"=>[{"addressline"=>"", "country"=>"", "latitude"=>"", 
                  "longitude"=>""}]}, "searchradius"=>"30", 
            "where"=>{"or"=>{"PROPANE"=>{"eq"=>""}, "REDBOX"=>{"eq"=>""}, "RUGDR"=>{"eq"=>""}}}, 
            "false"=>"0"}}}
    stores_url = "http://hosted.where2getit.com/dollargeneral/rest/locatorsearch?like=0.17714691056557652&lang=fr_FR"
    mechanize = Mechanize.new
    cities_page = mechanize.get("https://api.ilikesales.com/v1/cities")
    puts "GOT STORES PAGE"
    cities = JSON.parse(cities_page.body)
#    cities = ["Los Angeles", "Buenos Aires", "texas", "massachussets"] # for testing
    cities.each do |city|
      mechanize = Mechanize.new
      sleep(1)
#      data["request"]["formdata"]["geolocs"]["geoloc"][0]["addressline"] = city # for testing
#      data["request"]["formdata"]["geolocs"]["geoloc"][0]["latitude"] = 34.0207504 # for testing
#      data["request"]["formdata"]["geolocs"]["geoloc"][0]["addressline"] = -118.6919324 # for testing
      next unless (city["state"].eql?("confirmed") && city["country"].eql?("USA"))
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["addressline"] = city["name"]
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["latitude"] = city["latitude"]
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["longitude"] = city["longitude"]
      store_results_page = mechanize.post stores_url, data.to_json, {'Content-Type' => 'application/json'}
      puts "GOT STORE PAGE FOR #{city["name"]}"
#      puts "GOT STORE PAGE FOR #{city}" # for testing
      stores_parsed = JSON.parse(store_results_page.body)
      next unless stores_parsed["response"]["collection"]
      stores_list = stores_parsed["response"]["collection"]
      stores_list.each do |store_item|
        store_clientkey = store_item["clientkey"]
        store_name = store_item["name"]
        store_address = store_item["address1"]
        store_city = store_item["city"]
        store_zipcode = store_item["postalcode"]
        store_phone = store_item["phone"]
        store_latitude = store_item["latitude"]
        store_longitude = store_item["longitude"]
        if store_address.nil? || store_address.to_s.empty?
          puts "STORE ADDRESS NOT FOUND. USING GEOCODER NOW"
          store_address = get_address_from_coord("#{store_latitude}, #{store_longitude}")
          next if store_address.empty?
          store_address = store_address.sub("#{store_city}", "").sub("#{store_zipcode}","").sub("USA","")
        end
        if store_name.nil? || store_name.to_s.empty?
          store_name = store_address
        end
        puts "ON #{store_address} NOW"
        opening_hours = parse_hours(store_item)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{stores_url}"
        puts "********"
        allStoresIds << store.id
        store_leaflets = get_leaflets(store.id, store_zipcode, store_clientkey)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      puts "\nGOING TO NEXT CITY NOW\n"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_address_from_coord(coord_string)
    geocodes = Geocoder.search(coord_string)
    lat = coord_string.split(",")[0].strip.to_f.round(3).to_s[0..-2]
    lng = coord_string.split(",")[1].strip.to_f.round(3).to_s[0..-2]
    address = ""
    geocodes.each do |geocode|
      if geocode.latitude.round(3).to_s[0..-2].include?(lat) && geocode.longitude.round(3).to_s[0..-2]
                                                                                        .include?(lng)
       address = geocode.address 
       break
      end
    end
    address
  end
  
  def get_leaflets(store_id, store_zipcode, store_clientkey)
    leaflets = []
    base_url = "http://weeklyads.dollargeneral.com"
    leaflets_list_url = "#{base_url}/flyers/dollargeneral?type=1&locale=en&postal_code=#{store_zipcode}&store_code=#{store_clientkey}&is_store_selection=true#!/flyers/dollargeneral-circular?flyer_run_id=119156"
    mechanize = Mechanize.new
    leaflets_list_page = mechanize.get(leaflets_list_url)
    puts "GOT LEAFLETS LIST PAGE #{leaflets_list_url}"
    store_leaflets_hrefs = leaflets_list_page.search("//table[@class='other_flyer_runs_table']
                                                      //td[@class='info']//a")
    store_leaflets_hrefs.each do |store_leaflet_href|
      store_leaflet_name = store_leaflet_href.search(".//span[@class='font-14 bold']")[0].text.strip
      leaflet_dates = store_leaflet_href.search("./text()")[-1].text.strip
      leaflet_start_date = Date.parse(leaflet_dates.split("-")[0].strip)
      leaflet_end_date = Date.parse(leaflet_dates.split("-")[1].strip)
      store_leaflet_url = "#{base_url}#{store_leaflet_href.search("./@href")[0].value}"
      sleep(1)
      mechanize = Mechanize.new
      store_leaflet_page = mechanize.get(store_leaflet_url)
      puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
      script_text = store_leaflet_page.search("//script[contains(text(), 'pdf_url')]")[0].text
      pdf_string = script_text.match(/\"pdf_url\":\".+pdf\"/).to_s
      leaflet_pdf_url = URI.extract(pdf_string)[0]
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = leaflet_pdf_url
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      #leaflet.save 
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      puts leaflet.start_date
      puts leaflet.end_date
      puts "*****"
      leaflets << leaflet
    end
    leaflets
  end
  
  def parse_hours(store_item)
    week_hours = []
    opening_time_sat = store_item["opening_time_sat"]
    opening_time_sun = store_item["opening_time_sun"]
    opening_time_mon = store_item["opening_time_mon"]
    opening_time_tue = store_item["opening_time_tue"]
    opening_time_wed = store_item["opening_time_wed"]
    opening_time_thu = store_item["opening_time_thu"]
    opening_time_fri = store_item["opening_time_fri"]
    
    closing_time_sat = store_item["closing_time_sat"]
    closing_time_sun = store_item["closing_time_sun"]
    closing_time_mon = store_item["closing_time_mon"]
    closing_time_tue = store_item["closing_time_tue"]
    closing_time_wed = store_item["closing_time_wed"]
    closing_time_thu = store_item["closing_time_thu"]
    closing_time_fri = store_item["closing_time_fri"]
    store_hour = {}
    store_hour["open_am"] = opening_time_mon if opening_time_mon && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_mon if opening_time_mon && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_mon if closing_time_mon && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_mon if closing_time_mon && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 0
      week_hours << store_hour
    end
    store_hour = {}
    store_hour["open_am"] = opening_time_tue if opening_time_tue && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_tue if opening_time_tue && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_tue if closing_time_tue && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_tue if closing_time_tue && get_am_pm(closing_time_mon).eql?("pm")
    store_hour["weekday"] = 0 if store_hour["open_am"] || store_hour["open_pm"] || 
                                 store_hour["close_am"] || store_hour["close_pm"]
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 1
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = opening_time_wed if opening_time_wed && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_wed if opening_time_wed && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_wed if closing_time_wed && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_wed if closing_time_wed && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 2
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = opening_time_thu if opening_time_thu && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_thu if opening_time_thu && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_thu if closing_time_thu && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_thu if closing_time_thu && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 3
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = opening_time_fri if opening_time_fri && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_fri if opening_time_fri && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_fri if closing_time_fri && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_fri if closing_time_fri && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 4
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = opening_time_sat if opening_time_sat && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_sat if opening_time_sat && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_sat if closing_time_sat && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_sat if closing_time_sat && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 5
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = opening_time_sun if opening_time_sun && get_am_pm(opening_time_mon).eql?("am")
    store_hour["open_pm"] = opening_time_sun if opening_time_sun && get_am_pm(opening_time_mon).eql?("pm")
    store_hour["close_am"] = closing_time_sun if closing_time_sun && get_am_pm(closing_time_mon).eql?("am")
    store_hour["close_pm"] = closing_time_sun if closing_time_sun && get_am_pm(closing_time_mon).eql?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 6
      week_hours << store_hour
    end
    store_hour = {}
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '9783c006191cd427b03822cc1a1f2cc5859c5beadfb1eda1b8701feacfe7e3f0'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES, AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
  end
end