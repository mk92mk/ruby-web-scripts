#4daaf9c897a266ea682674bad4f035edf947c7d22e63bbf9ac5f6ea04e07a446 for decicco
require 'pqsdk'
require 'mechanize'
require 'geocoder'


class DeCicco
  
  def get_stores(start_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    Geocoder::Configuration.timeout = 15
    mechanize = Mechanize.new
    sleep(2)
    base_page = mechanize.get(start_url)
    locations_list = base_page.search("//select[@name='drpStoreID']//option")
    locations_list.each do |location_item|
      next if location_item.text.match(/\s/).nil? 
      sleep(2)
      base_url = "http://deciccofoodmarket.inserts2online.com/"
      form_url = "#{base_url}storeReview.jsp"
      mechanize.redirect_ok = false
      form = base_page.form_with(:action => 'storeReview.jsp')
      form.drpStoreID = "#{location_item['value']}"
      redirect_page = form.submit
      code = ""
      option_address = ""
      code = redirect_page.code
      redirect_page = get_redirected_page(redirect_page, code, mechanize) if code.eql?("302")
      if redirect_page.uri.to_s.include?("I2O_MainFrame.jsp") # landed at right page without ads list
        store = get_store_info(redirect_page, mechanize, base_url)
        store_leaflet_href = redirect_page.search("//frame[@id='Large']/@src")[0].value
        store_leaflet_url = "#{base_url}#{store_leaflet_href}"
        store_leaflets = get_leaflets(store.name, store.id, store_leaflet_url, mechanize)
      elsif redirect_page.uri.to_s.include?("availableAds.jsp") # encountered ads list page
        leaflet_hrefs = redirect_page.search("//a[@class='aahref']/@href")
        leaflet_href = leaflet_hrefs[0] unless leaflet_hrefs.empty?
        leaflet_url = "#{base_url}#{leaflet_href.value}"
        leaflet_base_page = mechanize.get(leaflet_url)
        code = leaflet_base_page.code
        leaflet_base_page = get_redirected_page(leaflet_base_page, code, mechanize) if code.eql?("302")
        if leaflet_base_page.uri.to_s.include?("I2O_MainFrame.jsp") # store info page reached
          store = get_store_info(leaflet_base_page, mechanize, base_url)
        end
        leaflet_hrefs.each do |leaflet_href|
          leaflet_url = "#{base_url}#{leaflet_href.value}"
          leaflet_base_page = mechanize.get(leaflet_url)
          code = leaflet_base_page.code
          leaflet_base_page = get_redirected_page(leaflet_base_page, code, mechanize) if code.eql?("302")
          if leaflet_base_page.uri.to_s.include?("I2O_MainFrame.jsp")
            store_leaflet_href = leaflet_base_page.search("//frame[@id='Large']/@src")[0].value
            store_leaflet_url = "#{base_url}#{store_leaflet_href}"
            store_leaflets = get_leaflets(store.name, store.id, store_leaflet_url, mechanize)
          end
        end
      end
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      allStoresIds << store.id
      sleep(5)
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_store_info(info_page, mechanize, base_url)
    store_info_href = info_page.search("//frame[@id='navBar']/@src")
    store_leaflet_href = info_page.search("//frame[@id='Large']/@src")[0].value
    store_info_url = "#{base_url}#{store_info_href}"
    sleep(2)
    store_info_page = mechanize.get(store_info_url)
    store_city = store_info_page.search("//div[@class='small'][2]")[0].text.split(",")[0].strip
    store_name = store_city
    store_address = store_info_page.search("//div[@class='small'][1]")[0].text.strip
    store_zipcode = store_info_page.search("//div[@class='small'][2]")[0].text.split(",")[1].strip
    store_phone = store_info_page.search("//div[@class='small'][contains(./b,'Phone')]")[0]
    .text.gsub("Phone:","").strip
    temp_address = "#{store_info_page.search("//div[@class='small'][1]").text} #{store_info_page
    .search("//div[@class='small'][2]").text}"
    store_leaflet_url = "#{base_url}#{store_leaflet_href}"
    location_url = store_leaflet_url
    opening_hours = []
    store = PQSDK::Store.find store_address, store_zipcode
    if store.nil?
      geocode = Geocoder.search(temp_address)
      store_latitude = get_coord_from_address(geocode, "lat")
      store_longitude = get_coord_from_address(geocode, "lng")
      store = PQSDK::Store.new
      store.name = store_name
      store.address = store_address
      store.zipcode = store_zipcode
      store.city = store_city
      store.latitude = store_latitude
      store.longitude = store_longitude
      store.phone = store_phone
      store.origin = location_url
    end
    store.opening_hours = opening_hours
    #store.save
    puts  "*************STORE*********************"
    puts  "STORE NAME IS #{store.name}"
    puts  "STORE CITY IS #{store.city}"
    puts  "STORE ZIP CODE IS #{store.zipcode}"
    puts  "STORE ADDRESS IS #{store.address}"
    puts  "STORE LONGITUDE #{store.longitude}"
    puts  "STORE LATITUDE #{store.latitude}"
    puts  "STORE PHONE #{store.phone}"
    puts  "STORE HOURS #{store.opening_hours}"
    puts  "STORE URL #{store.origin}"
    store
  end
  
  
  def get_coord_from_address(geocode, coord)
    if geocode.size == 0
      return nil
    end
    latitude = geocode[0].data["geometry"]["location"]["lat"]
    longitude = geocode[0].data["geometry"]["location"]["lng"]
    return latitude if coord.eql?("lat")
    return longitude if coord.eql?("lng")
  end
  
  
  def get_leaflets(store_name, store_id, store_leaflet_url, mechanize)
    storeLeaflets = []
    base_url = "http://deciccofoodmarket.inserts2online.com/"
    leaflets_url = "#{base_url}storeLocator.jsp?id="
    sleep(2)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    pdf_base_url = "#{base_url}version_pdf.jsp"
    pdf_page = mechanize.get(pdf_base_url)
    code = pdf_page.code
    pdf_url = get_redirected_page(pdf_page, code, mechanize, true) if code.eql?("302")
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = pdf_url
      leaflet.name = store_leaflet_name
    end
    leaflet.name = "#{store_name}_#{leaflet.name}" unless leaflet.name.include?("#{store_name}")
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    storeLeaflets << leaflet
    storeLeaflets
    storeLeaflets
  end
  
  def get_redirected_page(redirect_page, code, mechanize, pdf_flag = false)
    begin
      if code.eql?("302")
        redirect_url = redirect_page.response['location']
        if pdf_flag == true
          circular_start = "http://deciccofoodmarket.inserts2online.com/KrasdaleDeCiccoWeeklyCircular"
          return redirect_url if redirect_url.start_with?(circular_start) && redirect_url.end_with?(".pdf")
        end
        mechanize.redirect_ok = false
        sleep(2)
        redirect_page = mechanize.get(redirect_url)
        code = redirect_page.code
      end
    end while !code.eql?("200")
    redirect_page
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    #    PQSDK::Settings.app_secret = '4daaf9c897a26an[@6ea682674bad4f035edf947c7d22e63bbf9ac5f6ea04e07a446'
    PQSDK::Settings.app_secret = 'a762f90e98d0698c1faa0ea597f15c17c246949d75fb0c4daa3574c2545da400'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids = get_stores("http://deciccofoodmarket.inserts2online.com/storeLocator.jsp?id=")
    puts "JUST ENDED THE SITE DATA"
  end
end