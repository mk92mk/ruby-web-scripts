#ef3d6f465842fcc449ceefc691aec35eb65284512376cd3ec93ecabaabfa24a6 FOR CYCLEGEAR
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class CycleGear
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    stores_form_url = "http://www.cyclegear.com/store-finder"
    mechanize = Mechanize.new
    mechanize.user_agent_alias = "Windows Mozilla"
    stores_form_page = mechanize.get(stores_form_url)
    puts "GOT STORES FORM PAGE AT #{stores_form_url}"
    states_values = stores_form_page.search("//select[@id='state']//option/@value")
    csrf_token = stores_form_page.search("//input[@name='CSRFToken']/@value")[0].value
    stores_post_url = "http://www.cyclegear.com/store-finder/findStore"
    states_values.each do |state_value|
      next if state_value.value.empty?
      sleep(1)
      mechanize = Mechanize.new
      stores_form_page = mechanize.get(stores_form_url)
      puts "GOT STORES FORM PAGE FOR #{state_value.value}"
      csrf_token = stores_form_page.search("//input[@name='CSRFToken']/@value")[0].value
      sleep(1)
      stores_results_page = mechanize.post(stores_post_url, 
                                            {"state" => state_value.value, "city" => "", "q" => "",
                                             "CSRFToken" => csrf_token})
#      stores_results_page = mechanize.post(stores_post_url, 
#                                            {"state" => "AZ", "city" => "", "q" => "",
#                                             "CSRFToken" => csrf_token})                                         
      puts "GOT STORES RESULTS PAGE"
      stores_items = stores_results_page.search("//div[@class='results']//div[@class='row']")
      stores_items.each do |store_item|
        store_name = store_item.search(".//div[@class='store_name']/a")[0].text
        store_address = store_item.search(".//td[@class='col_address']")[0].text.strip.lines[0].strip
        store_city = store_item.search(".//td[@class='col_address']")[0].text.strip.lines[-2]
                                                                        .split(",")[0].strip
        store_zipcode = store_item.search(".//td[@class='col_address']")[0].text.strip.lines[-1].strip
        store_phone = store_item.search(".//div[@class='store_number']")[0].text.strip
        store_href = store_item.search(".//div[@class='store_name']/a/@href")[0].value
        store_latitude = store_href.scan(/lat=\d+.\d+|lat=-\d+.\d+/)[0].sub("lat=", "")
        store_longitude = store_href.scan(/long=\d+.\d+|long=-\d+.\d+/)[0].sub("long=", "")
        store_hours = store_item.search(".//tr[@class='weekday_openings']")
        puts "AT #{store_address} NOW"
        opening_hours = parse_hours(store_hours)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude if store_latitude
          store.longitude = store_longitude if store_longitude
          store.phone = store_phone
          store.origin = stores_post_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}" if store_longitude
        puts  "STORE LATITUDE #{store_latitude}" if store_latitude
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{stores_post_url}"
        puts "******"
        allStoresIds << store.id
        store_leaflets = get_leaflets(store.id, store_zipcode)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      puts "GOING TO NEXT STATE"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode)
    leaflets = []
    store_leaflet_url = "http://www.circularhub.com/flyers/cyclegear?flyer_run_id=97116&type=2&postal_code=#{store_zipcode}&filler=&is_postal_entry=true"
    sleep(1)
    mechanize = Mechanize.new
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORES LEAFLET PAGE AT #{store_leaflet_url}"
    store_leaflet_name = store_leaflet_page.search("//title")[0].text.split("-")[0].strip
    store_leaflet_dates = store_leaflet_page.search("//title")[0].text.split("-")[1]
    leaflet_start_date = Time.parse(store_leaflet_dates.split("to")[0])
    leaflet_end_date = Time.parse(store_leaflet_dates.split("to")[1])
    script_text = store_leaflet_page.search("//script[contains(text(), 'pdf_url')]")[0].text
    pdf_text = script_text.match(/\"pdf_url\":\".+.pdf\"/).to_s
    leaflet_pdf_url = URI.extract(pdf_text)[0]
    puts "DOWNLOAD FROM #{leaflet_pdf_url}"
#    @report.info "DOWNLOAD FROM #{pdf_url}"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = leaflet_pdf_url
      leaflet.name = store_leaflet_name
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    leaflets << leaflet
    puts "********"
    leaflets    
  end
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      week_days = store_hour_node.search(".//td[@class='weekday_openings_day']")[0].text.downcase.strip
      open_hours = store_hour_node.search(".//td[@class='weekday_openings_times']")[0].text.downcase.strip
      puts "GOING TO PARSE #{week_days} AND #{open_hours} NOW"
#      @logger.info "GOING TO PARSE #{week_days} AND #{open_hours} NOW"
      days = week_days.split("-")
      days = week_days.split("–") if week_days.include?("–")
      days = week_days.split("to") if week_days.include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
                                .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").strip) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").strip) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").strip) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").strip) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").strip) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").strip) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").strip) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").strip) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").strip) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").strip) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").strip) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").strip) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'ef3d6f465842fcc449ceefc691aec35eb65284512376cd3ec93ecabaabfa24a6'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
#    @logger.info "IN RUN METHOD"
#    @logger.info "GOING TO COLLECT THE STORES"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
#    @logger.info "JUST ENDED THE SITE DATA"
  end
end
