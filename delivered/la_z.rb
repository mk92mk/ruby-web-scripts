#86d7b999610db45b1a3290f6ccb34ac2f73d3d9d4ba1265d4459f2e33dd3f7ee for LaZ
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class LazBoy
  
  def get_stores(store_form_url)
    allStoresIds = []
    base_url = "http://www.la-z-boy.com"
    mechanize = Mechanize.new
    cities_page = mechanize.get("https://api.ilikesales.com/v1/cities")
    puts "GOT CITIES PAGE"
    cities = JSON.parse(cities_page.body)
    mechanize = Mechanize.new
    stores_form_page = mechanize.get(store_form_url)
    puts "GOT STORES FORM PAGE"
#    cities = ["florida", "massachussets", "texas", "washington"] # for testing line
    cities.each do |city|
      store_form = stores_form_page.form_with(:id => "storeLocator")
      address_field = store_form.field_with(:id => "locator")
      address_field.value = city["name"]
      button = store_form.button_with(:id => "findStore")
      sleep(1)
      mechanize = Mechanize.new
      store_results_page = mechanize.submit(store_form, button)
      puts "GOT STORE RESULTS PAGE FOR #{city["name"]}"
      blank_message = store_results_page.search("//p[@class='message-txt'][contains(text(), 
                                                'No Stores Found')]")[0]
      if blank_message
        puts "NO STORES FOUND FOR #{city["name"]}"
        next
      end
      stores_text = store_results_page.search("//script[contains(text(), 'var __locatorResults')]")[0].text
      stores_list = stores_text[stores_text.index("\"stores\" :")..-1].sub("\"stores\" :", "").strip
                                                                  .chomp("]").strip.chomp("}").strip
      unless stores_list.match(/\"[A-Z]\"/).to_s.empty?
        ("A".."Z").each do |alpha|
          stores_list = stores_list.gsub("\"#{alpha}\"", "#{alpha}")
        end
      end
      stores_parsed = JSON.parse(stores_list)
      stores_parsed.each do |store_item|
        store_name = store_item["storename"]
        store_address = store_item["address1"]
        store_city = store_item["city"]
        store_zipcode = store_item["zip"]
        store_phone = store_item["phone"]
        store_latitude = store_item["lat"]
        store_longitude = store_item["longi"]
        store_info_url = store_item["website"]
        opening_hours = []
        puts "ON #{store_address} NOW"
        unless store_info_url.empty? || store_info_url.include?("http")
          puts "GOING TO STORE HOURS PAGE"
          store_details_url = "#{base_url}#{store_info_url}Store-Info"
          sleep(1)
          mechanize = Mechanize.new
          store_details_page = mechanize.get(store_details_url)
          puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
          store_hours_rows = store_details_page.search("//ul[@class='hours']//li")
          opening_hours = parse_hours(store_hours_rows)
        end
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_results_page.uri.to_s
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_results_page.uri.to_s}"
        puts "******"
        allStoresIds << store.id
      end
      puts "\n GOING TO NEXT CITY"
    end
    allStoresIds
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    sleep(2)
    leaflets_page = mechanize.get("http://www.la-z-boy.com/content/CustomerCare/catalogs/")
    leaflets_list = leaflets_page.search("//div[@class='pic-download-pdf']//li")
    leaflets_list.each do |leaflet_item|
      store_leaflet_name = leaflet_item.search(".//a")[0].text.sub("Download the", "")
      leaflet_pdf_url = leaflet_item.search(".//a/@href")[0].value
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = leaflet_pdf_url
        leaflet.store_ids = store_ids
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name.strip
      puts leaflet.url
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.store_ids
      puts "********"
      allStoresLeafletsIds << leaflet.id
    end
    allStoresLeafletsIds
  end
  
  def parse_hours(store_hours_rows)
    week_hours = []
    store_hours_rows.each do |store_hour_row|
      puts "GOT #{store_hour_row.text} TO PARSE"
      store_hour = store_hour_row.text.strip.downcase
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
              .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "").sub("-", "").sub(".", ":")}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
            .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
            .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end    
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '86d7b999610db45b1a3290f6ccb34ac2f73d3d9d4ba1265d4459f2e33dd3f7ee'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids= get_stores("http://www.la-z-boy.com/storeLocator/storeLocator.jsp")
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end
end