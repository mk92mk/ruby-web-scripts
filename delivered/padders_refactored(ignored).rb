require 'mechanize'
require 'json'
require 'byebug'
require 'pqsdk'
require 'geocoder'

class Crawler::Uk::Padders < Crawler::Base
  
  def get_stores
    allStores = []
    start_url = "http://www.padders.co.uk/storelocator"
    base_url = "http://www.padders.co.uk/storesearch.asp?region="
    country = "United Kingdom"
    Geocoder::Configuration.timeout = 15 # for handling geocoding api not responding fast enough
    mechanize = Mechanize.new
    start_page = mechanize.get(start_url)
    options_list = start_page.search("//select[@name='storeregion']//option")
    options_list.each do |option|
      option_value = option["value"]
      next if option_value.empty? || option_value.start_with?("-----") || 
              option_value.downcase.eql?("international")
      stores_url = "#{base_url}#{option_value}"
      mechanize = Mechanize.new
      sleep(10)
      stores_page = mechanize.get(stores_url)
      @report.info << "GOT THE PAGE FOR #{option_value}"
      stores_list = stores_page.search("//div[@class='storebox']")
      stores_list.each do |store_item|
        store_name = store_item.search(".//strong")[0].text
        store_info_xml = store_item.search(".//p")[0]
        address_parts = []
        store_phone = ""
        store_info_xml.children.each do |store_info_child|
          next if store_info_child.name.eql?("span") || store_info_child.name.eql?("br") || 
            store_info_child.name.eql?("a")
          child_text = store_info_child.text
          address_parts << "#{child_text.strip}" unless child_text.strip.start_with?("Tel:")
          store_phone = child_text.strip.gsub("Tel:", "") if child_text.strip.start_with?("Tel:")
        end
        temp_address = address_parts.join(" ")
        store_latitude = get_coord_from_address(temp_address, "lat").to_s
        if store_latitude.empty?
          temp_address2 = "#{address_parts[1..-1].join(" ")} #{country}"
          store_latitude = get_coord_from_address(temp_address2, "lat").to_s
          if store_latitude.empty?
            @report.info << "--------SKIPPING STORE #{store_name} DIDN'T GET LATITUDE------------"
            next
          end
        end
        store_longitude = get_coord_from_address(temp_address, "lng").to_s
        if store_longitude.empty?
          temp_address2 = "#{address_parts[1..-1].join(" ")} #{country}"
          store_longitude = get_coord_from_address(temp_address2, "lng").to_s
          if store_longitude.empty?
            @report.info << "-------SKIPPING STORE #{store_name} DIDN'T GET LONGITUDE-------------"
            next
          end
        end
        store_zipcode = get_zipcode(temp_address).to_s
        if store_zipcode.empty?
          temp_address2 = "#{address_parts[1..-1].join(" ")} #{country}"
          store_zipcode = get_zipcode(temp_address2).to_s
          if store_zipcode.empty?
            store_zipcode = "00000"
          end
        end
        store_city = get_city_from_address(temp_address).to_s
        if store_city.empty?
          temp_address2 = "#{address_parts[1..-1].join(" ")} #{country}".to_s
          store_city = get_city_from_address(temp_address2).to_s
          if store_city.empty?
            @report.info << "++++DIDN'T GET CITY FROM GEOCODER SO ASSIGNING LIST OPTION AS CITY++++"
            store_city = option_value
          end
        end
        opening_hours = []
        store_address = address_parts.join(" ")
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        store.save
        @report.info << "*************STORE*********************"
        @report.info << "STORE NAME #{store_name}"
        @report.info << "STORE ADDRESS: #{store_address}"
        @report.info << "STORE CITY #{store_city}"
        @report.info << "STORE ZIPCODE #{store_zipcode}"
        @report.info << "STORE LONGITUDE #{store_longitude}"
        @report.info << "STORE LATITUDE #{store_latitude}"
        @report.info << "STORE PHONE #{store_phone}"
        @report.info << "STORE HOURS #{opening_hours}"
        @report.info << "STORE URL #{stores_url}"
      end
    end
  end  
  
  def get_coord_from_address(address, coord)
    geocode = Geocoder.search(address)
    if geocode.size == 0
      return nil
    end
    latitude = geocode[0].data["geometry"]["location"]["lat"]
    longitude = geocode[0].data["geometry"]["location"]["lng"]
    return latitude if coord.eql?("lat")
    return longitude if coord.eql?("lng")
  end
  
  def get_city_from_address(address)
    if address.nil?
      return nil
    end
    geocode = Geocoder.search(address)
    if geocode.size == 0
      return nil
    end
    geocode = geocode.first.data['address_components']
    city = nil
    geocode.each do |element|
      if element['types'].first == 'locality'
        city = element['long_name']
      end
    end
    city
  end

  def get_zipcode(address)
    geocode = Geocoder.search(address)
    if geocode.size == 0
      return nil
    end
    zip_code = geocode[0].postal_code
    zip_code
  end
  
  def reset_mechanize
    mechanize ||= Mechanize.new do |m|
     m=m.set_proxy("107.151.152.210", 80)
    end
    mechanize
  end
  
  def get_leaflets(store_ids, leaflets_url)
    base_url = "http://padders.co.uk"
    allLeaflets = []
    mechanize = Mechanize.new
    sleep(10)
    brochure_page = mechanize.get(leaflets_url)
    brochure_href = brochure_page.search("//p[contains(text(),'To view our Padders brochure, check out our online Look Book by')]//a/@href")[0].value
    brochure_url = "#{base_url}#{brochure_href}"
    brochure_href = brochure_href.gsub("index.html","files/page/")
    status_code = "200"
    image_urls=[]
    page_number = 1
    temp = true
    begin
      leaflet_image_url = "#{base_url}#{brochure_href}#{page_number}.jpg"
      mechanize = Mechanize.new
      sleep(3)
      begin
      image_page = mechanize.get(leaflet_image_url)
      rescue Exception => e
        case e.message
          when /404/ then @report.info << "End of brochure reached "
            temp=false
            break
        end
      end
      status_code = image_page.code
      if status_code.eql?("200")
        image_urls << leaflet_image_url
        @report.info << "#{leaflet_image_url} added in main array"
      end
      page_number = page_number + 1
    end while temp
    
    leaflet_name = "Padders Leaflet"
    leaflet = PQSDK::Leaflet.find brochure_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = leaflet_name
      leaflet.url = brochure_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = Time.now.to_s        
      leaflet.image_urls = image_urls
      leaflet.store_ids = store_ids
      leaflet.save   
      allLeaflets << leaflet     
      @report.info << "********LEAFLET***START******"
      @report.info << leaflet.name
      @report.info << "********LEAFLET END********"
    end
    @report.info << "Leaflet created"
    allLeaflets
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '527cab2721d977d8d7cf1748e7b1e5c9d09e89806b6377e0741f8b22a07515ca'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES AND LEAFLETS DATA"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.padders.co.uk/brochure")
    @report.info << "JUST ENDED THE SITE DATA"
  end
end