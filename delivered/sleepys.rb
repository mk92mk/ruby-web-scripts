#7052a5a42d68d0e7d09e81295c529c2a9e278a47f8f57d487fd157856ed6d0fe for Sleepys
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class Sleepys
  
  def get_stores(stores_list_url)
    allStoresIds = []
    base_url = "http://store.sleepys.com"
    mechanize = Mechanize.new
    sleep(1)
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST OF USA"
    pagination_link = stores_list_page.search("//a[@class='results-pagination-link']/@href")[-1].value
    last_page_number = pagination_link.match(/page=\d+/).to_s.match(/\d+/).to_s.to_i
    (2..last_page_number).each do |page_number|
      stores_list = stores_list_page.search("//div[contains(@class,'results-pos col-xs-10')]")
      stores_list.each do |store_item|
        store_name = store_item.search(".//a[@class='results-pos-name-link']")[0].text.strip
        temp_address = store_item.search(".//div[@itemprop='streetAddress']")[0].text.strip.lines
                                                                                  .map{|l| l.strip}
        temp_address.delete("")
        store_address = temp_address.join(" ")
        store_city = store_item.search(".//span[@itemprop='addressLocality']")[0].text.strip
        store_zipcode = store_item.search(".//span[@itemprop='postalCode']")[0].text.strip
        store_details_href = store_item.search(".//a[@class='results-pos-name-link']/@href")[0].value
        store_details_url = "#{base_url}#{store_details_href}"
        sleep(1)
        mechanize = Mechanize.new
        store_details_page = mechanize.get(store_details_url)
        puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
        store_phone = store_details_page.search("//span[@itemprop='telephone']")[0].text.strip
        store_latitude = store_details_page.search("//meta[@itemprop='latitude']/@content")[0].value
        store_longitude = store_details_page.search("//meta[@itemprop='longitude']/@content")[0].value
        working_hours = store_details_page.search("//meta[@itemprop='openingHours']/@content")[0].value
        opening_hours = parse_hours(working_hours)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_list_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{stores_list_url}"
        puts "******"
        allStoresIds << store.id
      end
      puts "CURRENT PAGE ENDED"
      stores_list_url = "#{base_url}/us?page=#{page_number}"
      sleep(2)
      mechanize = Mechanize.new
      stores_list_page = mechanize.get(stores_list_url)
      puts "GOT NEXT PAGE AT #{stores_list_url}"
    end
    
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    sleep(2)
    store_leaflet_url = "http://www.sleepys.com/store-ad?fdid=store-ad"
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
    image_url = store_leaflet_page.search("//img[@alt='Store Ad']/@src")[0].value
    store_leaflet_name = store_leaflet_page.search("//img[@alt='Store Ad']/@title")[0].value
    images_urls = [image_url]
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = store_leaflet_url
      leaflet.image_urls = images_urls
      leaflet.store_ids = store_ids
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.image_urls
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    allStoresLeafletsIds << leaflet.id
    allStoresLeafletsIds
  end
  
  def parse_hours(working_hours)
    week_hours = []
    puts "GOT #{working_hours} TO PARSE"
    store_hours = working_hours.split(",")
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase.strip
      puts "GOING TO PARSE #{store_hour} NOW"
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
                      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end

  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mo")
      index = 0
    elsif day.include?("tu")
      index = 1
    elsif day.include?("we")
      index = 2
    elsif day.include?("th")
      index = 3  
    elsif day.include?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '7052a5a42d68d0e7d09e81295c529c2a9e278a47f8f57d487fd157856ed6d0fe'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    #    @report.info "IN RUN METHOD"
    #    @report.info "GOING TO COLLECT THE STORES"
    store_ids= get_stores("http://store.sleepys.com/us")
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
    #    @report.info "GOING TO COLLECT THE LEAFLETS"
    #    @report.info "JUST ENDED THE SITE DATA"
  end
end