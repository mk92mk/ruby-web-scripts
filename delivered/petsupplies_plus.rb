#8550d9de761a5314f58c4bdab85b9214c424c0394c8691baeb8499e3ffa87e26 for PET SUPPLIES PLUS
require 'pqsdk'
require 'mechanize'


class PetSuppliesPlus
  
  def initialize
    @report = Logger.new("PetSupplies_Plus_#{Time.now.to_s}.log")
  end
  
  def get_stores(start_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    start_page = mechanize.get(start_url)
    puts "GOT START PAGE #{start_url}"
    @report.info "GOT START PAGE #{start_url}"
    base_url = "http://www.petsuppliesplus.com/custserv/locate_store.cmd?radius=50&cityStateZip=%s&latitude=%s&longitude=%s#"
#    cities_names = ["chicago",  "houston", "new york"]
    cities = PQSDK::City.all
    cities.each do |city|
      puts "GOING TO FIND OUT STORES FOR #{city.name}"
      @report.info "GOING TO FIND OUT STORES FOR #{city.name}"
      stores_list_url = base_url %[city.name, city.latitude, city.longitude]
      puts "GOING TO GET THE STORES LIST ON #{stores_list_url}"
      @report.info "GOING TO GET THE STORES LIST ON #{stores_list_url}"
      sleep(5)
      mechanize = Mechanize.new
      stores_list_page = mechanize.get(stores_list_url)
      stores_lists = stores_list_page.search("//div[@class='results']//div[@class='row']")
      store_details_url = stores_list_page.uri.to_s
      if stores_lists.empty?
        puts "NO STORE FOUND FOR #{city.name}"
        @report.info "NO STORE FOUND FOR #{city.name}"
        next
      end
      stores_lists.each do |store_item|
        temp_address = store_item.search(".//div[@class='four columns store-address']/text()")
                                                                                      .text.strip
        address_parts = temp_address.lines
        store_name = address_parts[0].strip
        store_address = store_name
        store_city = address_parts[1].split(",")[0].strip
        if address_parts[1].include?(",") && address_parts[1].strip.scan(/\d+/).first.length > 3
          store_zipcode = address_parts[1].split(",")[1].scan(/\d+/).first 
        elsif address_parts[2]
          store_zipcode = address_parts[2].split(",")[1].scan(/\d+/).first 
        else
          store_zipcode = "00000"
        end
        store_phone = store_item.search(".//a[@class='telephone-link']")[0].text.strip
        coords_script = stores_list_page.search("//script[contains(./text(),'var allStores = []')]")[0]
                                                                                                 .text
        address_index = coords_script.index(store_address)
        temp_script = coords_script[address_index..-1].split("allStores[")[0]
        coords_index = temp_script.index("store.LATITUDE")
        coords_text = temp_script[coords_index..coords_index+100]
        coords = coords_text.scan(/\d+.\d+|-\d+.\d+/)
        store_latitude = coords[0]
        store_longitude = coords[1]
        opening_hours_spans = store_item.search(".//div[@class='four columns store-hours']//span")
        opening_hours = parse_hours(opening_hours_spans)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_details_url
        end
        store.opening_hours = opening_hours
        #store.save          
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_details_url}"
        @report.info  "*************STORE*********************"
        @report.info  "STORE NAME IS #{store_name}"
        @report.info  "STORE CITY IS #{store_city}"
        @report.info  "STORE ZIP CODE IS #{store_zipcode}"
        @report.info  "STORE ADDRESS IS #{store_address}"
        @report.info  "STORE LONGITUDE #{store_longitude}"
        @report.info  "STORE LATITUDE #{store_latitude}"
        @report.info  "STORE PHONE #{store_phone}"
        @report.info  "STORE HOURS #{opening_hours}"
        @report.info  "STORE URL #{store_details_url}"
        mechanize = Mechanize.new
        mechanize.redirect_ok = false
        sleep(5)
        begin
          leaflet_name_page = mechanize.post("http://petsuppliesplus.com/custserv/find_store_flyer.cmd",
            {"form_state" => "locateStoreFormLightBox", "radius" => "50", "cityStateZip" => store_city})
        rescue Exception => e
          case e.message
          when /404/ then puts "404! LEAFLET NOT FOUND FOR #{store_city}"
            @report.info "404! LEAFLET NOT FOUND FOR #{store_city}"
            puts "SO TRYING ADDRESS AND CITY"
            @report.info "SO TRYING ADDRESS AND CITY"
            mechanize = Mechanize.new
            sleep(5)
            leaflet_name_page = mechanize.post("http://petsuppliesplus.com/custserv/find_store_flyer.cmd",
            {"form_state" => "locateStoreFormLightBox", "radius" => "50", 
              "cityStateZip" => "#{store_address} #{store_city}"})
            
          else puts "SOME OTHER EXCEPTION GETTING LEAFLET NAME"
            @report.info "SOME OTHER EXCEPTION GETTING LEAFLET NAME"
          end
        end
        leaflet_name = leaflet_name_page.body
        puts "GOT LEAFLET NAME #{leaflet_name}"
        @report.info "GOT LEAFLET NAME #{leaflet_name}"
        store_leaflets = get_leaflets(leaflet_name, store.id)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
        allStoresIds << store.id
      end
      puts "STORES FOR CITY #{city} FINISHED. GOING TO NEXT CITY"
      @report.info "STORES FOR CITY #{city} FINISHED. GOING TO NEXT CITY"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(leaflet_name, store_id)
    leaflets = []
    base_url = "http://petsuppliesplus.com/custserv/flyer_flipbook.jsp?pageName="
    pdf_base_url = "http://petsuppliesplus.com"
    leaflet_fetch_url = "#{base_url}#{leaflet_name}"
    mechanize = Mechanize.new
    sleep(5)
    leaflet_page = mechanize.get(leaflet_fetch_url)
    pdf_href = leaflet_page.search("//p[@class='pdf-link']//a/@href")[0].value
    pdf_url = "#{pdf_base_url}#{pdf_href}"
    puts "DOWNLOAD FROM #{pdf_url}"
    @report.info "DOWNLOAD FROM #{pdf_url}"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = pdf_url
      leaflet.name = leaflet_name
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "*******************LEAFLET"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    @report.info "*******************LEAFLET"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(opening_hours_spans)
    week_hours = []
    opening_hours_spans.each_with_index do |hour_span, index|
      span_text = hour_span.text.strip.downcase
      puts "GOT #{span_text} IN THE SPAN TO PARSE"
      @report.info "GOT #{span_text} IN THE SPAN TO PARSE"
      if span_text.include?("mon") || span_text.include?("tue") || 
          span_text.include?("wed") || span_text.include?("thu") ||
          span_text.include?("fri") || span_text.include?("sat") ||
          span_text.include?("sun")
        temp_days = span_text
        open_hours = opening_hours_spans[index+1].text.strip.downcase if opening_hours_spans[index+1]
        days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
          !temp_days.include?("to") && !temp_days.include?("–")
        days = temp_days.split("-")  if temp_days.include?("-")
        days = temp_days.split("to") if temp_days.include?("to")
        days = temp_days.split("–") if temp_days.include?("–")
        days = temp_days.split.map{|d| d.strip} unless days
        days = days.map{|d| d.strip}
        days.delete("")
        hours = open_hours.split("-")
        hours = open_hours.split(" - ") if open_hours.include?(" - ")
        hours = open_hours.split(" to ") if open_hours.include?(" to ")
        hours = open_hours.split("–") if open_hours.include?("–")
        hours = hours.map{|h| h.strip}
        return hours if hours.empty?
        if days.length == 2 # > 1(before)
          starting_day = days[0]
          ending_day = days[1]
          starting_day_index = get_day_index(starting_day)
          ending_day_index = get_day_index(ending_day)
          if starting_day_index - ending_day_index == 1
            (0..6).each do |n|
              day_hours = {}
              day_hours["weekday"] = n
              formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".", "")) if  hours[0]
              .include?("am")
              formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
              .include?("pm")
              formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".", "")) if hours[1]
              .include?("am")
              formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
              .include?("pm")
              day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
              day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
              day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
              day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
              day_hours["closed"] = true if open_hours.include?("Closed")
              week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
            end              
          else
            (starting_day_index..ending_day_index).each do |n|
              day_hours = {}
              day_hours["weekday"] = n
              formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".","")) if  hours[0]
              .include?("am")||hours[0].include?("a.m")||hours[0].include?("a.m.")
              formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
              .include?("pm")||hours[0].include?("p.m.")
              formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".","")) if hours[1]
              .include?("am")||hours[1].include?("a.m")||hours[1].include?("a.m.")
              formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
              .include?("pm")||hours[1].include?("p.m.")
              day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
              day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
              day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
              day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
              day_hours["closed"] = true if open_hours.include?("Closed")
              week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
            end
          end
        elsif days.length == 1
          day_index = get_day_index(days[0])
          day_hours = {}
          day_hours["weekday"] = day_index
          formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m", "").sub(".","")) if  hours[0]
          .include?("am")
          formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.", "")) if hours[0]
          .include?("pm")
          formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m", "").sub(".","")) if hours[1]
          .include?("am")
          formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
          .include?("pm")
          day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
          day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
          day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
          day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
          day_hours["closed"] = true if open_hours.include?("Closed")
          week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
        end
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end

  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '8550d9de761a5314f58c4bdab85b9214c424c0394c8691baeb8499e3ffa87e26'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.petsuppliesplus.com/")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end