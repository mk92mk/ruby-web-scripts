require 'mechanize'
require 'json'
require 'byebug'
require 'pqsdk'

#class Crawler::Uk::ShopRite < Crawler::Base
class ShopRite
  
  def initialize
    @report = Logger.new("shoprite_#{Time.now.to_s}.log")
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  
  def get_stores
    allStores = []
    allStoresLeaflets = []
    base_url = "http://plan.shoprite.com"
    start_url = "#{base_url}/Stores?f=Any&mobile=0"
    stores_base_url1 = "#{base_url}/Stores/Get?Country=United%20States&Region="
    stores_base_url2 = "&StoreType=Any&StoresPageSize=undefined&IsShortList=undefined&_=1453235123002"
    mechanize = Mechanize.new
    sleep(10)
    start_page = mechanize.get(start_url)
    states_options = start_page.search("//select[@id='StateDropDown']//option")
    states_options.shift
    states_and_cities = []    
    states_options.each do |state_element|
      state_cities = {}      
      state_name = state_element.text
      state_stores_url = "#{stores_base_url1}#{state_name}#{stores_base_url2}"
      mechanize = Mechanize.new
      sleep(10)
      stores_page = mechanize.get(state_stores_url)
      @report.info "GOT THE PAGE FOR #{state_name}"
      stores_list = stores_page.search(".//div[contains(@class,'store-item')]")
      stores_list.each do |store_item|
        store = {}
        store_info_div = store_item.search(".//div[@class='storelist-info-text']")
        store_name = store_info_div.search("./h4")[0].text.strip
        store_address = store_info_div.search("./p")[0].text
        store_city_zipcode = store_info_div.search("./p")[1].text
        store_city = store_city_zipcode.split(",")[0]
        store_zipcode = store_city_zipcode.split.last
        store_phone = store_info_div.search(".//span[contains(text(),'Phone:')]").text
        store_phone.slice! "Phone: "
        store_phone = store_phone
        store_longitude = store_item['data-lng']
        store_latitude = store_item['data-lat']
        circular_href_node = store_info_div.search(".//a[contains(text(), 'Weekly Circular')]")
        circular_href = store_info_div.search(".//a[contains(text(),
                                    'Weekly Circular')]")[0]["data-outboundhref"]  unless circular_href_node.empty? 
        main_circular_url = "#{base_url}#{circular_href}" 
        store_hours_xml = store_item.search(".//div[@id='StoreServicesContainer']//span")[0]
        opening_hours = parse_hours(store_hours_xml)
        @report.info "*************STORE*********************"
        @report.info "STORE NAME #{store_name}"
        @report.info  "STORE ADDRESS #{store_address}"
        @report.info  "STORE CITY #{store_city}"
        @report.info  "STORE ZIPCODE #{store_zipcode}"
        @report.info  "STORE LONGITUDE #{store_longitude}"
        @report.info  "STORE LATITUDE #{store_latitude}"
        @report.info  "STORE PHONE #{store_phone}"
        @report.info  "STORE HOURS #{opening_hours}"
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = URI.escape(state_stores_url)
        end
        store.opening_hours = opening_hours
#        store.save
        store_leaflets = get_weekly_circulars(base_url, store_name, store.id, main_circular_url) if main_circular_url
        store_leaflets.each do |store_leaflet|
          allStoresLeaflets << store_leaflet.id
        end
        allStores << store
      end
    end
    return allStores, allStoresLeaflets
  end
  
  def reset_mechanize
    mechanize ||= Mechanize.new do |m|
     m=m.set_proxy("107.151.152.210", 80)
    end
    mechanize
  end
  
  def parse_hours(hours_xml)
    store_hours = []
    if hours_xml
      hours_xml.children.each_with_index do |hour_child, index|
        child_text = hour_child.text.strip
        #break if child_text.include?("----")
        next if child_text.empty?
        if child_text.eql?("24 hours")
          child_text = "Mon-Sun 24 hours"
        end
        digit_start = /\d+/.match(child_text)
        unless digit_start.nil?
          start_digit = digit_start.to_s
          start_digit_index = child_text.index(start_digit)
          if start_digit_index  == 0 && (child_text.downcase.include?("sun") ||
                                               child_text.downcase.include?("sa")) #7am-11pm Sun-Sat
            day_starting = child_text.downcase.index("sun")
            day_starting2 = child_text.downcase.index("sa")
            day_start_index = day_starting > day_starting2 ? day_starting2 : day_starting
            child_text = "#{child_text[day_start_index..-1]} #{child_text[0..day_start_index-1]}"
          end
        end
        unless digit_start.nil?
          digit_index = digit_start.to_s
          if child_text.length <= 8 && (child_text.downcase.include?("am") ||
                                                child_text.downcase.include?("pm")) #for handling Open 7 days a week \n 7am-11pm
            child_text = "Mon-Sun #{child_text}"
          elsif child_text.length <= 12 && child_text.downcase.include?("midnight")
            child_text = "Mon-Sun #{child_text}"
          end
          
        end
        if child_text.downcase.start_with?("store hours") && (child_text.downcase.include?("am") || 
                                                            child_text.downcase.include?("pm")) # Store hours: 6AM-11PM
          child_text = "#{child_text.sub('Store hours:','Mon-Sun')}"
        end
        child_text = child_text.sub("Spirits: ", "") if child_text.downcase.start_with?("spirits:") && 
                                                        child_text.downcase.include?("mon") # Spirits: Mon-Sat 9am-10pm 
        child_text = child_text.sub("Mon thru Sat", "Mon-Sat") if child_text.include?("Mon thru Sat") # Mon thru Sat 7am - 10pm
        if child_text.downcase.include?("sat closes") # Sat Closes 12am
          next_xml = hours_xml.children[index+2].text
          if next_xml.downcase.include?("sun")
            day_start = /\d+/.match(next_xml).to_s
            day_index = next_xml.index(day_start)
            temp = next_xml[day_index..-1]
            open_hour = temp.split("-")[0]
            child_text = child_text.downcase.sub("closes ", "#{open_hour}-")
            store_hours << child_text
          end
        end
        child_text = "Mon-Sun 12am-11:59pm" if child_text.strip.downcase.eql?("open 24 hours") || 
                                               child_text.strip.downcase.eql?("open 24 hrs") #Open 24 Hours
        next unless (child_text.start_with?("Mon") || child_text.start_with?("Tue") || 
            child_text.start_with?("Wed") || child_text.start_with?("Thu") ||
            child_text.start_with?("Fri") || child_text.start_with?("Sat") ||
            child_text.start_with?("Sun"))
        unless (child_text.empty? || child_text.include?("----"))
          if child_text.downcase.include?("fri") && (child_text.downcase.include?("sat") || 
                child_text.downcase.include?("sa")) &&
                ( (child_text.downcase.index("sa") - child_text.downcase.index("fri")) > 10 )#FOR Fri 7am-11pm Sat 7am-10pm
            breakpoint = child_text.downcase.index("sa")
            child_text1 = child_text[0..breakpoint-1]
            child_text2 = child_text[breakpoint..-1]
            store_hours << child_text1
            store_hours << child_text2
          elsif child_text.downcase.include?("sat") && child_text.downcase.include?("sun") && 
                ( (child_text.downcase.index("sun") - child_text.downcase.index("sat")) > 10 )# FOR Sat 7am-10pm Sun 7am-9pm
            breakpoint = child_text.downcase.index("sun")
            child_text1 = child_text[0..breakpoint-1]
            child_text2 = child_text[breakpoint..-1]
            store_hours << child_text1
            store_hours << child_text2
          elsif child_text.downcase.start_with?("mon-thu sat") # FOR Mon-Thu Sat 7:30am-9pm
            days_sep = child_text.downcase.split("sat")
            store_hours << "#{days_sep[0].strip} #{days_sep[-1].strip}"
            store_hours << "Sat #{days_sep[-1].strip}"
          else
            store_hours << child_text
          end
        end 
      end
    end
    
    week_hours = []
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase      
      store_hour = store_hour.sub("open 24 hours", "12am-11:59pm") if store_hour.include?("open 
                                                    24 hours")
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      open_hours = open_hours.gsub(" am ", "am") if open_hours.include?(" am ") # 6 am -12 midnight
      open_hours = open_hours.gsub(" am", "am") if open_hours.include?(" am") # 6 am - 12 am
      open_hours = open_hours.gsub(" pm", "pm") if open_hours.include?(" pm") # 6 am - 10 pm
      
      open_hours = "12am-11:59pm" if (open_hours.include?("24 hrs") || 
                                      open_hours.include?("24 hours") || 
                                      open_hours.include?("24hrs")
                                     )
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = hours.map{|h| h.strip}
      hours[1] = "11:59pm" if hours[1].include?("midnight") || hours[1].include?("12mid")
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
#            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
#            day_hours["opening_time"] = hours[0]
#            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_weekly_circulars(base_url, store, store_id, store_circular_url)
    pdf_base_url = "http://content.shoprite.com/circular/139/pages/"
    storeLeaflets = []
    mechanize = Mechanize.new
    circulars_page = mechanize.get(store_circular_url)
    circulars_list = circulars_page.search("//div[@class='circular rounded-border']")
    circulars_list.each do |circular|
      circular_name = circular.search("./a/text()")[0].text.strip
      circular_href = circular.search(".//div[@class='circular-image']//a/@href")[0].value
      circular_url = "#{base_url}#{circular_href}"
      circular_dates = circular.search("./a/span[2]")[0].text.split("-")
      circular_start_date = circular_dates[0].strip
      circular_end_date = circular_dates[1].strip
      mechanize = Mechanize.new
      sleep(10)
      circular_page = mechanize.get(circular_url)
      pdf_tag = circular_page.search("//div[@id='PDF']/a")
      pdf_href = pdf_tag[0]["data-clientanalyticslabel"]
      pdf_url = "#{pdf_base_url}#{pdf_href}"
      leaflet = PQSDK::Leaflet.find pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = circular_name
        leaflet.start_date = circular_start_date
        leaflet.end_date = circular_end_date
        leaflet.url = pdf_url
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
#      leaflet.save
      @report.info  "*************LEAFLET*****"
      @report.info  leaflet.name
      @report.info  leaflet.url
      @report.info  leaflet.start_date
      @report.info  leaflet.end_date
      @report.info  leaflet.store_ids
      storeLeaflets << leaflet
    end
    storeLeaflets
  end
  
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '66c3a64ee6803b97d47ead67d6461f5a8c3128280b5caee83782f5b26ace6dd6'
    @report.info  "IN RUN METHOD"
    @report.info  "GOING TO COLLECT THE STORES AND LEAFLETS DATA"
    stores, leaflets = get_stores
    @report.info  "JUST ENDED THE SITE DATA"

  end
  
end