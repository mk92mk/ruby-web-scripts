#c7a5c17bf586bc5dd4711f7376ef2113b37e4e4b3a67335c801d3258b78597e3 for rite aid
require 'pqsdk'
require 'mechanize'
require 'time'

class RiteAid
  def initialize
    @report = Logger.new("RiteAid#{Time.now.to_s}.log")
  end
  
  def get_stores(directory_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(directory_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_list_hrefs = stores_list_page.search("//div[@class='state']//a/@href")
    stores_list_hrefs.each do |store_href|
      mechanize = Mechanize.new
      store_page = mechanize.get(store_href.value)
      puts "GOT STORE DETAILS PAGE ON #{store_href.value}"
      @report.info "GOT STORE DETAILS PAGE ON #{store_href.value}"
      store_name_node = store_page.search("//div[@class='routingOptions-left-div']/h1/a")[0]
      if store_name_node.nil?
        puts "NO PARTICULARS FOUND FOR #{store_href.value} SO IGNORING "
        @report.info "NO PARTICULARS FOUND FOR #{store_href.value} SO IGNORING"
        next
      end
      store_name = store_name_node.text.strip
      store_address = store_page.search("//div[@class='address1 address-position']//p[1]/strong")[0].text
      store_city = store_page.search("//div[@class='address1 address-position']//p[2]/strong")[0].text
                                                                                .split(",")[0]
      store_zipcode = store_page.search("//div[@class='address1 address-position']//p[2]/strong")[0].text
                                                                                .split(",")[1]
                                                                                .scan(/\d+/).first
      store_phone = store_page.search("//div[@class='address1 address-position']
                                        //p[@class='padding-phone']/text()")[0].text
      store_latitude = store_page.search("//input[@id='fromAddress']/@onkeydown")[0].value
                                                                                .split(",")[-3]
                                                                                .gsub("'","").strip
      store_longitude = store_page.search("//input[@id='fromAddress']/@onkeydown")[0].value
                                                                                .split(",")[-2]
                                                                                .gsub("'","").strip
      hours_list = store_page.search("//div[@id='hours']//ul[@class='days']")
      opening_hours = parse_hours(hours_list)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_href.value
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_href.value}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_href.value}"
      
      store_leaflet_url = store_page.search("//div[@id='article_21010_167240_1.2']//a/@href")[0].value
      store_leaflets = get_leaflets(store_zipcode, store.id, store_leaflet_url)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      allStoresIds << store.id
      sleep(6)
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_zipcode, store_id, store_leaflet_url)
    leaflets = []
    leaflet_base_url = "https://weeklyad.info.riteaid.com/flyers/riteaid?type=2
                                                  &chrome=broadsheet&postal_code="
#    mechanize = Mechanize.new{|a| a.ssl_version, a.verify_mode = 'SSLv3', OpenSSL::SSL::VERIFY_NONE}
    mechanize = Mechanize.new
    mechanize.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    sleep(3)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    leaflet_base_page_url = "#{leaflet_base_url}#{store_zipcode}"
    pdf_url_starting = "https://f.wishabi.net/flyers/"
    sleep(2)
    leaflet_page = mechanize.get(leaflet_base_page_url)
    leaflet_script_text = leaflet_page.search("//script[contains(text(),'flyerData')]")[0].text
    script_urls = URI.extract(leaflet_script_text)
    return leaflets if script_urls.empty?
    leaflet_flags = script_urls.map{|su| su.include?(pdf_url_starting) && su.end_with?(".pdf")}
    pdf_url = script_urls[leaflet_flags.index(true)]
    leaflet_dates = leaflet_page.search("//title[contains(text(),'Rite Aid Weekly Ad -')]")[0].text
    leaflet_start_date = Time.parse(leaflet_dates.split("-")[1].split("to")[0].strip).to_s
    leaflet_end_date = Time.parse(leaflet_dates.split("-")[1].split("to")[1].strip).to_s
    puts "DOWNLOAD FROM #{pdf_url}"
    @report.info "DOWNLOAD FROM #{pdf_url}"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = pdf_url
      leaflet.name = "Weekly Ad"
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    @report.info leaflet.start_date
    @report.info leaflet.end_date
    leaflets << leaflet
    leaflets
  end

  def parse_hours(hours_list)
    week_hours = []
    hours_list.each do |hour_item|
      days_text = hour_item.search("./li[1]")[0].text.downcase
      hours_text = hour_item.search("./li[@class='store-details-hours-second-child']")[0].text.downcase
      puts "GOING TO PROCESS #{days_text} #{hours_text} FOR HOURS"
      @report.info "GOING TO PROCESS #{days_text} #{hours_text} FOR HOURS"
      hours_text = "12am-11:59pm" if hours_text.include?("open 24 hours")
      hours = hours_text.split("-").map{|ht| ht.strip}
      days = days_text.split("-")
      days = days.map{|d| d.strip}
      days.delete("")
      if days.length == 2
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            if hours_text.include?("closed")
              day_hours["closed"] = true
              next
            end
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            if hours_text.include?("closed")
              day_hours["closed"] = true
              next
            end
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        if hours_text.include?("closed")
          day_hours["closed"] = true
        else
          formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
          formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
          formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
          formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
          day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
          day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
          day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
          day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        end
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  
    def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'c7a5c17bf586bc5dd4711f7376ef2113b37e4e4b3a67335c801d3258b78597e3'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("https://www.riteaid.com/store-locator/stores-by-state")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end
