#01aac43aceab8cc2b1b473c73debb8bd55825f8b08cfea807c790b6f2ffa33de for Acmoor
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'


class Acmoore
  
  def get_stores
    allStoresIds = []
    store_form_url = "http://www.acmoore.com//dealerlocator/index/searchdealer"
    mechanize = Mechanize.new
    store_form_page = mechanize.get("http://www.acmoore.com/storelocator")
    puts "GOT MAIN LOCATOR PAGE"
    state_values = store_form_page.search("//select[@id='dealer-state']//option/@value")
    state_values.each do |state_value|
      sleep(5)
      mechanize = Mechanize.new
      store_results_page = mechanize.post(store_form_url, {"dealer_name" =>"", "dealer_lat" => "",
                                                           "dealer_lng" => "", "dealer_city" => "",
                                                           "dealer_zip" => "", 
                                                           "dealer_state" => state_value.value,
                                                           "dealer_within" => "100", "id" => ""})
      puts "GOT STORE RESULTS FOR #{state_value.value}"
      next if state_value.value.empty?
      stores_list = JSON.parse(store_results_page.body)
      next if stores_list[0]["nodata"]
      stores_list.each do |store_item|
        store_name = store_item["dealer_name"]
        store_address = "#{store_item["address1"]} #{store_item["address2"]}".strip.squeeze(" ")
        store_city = store_item["city"]
        store_zipcode = store_item["zip"]
        store_phone = store_item["phone"]
        store_latitude = store_item["latitude"]
        store_longitude = store_item["longitude"]
        store_hours = store_item["store_hours"]
        puts "ON #{store_address} NOW"
        opening_hours = parse_hours(store_hours[0])
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_form_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_form_url}"
        puts "******"
        allStoresIds << store.id
      end
      puts "GOING TO NEXT STATE"
    end
    allStoresIds
  end

  def parse_hours(store_item_hours)
    week_hours = []
    puts "GOT STORE HOURS #{store_item_hours}"
#    @logger.info "GOT STORE HOURS #{store_item_hours}"
    opening_time_sat = store_item_hours["sat_start"]
    opening_time_sun = store_item_hours["sun_start"]
    opening_time_mon = store_item_hours["mon_start"]
    opening_time_tue = store_item_hours["tue_start"]
    opening_time_wed = store_item_hours["wed_start"]
    opening_time_thu = store_item_hours["thus_start"]
    opening_time_fri = store_item_hours["fri_start"]
    
    closing_time_sat = store_item_hours["sat_end"]
    closing_time_sun = store_item_hours["sun_end"]
    closing_time_mon = store_item_hours["mon_end"]
    closing_time_tue = store_item_hours["tue_end"]
    closing_time_wed = store_item_hours["wed_end"]
    closing_time_thu = store_item_hours["thus_end"]
    closing_time_fri = store_item_hours["fri_end"]
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_mon.downcase.sub("am", "").strip).rjust(5, "0") if 
                                             opening_time_mon && opening_time_mon.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_mon.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_mon && opening_time_mon.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_mon.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_mon.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 0
      week_hours << store_hour
    end
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_tue.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_tue.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_tue.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_tue.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 1
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_wed.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_wed.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_wed.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_wed.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 2
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_thu.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_thu.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_thu.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_thu.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 3
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_fri.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_fri.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_fri.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_fri.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 4
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sat.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sat.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sat.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sat.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 5
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sun.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sun.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sun.downcase.sub("am", "").strip).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sun.downcase.sub("pm", "").strip).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 6
      week_hours << store_hour
    end
    week_hours
  end
  
  def convert_hours_am(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    leaflet_page = mechanize.get("http://www.acmoore.com/view-our-weekly-craft-deal/")
    puts "GOT WEEKLY DEAL PAGE"
    leaflet_dates = leaflet_page.search("//div[@class='sale-dates']")[0].text
    leaflet_start_date = Time.parse(leaflet_dates.split("-")[0].strip)
    leaflet_end_date = Time.parse(leaflet_dates.split("-")[1].strip)
    leaflet_pdf_url = leaflet_page.search("//div[@id='print-view']//a/@href")[0].value
    store_leaflet_name = "ACMoore_leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = leaflet_pdf_url
      leaflet.name = store_leaflet_name
      leaflet.store_ids = store_ids
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    # leaflet.save
    puts "*******************LEAFLET"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    allStoresLeafletsIds << leaflet.id
    allStoresLeafletsIds
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '01aac43aceab8cc2b1b473c73debb8bd55825f8b08cfea807c790b6f2ffa33de'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end  
end

