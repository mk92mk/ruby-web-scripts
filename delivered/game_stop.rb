#060c00b9549ef13f056c043412954ef46f1895fcfe4bcbbe619f6b5c66ded142 FOR GAMESTOP
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class GameStop
  
  def get_stores
    allStoresIds = []
    base_url = "http://spatial.virtualearth.net/REST/v1/data/8f92e4701aa94bbba485642dc6d15873/_AllStores/StoreSchema?s=1&$format=json&jsonp=getStoreInfoSuccess&callback=getStoreInfoSuccess&spatialFilter=nearby(%s,%s,24.1401)&$filter=DisplayType Eq '1' and StoreName Ne 'GameStop.com' and StoreName Ne 'Internet / Stores' and StoreName Ne 'GameStop Military'&key=Am44pKKIzJ_IC3tunys7AIs2HD1ZyJGhg9qCPHu2TlS5cyLYNd7wSLnm_dBCrjrK&$top=10000&callback=jQuery111106591388321496865_1462542147876&_=1462542147878"
    mechanize = Mechanize.new
    cities_page = mechanize.get("http://api.promoqui.eu/v1/cities")
    puts "GOT CITIES PAGE"    
    cities_list = JSON.parse(cities_page.body)
    cities_list.each do |city|
      next unless city["country"].include?("USA")
      city_lat = city["latitude"]
      city_lng = city["longitude"]
      city_stores_url = base_url %[city_lat, city_lng]
      sleep(1)
      mechnanize = Mechanize.new
      city_stores_page = mechanize.get(city_stores_url)
      puts "GOT STORES LIST PAGE FOR #{city["name"]} AT #{city_stores_url}"
      city_stores_text = city_stores_page.body.sub("getStoreInfoSuccess(", "").chomp(")").strip
      city_stores_list = JSON.parse(city_stores_text)["d"]["results"]
      city_stores_list.each do |city_store_item|
        store_name = city_store_item["MallName"]
        store_address = city_store_item["Address1"]
        store_city = city_store_item["Locality"]
        store_zipcode = city_store_item["PostalCode"]
        store_phone = city_store_item["Phone"]
        store_latitude = city_store_item["Latitude"]
        store_longitude = city_store_item["Longitude"]
        store_hours = city_store_item["StoreHours"]
        store_name = store_address if store_name.nil? || store_name.to_s.empty?
        puts "ON #{store_address} NOW"
        opening_hours = parse_hours(store_hours)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = city_stores_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{city_stores_url}"
        puts "******"        
        allStoresIds << store.id
      end
      puts "\nGOING TO NEXT CITY"
    end
    allStoresIds
  end
  
  def parse_hours(store_hours_text)
    week_hours = []
    puts "GOT #{store_hours_text} TO PARSE"
    store_hours = store_hours_text.downcase.split(",")
    store_hours.each do |store_hour|
      puts "GOING TO PARSE #{store_hour} NOW"
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
              .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
            .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
            .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    mechanize.redirect_ok = false
    base_url = "http://www.gamestop.com"
    leaflet_page = mechanize.get("http://www.gamestop.com/weeklyad")
    leaflet_pdf_href = leaflet_page.search("//a[@class='hsadnav_mid']/@href")[0].value
    leaflet_pdf_url = "#{base_url}#{leaflet_pdf_href}"
    store_leaflet_name = "Game Stop_Leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
      leaflet.store_ids = store_ids
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    puts "********"
    allStoresLeafletsIds << leaflet.id
    allStoresLeafletsIds
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '89d97c4daee22c5fd2d3bbad75bd12f46bf3781f18b34f6e3ea90707b5c370ac'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end
end

