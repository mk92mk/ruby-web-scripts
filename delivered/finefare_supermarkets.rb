#c821d614ab24e8f4ff96aaa6c23a71cb947c7fdf782152f6e17cad9ef342d2cc for finefare supermarkets
require 'pqsdk'
require 'mechanize'


class FineFareSuperMarkets
  
  def initialize
    @report = Logger.new("FineFareSuperMarkets#{Time.now.to_s}.log")
  end
  
  def get_stores(stores_page_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    stores_page = mechanize.get(stores_page_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_rows = stores_page.search("//table[@id='ContentPlaceHolder2_GridView1']//tr[td]")
    base_url = "http://finefaresupermarkets.com"
    query_url = "#{base_url}/ShowStoreLocations.aspx?Address="
    stores_rows.each do |store_row|
      store_elements = store_row.search(".//td")
      store_address =  store_elements[0].text.strip
      store_city = store_elements[1].text.strip
      store_state = store_elements[2].text.strip
      mechanize = Mechanize.new
      store_form_page = mechanize.get("http://finefaresupermarkets.com/FindAStore.aspx")
      store_form = store_form_page.form_with(:id => "form1")
      address_field = store_form.field_with(:id => "ContentPlaceHolder2_txtSearch")
      address_field.value = store_city
      miles_field = store_form.field_with(:id => "ContentPlaceHolder2_DropDownList1")
      miles_field.value = "100"
      button = store_form.button_with(:id => "ContentPlaceHolder2_btnSearch")
      mechanize = Mechanize.new
#      sleep(4)
      store_results_page = mechanize.submit(store_form, button)
      if store_results_page.uri.to_s.eql?("http://finefaresupermarkets.com/FindAStore.aspx")
        store_city_node = store_results_page.search("//a[contains(.//text(),
        '#{store_city.split.map{|st| st.capitalize}.join(" ")}, #{store_state.upcase}, USA')]/@href")[0]
        if store_city_node
          store_city_href = store_city_node.value 
          store_details_url = "#{base_url}/#{store_city_href}"
          store_details_page = mechanize.get(store_details_url)
        else
          address_field.value = store_state.upcase
          store_details_page = mechanize.submit(store_form, button)
          store_details_url = store_details_page.uri.to_s
          puts "GOT STORE DETAILS PAGE AT #{store_details_url} AT SECOND ATTEMPT"
          @report.info "GOT STORE DETAILS PAGE AT #{store_details_url} AT SECOND ATTEMPT"
        end
        sleep(1)
      elsif store_results_page.uri.to_s.include?("ShowStoreLocations.aspx")
        store_details_url = store_results_page.uri.to_s
        store_details_page = store_results_page
      end
      puts "GOT STORE DETAILS PAGE at #{store_details_url}"
      @report.info "GOT STORE DETAILS PAGE at #{store_details_url}"
      store_info = store_details_page.search("//table[@class='searchResults']
                                          //tr[contains(.//td//text(), '#{store_address.upcase}')]")[0]
      if store_info.nil?
        address_field.value = store_state.upcase
        store_details_page = mechanize.submit(store_form, button)
        store_info = store_details_page.search("//table[@class='searchResults']
                                          //tr[contains(.//td//text(), '#{store_address.upcase}')]")[0]
        store_details_url = store_details_page.uri.to_s
        puts "GOT STORE DETAILS PAGE AT #{store_details_url} AT SECOND ATTEMPT"
        @report.info "GOT STORE DETAILS PAGE AT #{store_details_url} AT SECOND ATTEMPT"
      end
      
      store_elements = store_info.search(".//td")[0].text.lines.map{|l| l.strip}
      temp_address = store_elements[1]
      address_parts = temp_address.split(",")
      store_address = address_parts[0].strip
      store_name = store_address
      store_city = address_parts[1].strip
      store_zipcode = address_parts[2].strip.scan(/\d+/).first
      store_phone = store_elements[11].strip
      store_img_src = store_info.search(".//img[contains(@id, 'SearchResults_imgIcon')]/@src")[0].value
      coords_script = store_details_page.search("//script[contains(./text(),
                                                 '#{store_img_src}')]")[0].text
      end_index = coords_script.index(store_img_src)
      start_index = end_index - 50
      coords_text = coords_script[start_index..end_index]
      coords = coords_text.scan(/\d+.\d+|-\d+.\d+/)
      store_latitude = coords[0]
      store_longitude = coords[1]
      opening_hours_text = store_elements[9].gsub("Hours:","")
      opening_hours = parse_hours(opening_hours_text)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_details_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_details_url}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_details_url}"
      leaflet_mech_form = store_details_page.search("//form[@id='form1']")
      leaflet_form = store_details_page.form_with(:id => "form1") 
      button_id = store_info.search(".//input[contains(@id, 
                                  'ContentPlaceHolder1_lvSearchResults_Button1')]/@id")[0].value
      button = leaflet_form.button_with(:id => button_id)
      mechanize = Mechanize.new
      mechanize.redirect_ok = false
      leaflets_page = mechanize.submit(leaflet_form, button)
      store_leaflet_url = leaflets_page.uri.to_s
      if leaflets_page.code.eql?("302")
        redirected_url = leaflets_page['location']
        leaflets_url = "#{base_url}#{redirected_url}"
        leaflets_page = mechanize.get(leaflets_url)
      end
      store_leaflets = get_leaflets(leaflets_page, store_address, store_name, store.id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
      allStoresIds << store.id
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_leaflets_page, store_address, leaflet_name, store_id)
    leaflets = []
    images_urls = []
    base_url = "http://finefaresupermarkets.com"
    leaflets_imgs_hrefs = store_leaflets_page.search("//table[contains(.//tr//a/@href,'/Circulars')]
                                                      //a/@href")
    leaflets_imgs_hrefs.each do |leaflet_img_href|
      images_urls << "#{base_url}#{leaflet_img_href.value}"
      puts "#{base_url}#{leaflet_img_href.value} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      @report.info "#{base_url}#{leaflet_img_href.value} ADDED IN THE MAIN ARRAY FOR LEAFLET"
    end
    store_leaflet_name = "#{leaflet_name}_LEAFLET"
    store_leaflet_url = "#{base_url}/Circular4Page.aspx##{store_address}"
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.image_urls = images_urls
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    leaflets << leaflet
    leaflets
  end
  
  
  def parse_hours(hours_text)
    puts "GOT #{hours_text} TO PARSE"
    @report.info "GOT #{hours_text} TO PARSE"
    hours_text = hours_text.gsub("12 Midnight", "11:59PM")
    hours_lines = hours_text.split("/")
    week_hours = []
    
    hours_lines.each do |store_hour|
      store_hour = store_hour.downcase   
      store_hour = "mon-sun 12am-11:59pm" if store_hour.include?("open 24 hours")
      first_digit = /\d+/.match(store_hour).to_s
      next if first_digit.empty?
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
                                     !temp_days.include?("to") && !temp_days.include?("–")
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
      if days.length == 2 # > 1(before)
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".", "")) if  hours[0]
                          .include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
                          .include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".", "")) if hours[1]
                          .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                          .include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".","")) if  hours[0]
              .include?("am")||hours[0].include?("a.m")||hours[0].include?("a.m.")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
              .include?("pm")||hours[0].include?("p.m.")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".","")) if hours[1]
              .include?("am")||hours[1].include?("a.m")||hours[1].include?("a.m.")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
              .include?("pm")||hours[1].include?("p.m.")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m", "").sub(".","")) if  hours[0]
                        .include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.", "")) if hours[0]
                        .include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m", "").sub(".","")) if hours[1]
                        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '3776337d7d85e27915e30a2e98996c4301d0a2fb8513b0ff7d65ee7bcaf16b16'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.finefaresupermarkets.com/locations.aspx")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end