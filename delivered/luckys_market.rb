#a024ccff7532b39889a258e18ca162e77c5090f53aacc870b68ce82d9ec5e9fb FOR LUCKYS MARKET
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class LuckysMarket
  
  def get_stores(stores_locations_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    stores_locations_page = mechanize.get(stores_locations_url)
    puts "GOT STORES LOCATIONS PAGE"
    stores_locations = stores_locations_page.search("//div[@class='locations']")
    stores_locations.each do |store_location|
      store_name = store_location.search(".//h3")[0].text.strip
      store_address = store_location.search(".//div[@class='addr']")[0].text.strip
      if store_address.downcase.eql?("coming soon")
        store_address = store_location.search(".//div[@class='addr2']")[0].text.strip
      end
      store_city = store_location.search(".//span[@class='city']/text()")[0].text.strip.chomp(",")
      store_zipcode = store_location.search(".//span[@class='zipcode']")[0].text.strip
      store_phone = store_location.search(".//span[@class='num phone']")[0].text.strip
      store_details_url = store_location.search(".//a[@class='website']/@href")[0].value
      if store_details_url.start_with?("www.")
        store_details_url = "http://#{store_details_url}"
      end
      puts "ON #{store_address} NOW"
      opening_hours = []
      sleep(1)
      mechanize = Mechanize.new
      flag = true
      begin
        store_details_page = mechanize.get(store_details_url)
      rescue Exception => e
        case e.message
        when /404/ then puts "404! STORE NOT FOUND AT #{store_details_url}"
          flag = false
        end
      end
      if flag == true
        puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
        unless store_details_page.uri.to_s.include?("http://www.luckysmarket.com/map-location")
          coord_url_node = store_details_page.search("//div[@class='widget-pane-section-info-line']
                                                 //a[contains(@href, 'google.com/maps/')]/@href")[0]
          if coord_url_node
            coord_url = coord_url_node.value
          else
            coord_url_node = store_details_page.search("//div[@class='wpb_wrapper']
                                                       //a[contains(@href, 'google.com/maps/')]/@href")[0]
            coord_url = coord_url_node.value
          end
          coords = coord_url.scan(/@\d+.\d+|@-\d+.\d+|,-\d+.\d+|,\d+.\d+/)
          store_latitude = coords[0].gsub("@", "").gsub(",", "")
          store_longitude = coords[1].gsub("@", "").gsub(",", "")
          store_hours_node = store_details_page.search("//div[@class='widget-pane-section-info-line']
                                                  [contains(text(), 'Hours')]")[0]
          if store_hours_node
            store_days = store_details_page.search("//div[@class='widget-pane-section-info-line']
                                          [contains(text(), 'Hours')]//following-sibling::div")[0].text
            store_hours = store_hours_node.text.strip
            opening_hours = parse_hours("#{store_days} #{store_hours}")
          else
            store_hours_node = store_details_page.search("//div[@class='wpb_wrapper']//p[contains(text(), 
                                                    'Hours')]")[0]
            if store_hours_node
              store_hours = store_hours_node.text.strip
              store_days_node = store_details_page.search("//div[@class='wpb_wrapper']//p[contains(text(), 
                                                    'Hours')]/following-sibling::p")[0]
              if store_days_node
                store_days = store_days_node.text
              else
                store_days = ""
              end
              opening_hours = parse_hours("#{store_days} #{store_hours}")
            end
          end
        end
      end
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude if store_latitude
        store.longitude = store_longitude if store_longitude
        store.phone = store_phone
        store.origin = stores_locations_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}" if store_longitude
      puts  "STORE LATITUDE #{store_latitude}" if store_latitude
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_locations_url}"
      puts "******"
      allStoresIds << store.id
      if store_details_page
        store_leaflets = get_leaflets(store.id, store_name, store_details_page)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_name, store_details_page)
    leaflets = []
    store_leaflet_node = store_details_page.search("//li[@class='isotope-item']//a[contains(@href, 
                                                    'weekly-sales')]/@href")[0]
    if store_leaflet_node
      store_leaflet_url = store_leaflet_node.value
    else
      puts "NO LEAFLET FOUND FOR #{store_name}"
      return leaflets
    end
    sleep(1)
    mechanize = Mechanize.new
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
    leaflet_pdf_url = store_leaflet_page.search("//div[@class='wpb_wrapper']
                                //a[contains(text(), 'DOWNLOAD THIS WEEK’S SALES FLYER')]/@href")[0].value
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    puts "********"
    leaflets << leaflet
    return leaflets
  end
  
  def parse_hours(store_hours_text)
    week_hours = []
    puts "GOT #{store_hours_text} TO PARSE"
    store_hour = store_hours_text.strip.downcase
    store_hour = store_hour.sub("7 days a week", "mon-sun").sub("seven days a week", "mon-sun")
                           .sub("hours", "").strip.chomp(":").strip
    if store_hour.include?("7") && store_hour
      .include?("days") && store_hour.include?("a") && store_hour.include?("week")
        store_hour = store_hour.sub("7","").sub("days","").sub("a","").sub("week","")
        store_hour = "mon-sun #{store_hour}"
    end 
    unless store_hour.include?("mon") || store_hour.include?("tue") || 
            store_hour.include?("wed") || store_hour.include?("thu") ||
            store_hour.include?("fri") || store_hour.include?("sat") ||
            store_hour.include?("sun")
          store_hour = "mon-sun #{store_hour}"
    end
    first_digit = /\d+/.match(store_hour).to_s
    breakpoint = store_hour.index(first_digit)
    days = store_hour[0..breakpoint-1].split("-")
    days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
    days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
    days = days.map{|d| d.strip}
    days.delete("")
    open_hours = store_hour[breakpoint..-1]
    hours = open_hours.split("-")
    hours = open_hours.split(" - ") if open_hours.include?(" - ")
    hours = open_hours.split(" to ") if open_hours.include?(" to ")
    hours = open_hours.split("–") if open_hours.include?("–")
    hours = open_hours.split("—") if open_hours.include?("—")
    hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
            .include?("to") || open_hours.include?("–") || open_hours.include?("-")
    if days.length > 1
      starting_day = days[0]
      ending_day = days[1]
      starting_day_index = get_day_index(starting_day)
      ending_day_index = get_day_index(ending_day)
      if starting_day_index - ending_day_index == 1
        (0..6).each do |n|
          day_hours = {}
          day_hours["weekday"] = n
          formatted_time_open_am = convert_hours_am(hours[0].strip.sub("am", "")) if hours[0].include?("am")
          formatted_time_open_pm = convert_hours_pm(hours[0].strip.sub("pm", "")) if hours[0].include?("pm")
          formatted_time_close_am = convert_hours_am(hours[1].strip.sub("am", "")) if hours[1].include?("am")
          formatted_time_close_pm = convert_hours_pm(hours[1].strip.sub("pm", "")) if hours[1].include?("pm")
          day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
          day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
          day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
          day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
          day_hours["closed"] = true if open_hours.include?("closed")
          week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
        end              
      else
        (starting_day_index..ending_day_index).each do |n|
          day_hours = {}
          day_hours["weekday"] = n
          formatted_time_open_am = convert_hours_am(hours[0].strip.sub("am", "")) if hours[0].include?("am")
          formatted_time_open_pm = convert_hours_pm(hours[0].strip.sub("pm", "")) if hours[0].include?("pm")
          formatted_time_close_am = convert_hours_am(hours[1].strip.sub("am", "")) if hours[1] && hours[1]
          .include?("am")
          formatted_time_close_pm = convert_hours_pm(hours[1].strip.sub("pm", "")) if hours[1] && hours[1]
          .include?("pm")        
          day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
          day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
          day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
          day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
          day_hours["closed"] = true if open_hours.include?("closed")
          week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
        end
      end
    elsif days.length == 1
      day_index = get_day_index(days[0])
      day_hours = {}
      day_hours["weekday"] = day_index
      formatted_time_open_am = convert_hours_am(hours[0].strip.sub("am", "")) if hours[0].include?("am")
      formatted_time_open_pm = convert_hours_pm(hours[0].strip.sub("pm", "")) if hours[0].include?("pm")
      formatted_time_close_am = convert_hours_am(hours[1].strip.sub("am", "")) if hours[1] && hours[1]
                                                                              .include?("am")
      formatted_time_close_pm = convert_hours_pm(hours[1].strip.sub("pm", "")) if hours[1] && hours[1]
                                                                              .include?("pm")
      day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
      day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
      day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
      day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
      day_hours["closed"] = true if open_hours.include?("closed")
      week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'a024ccff7532b39889a258e18ca162e77c5090f53aacc870b68ce82d9ec5e9fb'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores("http://www.luckysmarket.com/find-a-location/")
    puts "JUST ENDED THE SITE DATA"
  end
end