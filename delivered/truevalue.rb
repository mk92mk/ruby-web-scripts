#d36e6f42e797dd1c0a783549027aadfe2e5abf94f84947a46b31ca15eca58e2c for True Value
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'

class TrueValue
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    data = {"request"=>{"appkey"=>"41C97F66-D0FF-11DD-8143-EF6F37ABAA09", 
                      "formdata"=>{"geoip"=>false, "dataview"=>"store_default", "limit"=>100000, 
                                  "geolocs"=>{"geoloc"=>[{"addressline"=>"", "country"=>"", 
                                                          "latitude"=>"", 
                                                          "longitude"=>""}]}, 
                                  "searchradius"=>"40|50|80", 
                                  "where"=>{"and"=>{"giftcard"=>{"eq"=>""}, 
                                            "tvpaint"=>{"eq"=>""}, "creditcard"=>{"eq"=>""}, 
                                            "localad" => {"eq" => ""}, "ja" => {"eq"=>""}, "tvr" => {"eq"=> ""},
                                            "activeshiptostore" => {"eq" => ""}, "tv" => {"eq"=> ""}}}, 
                                  "false"=>"0"}}}
    stores_url = "http://hosted.where2getit.com/truevalue/rest/locatorsearch?like=0.9127130811547209&lang=en_US"
    mechanize = Mechanize.new
    cities_page = mechanize.get("http://api.promoqui.eu/v1/cities")
    puts "GOT PROMOQUI CITIES PAGE"
    cities_list = JSON.parse(cities_page.body)
    cities_list.each do |city|
      next unless (city["state"].eql?("confirmed") && city["country"].eql?("USA"))
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["addressline"] = city["name"]
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["latitude"] = city["latitude"]
      data["request"]["formdata"]["geolocs"]["geoloc"][0]["longitude"] = city["longitude"]
      store_results_page = mechanize.post stores_url, data.to_json, {'Content-Type' => 'application/json'}
      puts "GOT STORE PAGE FOR #{city["name"]}"
      stores_list = JSON.parse(store_results_page.body)["response"]["collection"]
      stores_list.each do |store_item|
        store_name = store_item["name"]
        store_address = store_item["address1"]
        store_city = store_item["city"]
        store_zipcode = store_item["postalcode"]
        store_phone = store_item["phone"]
        store_latitude = store_item["latitude"]
        store_longitude = store_item["longitude"]
        puts "ON #{store_address} NOW"
        opening_hours = parse_hours(store_item)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude if store_latitude
          store.longitude = store_longitude if store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}" if store_longitude
        puts  "STORE LATITUDE #{store_latitude}" if store_latitude
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{stores_url}"
        puts "******"     
        allStoresIds << store.id
        if store_item["localad"]
          tvad_url = store_item["tvadvurl"]
          store_leaflets = get_leaflets(store.id, tvad_url)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
        end
      end
      puts "GOING TO NEXT CITY"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, leaflets_list_url)
    leaflets = []
    base_url = "http://truevalue.shoplocal.com"
    mechanize = Mechanize.new
    leaflets_list_page = mechanize.get(leaflets_list_url)
    puts "GOT LEAFLETS LIST PAGE AT #{leaflets_list_url}"
    leaflets_sections = leaflets_list_page.search("//div[@class='promotionOverlay']")
    leaflets_sections.each do |leaflet_section|
      store_leaflet_name = leaflet_section.search(".//a[@class='promotionTitle action-tracking-nav']")[0]
                                          .text.strip
      store_leaflet_dates = leaflet_section.search(".//p[@class='validDates']")[0].text
                                           .gsub("(except as noted)", "").gsub("Valid", "")
      leaflet_start_date = Time.parse(store_leaflet_dates.split("-")[0])
      leaflet_end_date = Time.parse(store_leaflet_dates.split("-")[1])
      store_leaflet_href = leaflet_section.search(".//a[@class='promotionTitle action-tracking-nav']/
                                                      @href")[0].value
      store_leaflet_url = "#{base_url}#{store_leaflet_href}"
      sleep(1)
      mechanize = Mechanize.new
      leaflet_images_page = mechanize.get(store_leaflet_url)
      puts "GOT LEAFLET IMAGES PAGE AT #{store_leaflet_url}"
      script_txt = leaflet_images_page.search("//script[contains(text(), 'pageElementsArray')]")[0].text
      images_urls = []
      urls = URI.extract(script_txt)
      urls.map{|u| images_urls << u if u.start_with?("http") && u.end_with?("jpg")}
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.image_urls
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.store_ids
      puts "**********"  
      leaflets << leaflet
      sleep(2)
    end
    leaflets
  end
  
  
  
  def parse_hours(store_item_hours)
    week_hours = []
    puts "GOT STORE HOURS #{store_item_hours}"
    opening_time_sat = store_item_hours["sat_open_time"]
    opening_time_sun = store_item_hours["sun_open_time"]
    opening_time_mon = store_item_hours["mon_open_time"]
    opening_time_tue = store_item_hours["tue_open_time"]
    opening_time_wed = store_item_hours["wed_open_time"]
    opening_time_thu = store_item_hours["thur_open_time"]
    opening_time_fri = store_item_hours["fri_open_time"]
    
    closing_time_sat = store_item_hours["sat_close_time"].sub("-", "").strip if store_item_hours["sat_close_time"]
    closing_time_sun = store_item_hours["sun_close_time"].sub("-", "").strip if store_item_hours["sun_close_time"]
    closing_time_mon = store_item_hours["mon_close_time"].sub("-", "").strip if store_item_hours["mon_close_time"]
    closing_time_tue = store_item_hours["tue_close_time"].sub("-", "").strip if store_item_hours["tue_close_time"]
    closing_time_wed = store_item_hours["wed_close_time"].sub("-", "").strip if store_item_hours["wed_close_time"]
    closing_time_thu = store_item_hours["thur_close_time"].sub("-", "").strip if store_item_hours["thur_close_time"]
    closing_time_fri = store_item_hours["fri_close_time"].sub("-", "").strip if store_item_hours["fri_close_time"]
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_mon.downcase.sub("am", "")).rjust(5, "0") if 
                                             opening_time_mon && opening_time_mon.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_mon.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_mon && opening_time_mon.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_mon.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_mon.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_mon && closing_time_mon.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 0
      week_hours << store_hour
    elsif opening_time_mon && opening_time_mon.include?("closed")
      store_hour["weekday"] = 0
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}
    store_hour["open_am"] = convert_hours_am(opening_time_tue.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_tue.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_tue && opening_time_tue.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_tue.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_tue.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_tue && closing_time_tue.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 1
      week_hours << store_hour
    elsif opening_time_tue && opening_time_tue.include?("closed")
      store_hour["weekday"] = 1
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_wed.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_wed.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_wed && opening_time_wed.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_wed.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_wed.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_wed && closing_time_wed.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 2
      week_hours << store_hour
    elsif opening_time_wed && opening_time_wed.include?("closed")
      store_hour["weekday"] = 2
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_thu.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_thu.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_thu && opening_time_thu.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_thu.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_thu.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_thu && closing_time_thu.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 3
      week_hours << store_hour
    elsif opening_time_thu && opening_time_thu.include?("closed")
      store_hour["weekday"] = 3
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_fri.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_fri.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_fri && opening_time_fri.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_fri.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_fri.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_fri && closing_time_fri.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 4
      week_hours << store_hour
    elsif opening_time_fri && opening_time_fri.include?("closed")
      store_hour["weekday"] = 4
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sat.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sat.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_sat && opening_time_sat.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sat.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sat.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_sat && closing_time_sat.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 5
      week_hours << store_hour
    elsif opening_time_sat && opening_time_sat.include?("closed")
      store_hour["weekday"] = 5
      store_hour["closed"] = true
      week_hours << store_hour
    end
    store_hour = {}    
    store_hour["open_am"] = convert_hours_am(opening_time_sun.downcase.sub("am", "")).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("am")
    store_hour["open_pm"] = convert_hours_pm(opening_time_sun.downcase.sub("pm", "")).rjust(5, "0") if 
                                            opening_time_sun && opening_time_sun.downcase.include?("pm")
    store_hour["close_am"] = convert_hours_am(closing_time_sun.downcase.sub("am", "")).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("am")
    store_hour["close_pm"] = convert_hours_pm(closing_time_sun.downcase.sub("pm", "")).rjust(5, "0") if 
                                            closing_time_sun && closing_time_sun.downcase.include?("pm")
    if store_hour["open_am"] || store_hour["open_pm"] || store_hour["close_am"] || store_hour["close_pm"]
      store_hour["weekday"] = 6
      week_hours << store_hour
    elsif opening_time_sun && opening_time_sun.include?("closed")
      store_hour["weekday"] = 6
      store_hour["closed"] = true
      week_hours << store_hour
    end
    week_hours
  end
  
  def convert_hours_am(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    times = times.downcase.gsub(" ", "")
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'd36e6f42e797dd1c0a783549027aadfe2e5abf94f84947a46b31ca15eca58e2c'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
  end
  
end