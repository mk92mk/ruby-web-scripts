#5bc1ec2cfc094dfb1b3d3254ca7941ccfde64d6a1fb2adb734caa901ca861bf9 FOR HEB

require 'pqsdk'
require 'mechanize'
require 'uri'


class Heb
  
  def initialize
    @report = Logger.new("Heb_#{Time.now.to_s}.log")
  end
  
  def get_stores(stores_list_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    store_urls = stores_list_page.search("urlset//url//loc")
    store_urls.each do |store_url|
      mechanize = Mechanize.new
      sleep(1)
      store_details_page = mechanize.get(store_url.text)
      puts "GOT STORE DETAILS PAE #{store_url.text}"
      @report.info "GOT STORE DETAILS PAE #{store_url.text}"
      store_name = store_details_page.search("//a[@class='breadcrumb-last']")[0].text.strip
      store_address = store_details_page.search("//div[@class='desc']//p[1]")[0].text.strip
      store_city = store_details_page.search("//div[@class='desc']//p[2]")[0].text.strip.split(",")[0]
      store_zipcode = store_details_page.search("//div[@class='desc']//p[2]")[0].text.strip.split(",")[1]
                                                                                .scan(/\d+\-\d+|\d+/).first
      store_phone = store_details_page.search("//p[@class='hours-detail-subtitle'][contains(text(), 
                                              'Main')]//following-sibling::p[1]//text()").text.strip
      coords_text = store_details_page.search("//script[contains(text(), 'GetMap')]")[0].text
      coords = coords_text.scan(/\,\d+.\d+|\,-\d+.\d+/)
      store_latitude = coords[0].gsub(",", "")
      store_longitude = coords[1].gsub(",", "")
      hours_text = store_details_page.search("//p[@class='hours-detail-subtitle'][contains(text(),
                                             'Store Hours')]//following-sibling::p[1]").text.strip
      opening_hours = parse_hours(hours_text)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url.text
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url.text}"
      puts "******"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_url.text}"
      @report.info "******"
      allStoresIds << store.id
      store_leaflet_id = store_details_page.search("//a[@class='icon store system-link']/@href")[0]
                                                                                .value.split("/")[-1]
      store_leaflets = get_leaflets(store.id, store_leaflet_id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_leaflet_id)
    leaflets = []
    base_url = "https://heb.inserts2online.com/"
    leaflet_base_url = "#{base_url}storeReview.jsp?drpStoreID=#{store_leaflet_id}"
    mechanize = Mechanize.new
    mechanize.redirect_ok = false
    sleep(2)
    redirect_page = mechanize.get(leaflet_base_url)
    code = redirect_page.code
    leaflet_name = ""
    redirect_page = get_redirected_page(redirect_page, code, mechanize) if code.eql?("302")
    if redirect_page.uri.to_s.include?("I2O_MainFrame.jsp")
      store_leaflet_href = redirect_page.search("//frame[@id='Large']/@src")[0].value
      store_leaflet_url = "#{base_url}#{store_leaflet_href}"
      sleep(2)
      store_leaflet_page = mechanize.get(store_leaflet_url)
      leaflet_dates = store_leaflet_page.search("//div[@class='pricesgood']")[0].text.strip
                                                    .scan(/\d+\/\d+\/\d+/)
      leaflet_start_date = leaflet_dates[0]
      leaflet_end_date = leaflet_dates[1]
      pdf_base_url = "#{base_url}version_pdf.jsp"
      sleep(1)
      pdf_page = mechanize.get(pdf_base_url)
      code = pdf_page.code
      leaflet_pdf_url = get_redirected_page(pdf_page, code, mechanize, true) if code.eql?("302")
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.url = leaflet_pdf_url
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      puts leaflet.start_date
      puts leaflet.end_date
      @report.info "************************LEAFLET***"
      @report.info leaflet.name
      @report.info leaflet.url
      @report.info leaflet.store_ids
      @report.info leaflet.start_date
      @report.info leaflet.end_date
      leaflets << leaflet
    elsif redirect_page.uri.to_s.include?("availableAds.jsp")
      leaflets_sect = redirect_page.search("//table[@id='globalContainer']//table//td")
      leaflets_sect.each do |leaflet_sec|
        next if leaflet_sec.search("./a[@target='pdf'][contains(@title, 'PDF')]//@href").empty?
        leaflet_name = leaflet_sec.search(".//a[@class='aahref']//center[@class='aahrefBold']")[0]
                                          .text.strip
        leaflet_dates = leaflet_sec.search(".//a[@class='aahref']//center")[1].text.strip
                                          .scan(/\d+\/\d+\/\d+/)
        leaflet_start_date = leaflet_dates[0]
        leaflet_end_date = leaflet_dates[1]
        leaflet_pdf_href = leaflet_sec.search(".//a[@target='pdf'][contains(@title, 'PDF')]//@href")[0]
                           .value
        leaflet_pdf_url = "#{base_url}#{leaflet_pdf_href}"
        leaflet = PQSDK::Leaflet.find leaflet_pdf_url
        if leaflet.nil?
          leaflet = PQSDK::Leaflet.new
          leaflet.url = leaflet_pdf_url
          leaflet.name = leaflet_name
          leaflet.start_date = leaflet_start_date
          leaflet.end_date = leaflet_end_date
        end
        if !leaflet.store_ids.include?(store_id)
          leaflet.store_ids << store_id
        end
        # leaflet.save
        puts "************************LEAFLET***"
        puts leaflet.name
        puts leaflet.url
        puts leaflet.store_ids
        puts leaflet.start_date
        puts leaflet.end_date
        @report.info "************************LEAFLET***"
        @report.info leaflet.name
        @report.info leaflet.url
        @report.info leaflet.store_ids
        @report.info leaflet.start_date
        @report.info leaflet.end_date
        leaflets << leaflet
      end
    end
    leaflets
  end

  
  def get_redirected_page(redirect_page, code, mechanize, pdf_flag = false)
    begin
      if code.eql?("302")
        redirect_url = redirect_page.response['location']
        if pdf_flag == true
          circular_start = "https://heb.inserts2online.com/HEB"
          return redirect_url if redirect_url.start_with?(circular_start) && redirect_url.end_with?(".pdf")
        end
        mechanize.redirect_ok = false
        sleep(2)
        redirect_page = mechanize.get(redirect_url)
        code = redirect_page.code
      end
    end while !code.eql?("200")
    redirect_page
  end  
  
  
  def parse_hours(hours_text)
    puts "GOT #{hours_text} TO PARSE"
    puts "GOT #{hours_text} TO PARSE"
    week_hours = []
    store_hours = []
    store_hours = hours_text.downcase.split("<br")
    store_hours.each do |store_hour|
      puts "STORE HOUR #{store_hour} TO PROCESS NOW"
      @report.info "STORE HOUR #{store_hour} TO PROCESS NOW"
      unless (store_hour.include?("mon") || store_hour.include?("tue") || 
            store_hour.include?("wed") || store_hour.include?("thu") ||
            store_hour.include?("fri") || store_hour.include?("sat") ||
            store_hour.include?("sun"))
        store_hour = "mon-sun #{store_hour}"
      end
      store_hour = store_hour.gsub("24 hours", "12am-11:59pm") if store_hour.include?("24 hours")
      store_hour = store_hour.gsub("daily", "") if store_hour.include?("daily")
      store_hour = store_hour.gsub("midnight", "11:59pm") if store_hour.include?("midnight")
      store_hour = store_hour.gsub("/7 days a week", "")
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      hours[1] = "11:59pm" if hours[1].include?("midnight") || hours[1].include?("12mid")
      hours[0] = "#{hours[0]}#{get_am_pm(hours[0])}" unless hours[0].include?("am") || hours[0].
        include?("pm")
      hours[1] = "#{hours[1]}#{get_am_pm(hours[1])}" unless hours[1].include?("am") || hours[1].
        include?("pm")                                                                                
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end

  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '5bc1ec2cfc094dfb1b3d3254ca7941ccfde64d6a1fb2adb734caa901ca861bf9'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("https://www.heb.com/sitemap/storeSitemap.xml")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end