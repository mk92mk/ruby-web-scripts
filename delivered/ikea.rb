#5f8bce4fef3eff5cecde6392326f77c919ccc2411d7c8f517cd0e0f8c89cb098 for IKEA

require 'pqsdk'
require 'mechanize'
require 'uri'
require 'byebug'

class Ikea
  

  def get_stores(home_url)
    allStoresIds = []
    base_url = "http://m.ikea.com/us/en/stores/"
    mechanize = Mechanize.new
    home_page = mechanize.get(home_url)
    puts "GOT HOME PAGE"
    stores_hrefs = home_page.search("//li[@class='ikea-text-listitem']//a/@href")
    stores_hrefs.each do |store_href|
      store_url = "#{base_url}#{store_href.value}"
      mechanize = Mechanize.new
      sleep(1)
      begin
        store_details_page = mechanize.get(store_url)
      rescue Exception => e
        case e.message
        when /404/ then puts "404! STORE NOT FOUND AT #{store_details_page}"
          puts "GOING TO NEXT STORE"
          next
        end
      end
      puts "GOT STORE DETAILS PAGE AT #{store_url}"
      store_name = store_details_page.search("//div[@class='ikea-store-name']//text()")[0].text.strip
      store_address = store_details_page.search("//div[@itemprop='streetAddress']//text()")[0].text.strip
      store_city = store_details_page.search("//span[@itemprop='addressLocality']//text()")[0]
                                                                                .text.split(",")[0].strip
      store_zipcode = store_details_page.search("//div[@itemprop='addressRegion']//text()")[0].text.strip
      store_phone = store_details_page.search("//a[@itemprop='telephone']//text()")[0].text.strip
      coords_href = store_details_page.search("//ul[@data-role='listview']//a[contains(text(), 
                                              'Driving Directions')]/@href")[0].value
      coords_url = "#{store_url}#{coords_href}"
      mechanize = Mechanize.new
      sleep(1)
      coords_page = mechanize.get(coords_url)
      puts "GOT COORDS PAGE AT #{coords_url}"
      store_latitude = coords_page.search("//meta[@itemprop='latitude']/@content")[0].value
      store_longitude = coords_page.search("//meta[@itemprop='longitude']/@content")[0].value
      hours_spans = store_details_page.search("//span[@itemprop='openingHours']/@content")
      opening_hours = parse_hours(hours_spans)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      puts "******"
      allStoresIds << store.id
      sleep(4)
    end
    allStoresIds
  end
  
  def parse_hours(hours_spans)
    week_hours = []
    hours_spans.each do |hour_span|
      hour_text = hour_span.value.downcase
      puts "GOING TO PARSE #{hour_text} NOW"
      first_digit = /\d+/.match(hour_text).to_s
      breakpoint = hour_text.index(first_digit)
      days = hour_text[0..breakpoint-1].split("-")
      days = hour_text[0..breakpoint-1].split("–") if hour_text[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = hour_text[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
                                  .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")} 
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end

  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
    
  def get_day_index(day)
    if day.start_with?("mon")
      index = 0
    elsif day.start_with?("tue")
      index = 1
    elsif day.start_with?("wed")
      index = 2
    elsif day.start_with?("thu")
      index = 3  
    elsif day.start_with?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end  
  
  def get_leaflets(store_ids, catalogue_url)
    leaflet_ids = []
    mechanize = Mechanize.new
    sleep(1)
    catalogue_page = mechanize.get(catalogue_url)
    store_leaflet_name = catalogue_page.search("//div[@class='valcontent']//h1")[0].text.strip
    leaflet_end_date = Time.parse(catalogue_page.search("//p[@class='footnote']//text()")[0].text
                                            .gsub("Prices valid through", ""))
    leaflet_url = catalogue_page.search("//p[@class='larger']//a/@href")[0].value
    sleep(1)
    leaflet_page = mechanize.get(leaflet_url)
    leaflet_image_node = leaflet_page.search("//meta[@property='og:image']/@content")[0]
    if leaflet_image_node
      leaflet_image_url = leaflet_image_node.value
      leaflet_image_url = leaflet_page.search("//meta[@property='og:image']/@content")[0].value
      leaflet_end_name = leaflet_url.split("/")[-1].downcase
      leaflet_pdf_url = leaflet_image_url.sub("pages/p_0/small.jpg", "#{leaflet_end_name}_us_en.pdf")#"ikea_catalog_us_en.pdf"
      puts "DOWNLOAD FROM #{leaflet_pdf_url}"
    else
      script_text = leaflet_page.search("//script[contains(text(), 'CATALOGUE_ID')]")[0].text
        leaflet_id = script_text.match(/var CATALOGUE_ID = \"\d+\";/).to_s.match(/\d+/).to_s
        leaflet_end_name = leaflet_url.split("/")[-1].downcase
        leaflet_pdf_url = "http://d1o0y7lcjaivfi.cloudfront.net/2013/media/catalogues/#{leaflet_id}/#{leaflet_end_name}_us_en.pdf"
        mechanize = Mechanize.new
        sleep(1)
        begin
          pdf_file = mechanize.get(leaflet_pdf_url)
        rescue Exception => e
            case e.message
              when /404/ then puts "NO PDF FOR THIS LEAFLET #{leaflet_pdf_url}"
            end
        end
    end
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      byebug
      leaflet.url = leaflet_pdf_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = leaflet_end_date if leaflet_end_date
      leaflet.store_ids = store_ids
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date 
    puts "*******"
    leaflet_ids << leaflet.id
    
    catalogue_sects = catalogue_page.search("//div[@class='catalog']")
    catalogue_sects.each do |catalogue_sect|
      mechanize = Mechanize.new
      store_leaflet_name = catalogue_sect.search(".//h2")[0].text.strip
      leaflet_end_date = catalogue_sect.search(".//p[contains(text(), 
                                               'Prices valid')]")[0].text
                                              .sub("Prices valid through", "").strip
      leaflet_url = catalogue_sect.search(".//a[contains(@id, 'hlnkSubTitle')]/@href")[0].value
      leaflet_page = mechanize.get(leaflet_url)
      leaflet_image_node = leaflet_page.search("//meta[@property='og:image']/@content")[0]
      leaflet_end_name = leaflet_url.split("/")[-1].downcase
      if leaflet_image_node
        leaflet_image_url = leaflet_image_node.value 
        leaflet_pdf_url = leaflet_image_url.sub("pages/p_0/small.jpg", "#{leaflet_end_name}_us_en.pdf")
        leaflet = PQSDK::Leaflet.find leaflet_pdf_url
        if leaflet.nil?
          leaflet = PQSDK::Leaflet.new
          leaflet.name = store_leaflet_name
          leaflet.url = leaflet_pdf_url
          leaflet.start_date = Time.now.to_s
          leaflet.end_date = leaflet_end_date if leaflet_end_date
          leaflet.store_ids = store_ids
        end
        # leaflet.save
        puts "************************LEAFLET***"
        puts leaflet.name
        puts leaflet.url
        puts leaflet.store_ids
        puts leaflet.start_date
        puts leaflet.end_date 
        puts "*******"
        leaflet_ids << leaflet.id
      else
        script_text = leaflet_page.search("//script[contains(text(), 'CATALOGUE_ID')]")[0].text
        leaflet_id = script_text.match(/var CATALOGUE_ID = \"\d+\";/).to_s.match(/\d+/).to_s
        leaflet_pdf_url = "http://d1o0y7lcjaivfi.cloudfront.net/2013/media/catalogues/#{leaflet_id}/#{leaflet_end_name}_us_en.pdf"
        mechanize = Mechanize.new
        sleep(1)
        begin
          pdf_file = mechanize.get(leaflet_pdf_url)
        rescue Exception => e
            case e.message
              when /404/ then puts "NO PDF FOR THIS LEAFLET #{leaflet_pdf_url}"
               next 
            end
        end
        leaflet = PQSDK::Leaflet.find leaflet_pdf_url
        if leaflet.nil?
          leaflet = PQSDK::Leaflet.new
          leaflet.name = store_leaflet_name
          leaflet.url = leaflet_pdf_url
          leaflet.start_date = Time.now.to_s
          leaflet.end_date = leaflet_end_date if leaflet_end_date
          leaflet.store_ids = store_ids
        end
        # leaflet.save
        puts "************************LEAFLET***"
        puts leaflet.name
        puts leaflet.url
        puts leaflet.store_ids
        puts leaflet.start_date
        puts leaflet.end_date 
        puts "*******"
      end    
    end
    leaflet_ids
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '5f8bce4fef3eff5cecde6392326f77c919ccc2411d7c8f517cd0e0f8c89cb098'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores("http://m.ikea.com/us/en/stores/")
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://info.ikea-usa.com/Catalog/")
    puts "JUST ENDED THE SITE DATA"
  end
end