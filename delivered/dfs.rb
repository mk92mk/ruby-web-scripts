#132faf3260dfacdb04f740f009fcc95e0403617d693d79ffa4ce1a5537fe4055 for DFS
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'


class Dfs
  
  def get_stores
    allStoresIds = []
    stores_list_url = "http://www.dfs.co.uk/store-directory"    
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES DIRECTORY PAGE"
    stores_urls = stores_list_page.search("//li[contains(@class, 'store grid-item')]//a[contains(@class, 
                                            'moreBttonRight')]/@href")
    stores_urls.each do |store_url|
      mechanize = Mechanize.new
      store_details_page = mechanize.get(store_url.value)
      puts "GOT STORE DETAILS PAGE AT #{store_url.value}"
      store_name = store_details_page.search("//h3[@itemprop='legalName']")[0].text.strip
      store_address = store_details_page.search("//span[@itemprop='streetAddress']")[0].text.lines
                                        .map{|st| st.strip}.join
      store_city_node = store_details_page.search("//span[@itemprop='addressLocality']")[0]
      if store_city_node
        store_city = store_city_node.text.strip
      else
        store_city = store_details_page.search("//span[@itemprop='addressRegion']")[0].text.strip
      end
      store_zipcode_node = store_details_page.search("//span[@itemprop='postalCode']")[0]
      if store_zipcode_node
        store_zipcode = store_zipcode_node.text.strip
      else
        store_zipcode = "00000"
      end
      store_phone = store_details_page.search("//a[contains(@href, 'tel')]/@href")[0].value
                                      .gsub("tel:", "").lines.map{|sp| sp.strip}.join
      script_text = store_details_page.search("//script[contains(text(), 'var myStoreLat')]")[0].text
      store_latitude = script_text.match(/var myStoreLat = \d+.\d+|var myStoreLat = -\d+.\d+/).to_s
                                  .match(/\d+.\d+|-\d+.\d+/).to_s
      store_longitude = script_text.match(/var myStoreLon = \d+.\d+|var myStoreLon = -\d+.\d+/).to_s
                                   .match(/\d+.\d+|-\d+.\d+/).to_s
      store_hours_nodes = store_details_page.search("//div[@class='openingTimes'][contains(.//h3/text(), 
                                      'Office opening hours')]//li[@itemprop='openingHours']")
      puts "ON #{store_address} NOW"
      opening_hours = parse_hours(store_hours_nodes)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url.value
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url.value}"
      puts "******"      
      allStoresIds << store.id
    end
    allStoresIds
  end
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      week_days = store_hour_node.search(".//span[@class='day']")[0].text.downcase.strip
      open_hours = store_hour_node.search(".//span[@class='times']")[0].text.downcase.strip
      puts "GOING TO PARSE #{week_days} AND #{open_hours} NOW"
      days = week_days.split("-")
      days = week_days.split("–") if week_days.include?("–")
      days = week_days.split("to") if week_days.include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
                                .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '980ef1ed79a909d3961327573868d7950745ca42cffc835baa2d39c4609abbe1'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
  end  
end
