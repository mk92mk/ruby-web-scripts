#3c7500a7d25cccc2a4d40bf7097c20ce013790421ff066873a80cb89b28e8c96 for Pavers
require 'pqsdk'
require 'byebug'
require 'mechanize'
require 'json'

class Crawler::Uk::Pavers < Crawler::Base
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def get_stores
    allStoresIds = []
    base_url = "http://web3.aws.pavers.co.uk/api/v1/branch/"
    cities = PQSDK::City.all.select{|c| c.state == 'confirmed'}.map{|c| c.name.strip.downcase}
    mechanize = Mechanize.new
    stores_page = mechanize.get(base_url)
    store_jsons = stores_page.body
    stores_list = store_jsons.gsub("var branchArray = ","").gsub(";","")
    stores_parsed = JSON.parse(stores_list)
    @report.info << "TOTAL #{stores_parsed.length} STORES FOUND"
    stores_parsed.each do |store_item|
      store_name = store_item["branch_name"]
      store_address = store_item["address1"]
      store_phone = store_item["phone"]
      store_zipcode = store_item["postcode"]
      store_latitude = store_item["latitude"]
      store_longitude = store_item["longitude"]
      working_hours = store_item["openinghours"]
      stores_url = base_url
      opening_hours = parse_hours(working_hours)
      if store_address.count("\n") >= 1
#        address_parts = store_address.lines.map{|a| a.gsub(",","").strip}
         address_parts = store_address.lines.map{|a| a.strip.split(",")}.flatten.map{|a| a.strip}
      elsif store_address.count(",") >= 1
        address_parts = store_address.split(",")
      else
        @report.info << "UNUSUAL SITUATION CAUGHT AND NEED TO BE HANDLED"
      end
      store_city = ""
      address_parts.reverse.each do |address_part|
        if cities.include?(address_part.strip.downcase)
          store_city = address_part.strip
          break
        end
      end
      if store_city.empty?
        unless address_parts[-1] =~ /\d/
          unless address_parts[-1].chomp('.').end_with?("shire")
            store_city = address_parts[-1].strip
          end
        end
        if store_city.empty?
          store_city = address_parts[-2].strip 
        end
      end
      store_address = store_address.lines.map{|sa| sa.strip}.join(" ") 
      store_address = store_address.gsub(store_zipcode, "")
      store_address = store_address.strip.chomp(",") 
      temp_address = store_address.split(",").map{|a| a.strip} if store_address.count(",") >= 2
      if temp_address
        temp_address.delete("")
        store_address = temp_address.join(",")
      end
      store_address = store_address.squeeze(" ")
#      store_address = store_address.gsub(",#{store_city}", "") if store_address.include?(",")
#      store_address = store_address.gsub(" #{store_city}", "") if store_address.count(",") < 1
      if store_address.include?("#{store_city}shire")
        store_address = store_address.reverse.sub("#{store_city.reverse},", "").reverse 
      else
        store_address = store_address.reverse.sub(store_city.reverse, "").reverse
      end
      store_address = store_address.strip.chomp(",").squeeze(",")
      store_zipcode = "00000" if store_zipcode.empty? || !(store_zipcode =~ /\d/)
      store_latitude = "0" if store_latitude.nil? || store_latitude.empty?
      store_longitude = "0" if store_longitude.nil? || store_longitude.empty?
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      store.save
      allStoresIds << store.id
      @report.info <<  "*************STORE*********************"
      @report.info <<  "STORE NAME IS #{store_name}"
      @report.info <<  "STORE CITY IS #{store_city}"
      @report.info <<  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info <<  "STORE ADDRESS IS #{store_address}"
      @report.info <<  "STORE LONGITUDE #{store_longitude}"
      @report.info <<  "STORE LATITUDE #{store_latitude}"
      @report.info <<  "STORE PHONE #{store_phone}"
      @report.info <<  "STORE HOURS #{opening_hours}"
      @report.info <<  "STORE URL #{stores_url}"
    end
    allStoresIds
  end
  
  def parse_hours(hours_array)
    hours_lines = hours_array.lines.map{|hr| hr.strip.gsub("<br>","").gsub("<br,>","").gsub("<br","")}
    week_hours = []
    hours_lines.each do |hour_line|
      next if hour_line.strip.empty?
      next unless (hour_line.include?("Mon") || hour_line.include?("Tue") || 
          hour_line.include?("Wed") || hour_line.include?("Thu") ||
          hour_line.include?("Fri") || hour_line.include?("Sat") ||
          hour_line.include?("Sun"))
      hour_line = hour_line.downcase
      if hour_line.include?("&") #Mon-Wed & Fri:10:00-18:30
        first_digit = /\d+/.match(hour_line).to_s
        breakpoint = hour_line.index(first_digit)
        days_parts = hour_line[0..breakpoint-1]
        open_hours = hour_line[breakpoint..-1]
        days_split = days_parts.split("&")
        days_first = days_split[0]
        days_second = days_split[1]
        hours_lines << "#{days_split[0]} #{open_hours}"
        hours_lines << "#{days_split[1]} #{open_hours}"
        next
      end
      first_digit = /\d+/.match(hour_line).to_s
      breakpoint = hour_line.index(first_digit)
      days = hour_line[0..breakpoint-1].split("-")
      days = days.map{|d| d.strip}
      days.delete("")  
      open_hours = hour_line[breakpoint..-1]
      open_hours = open_hours.gsub(".", ":")
      open_hours = open_hours.split("(open")[0] if open_hours.include?("(open")# Sun: 11:00 - 17:00 (open 10:30 for browsing)
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = hours.map{|h| h.strip}
      hours[1] = "#{hours[1][0..1]}:#{hours[1][2..-1]}" if hours[1] && hours[1].count(":") < 1 #Thu & Fri: 09:30-2100
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            if open_hours.include?("closed")
              day_hours["closed"] = true
              week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
              next
            end
            day_hours["open_am"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("am")
            day_hours["open_pm"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("pm")
            day_hours["close_am"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("am")
            day_hours["close_pm"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("pm")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            if open_hours.include?("closed")
              day_hours["closed"] = true 
              week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
              next
            end
            day_hours["open_am"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("am")
            day_hours["open_pm"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("pm")
            day_hours["close_am"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("am")
            day_hours["close_pm"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("pm")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        if open_hours.include?("closed")
          day_hours["closed"] = true if open_hours.include?("closed")
          week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
          next
        end
        day_hours["open_am"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("am")
        day_hours["open_pm"] = hours[0].rjust(5, "0") if check_am_pm(hours[0]).eql?("pm")
        day_hours["close_am"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("am")
        day_hours["close_pm"] = hours[1].rjust(5, "0") if check_am_pm(hours[1]).eql?("pm")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def check_am_pm(timing)
    time_split = timing.split(":")
    hour = time_split[0].to_i
    if hour >=0 && hour <= 11
      day_part = "am"
    elsif hour >= 12 && hour <= 23
      day_part = "pm"
    end
    day_part
  end
  
  def get_leaflets(store_ids, leaflets_url)
    allLeaflets = []
    leaflet_name = "Pavers_Leaflet"
    mechanize = Mechanize.new
    leaflets_page = mechanize.get(leaflets_url).body.gsub("init(","").gsub(");","")
    page_parsed = JSON.parse(leaflets_page)
    images_urls = []
    base_url = page_parsed["config"]["pageFullImageDir"]
    counter = 0
    temp = true
    begin
      leaflet_image_url = "#{base_url}#{counter}.jpg"
      begin
        mechanize = Mechanize.new
        image_page = mechanize.get(leaflet_image_url)
        sleep(2)
      rescue Exception => e
        case e.message
        when /404/ then @report.info << "End of brochure reached "
          temp=false
          break
        end
      end
      if image_page.code.eql?("200")
        images_urls << leaflet_image_url
        @report.info << "#{leaflet_image_url} ADDED IN MAIN ARRAY"
      end
      counter = counter + 1
    end while temp
    leaflet = PQSDK::Leaflet.find leaflets_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = leaflet_name
      leaflet.url = leaflets_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = Time.now.to_s        
      leaflet.image_urls = images_urls
      leaflet.store_ids = store_ids
      leaflet.save   
      allLeaflets << leaflet     
      @report.info  "********LEAFLET***START******"
      @report.info  leaflet
      @report.info "********LEAFLET END********"
    end
    allLeaflets
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '3c7500a7d25cccc2a4d40bf7097c20ce013790421ff066873a80cb89b28e8c96'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.pavers.co.uk/media/wysiwyg/page/online-catalogue/catalogue-data.json")
    @report.info << "JUST ENDED THE SITE DATA"
  end
end