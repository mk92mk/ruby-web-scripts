# 86af1acced64ccc24384101ebe300695fe5a975d8daf27562d9bfb9eb2ceed29 for ctown_supermarkets.rb
require 'pqsdk'
require 'mechanize'
require 'uri'

class CtownSupermarkets
  
  def initialize
    @report = Logger.new("Ctown_Supermarkets#{Time.now.to_s}.log")
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    sleep(2)
    states_page = mechanize.get(states_url)
    puts "GOT STATES PAGE"
    @report.info "GOT STATES PAGE"    
    states_list = states_page.search("//div[@class='inside']//a[contains(@linktrack,
                                                         'State index page')]/@href")
    states_list.each do |state_element|
      mechanize = Mechanize.new
      sleep(3)
      cities_page = mechanize.get(state_element.value)
      puts "GOT CITIES PAGE ON #{state_element.value}"
      @report.info "GOT CITIES PAGE ON #{state_element.value}"    
      cities_list = cities_page.search("//a[contains(@linktrack,'City index page')]/@href")
                                                                   .map{|cl| cl.value}.uniq
      cities_list.each do |city_element|
        mechanize = Mechanize.new
        sleep(2)
        city_stores_page = mechanize.get(city_element)
        puts "GOT CITY STORES LIST ON #{city_element}"
        @report.info "GOT CITY STORES LIST ON #{city_element}"
        city_stores_list = city_stores_page.search("//a[contains(@linktrack,'Location page')]/@href")
        city_stores_list.each do |store_element|
          mechanize = Mechanize.new
          sleep(1)
          begin
            store_page = mechanize.get(store_element.value)
          rescue Exception => exception
            puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
            @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
            next
          end
          puts "GOT STORES PAGE ON #{store_element.value}"
          @report.info "GOT STORES PAGE ON #{store_element.value}"
          store_name = store_page.search("//span[@itemprop='name']")[0].text.strip
          store_address = store_page.search("//span[@itemprop='streetAddress']")[0].text.strip
          store_city = store_page.search("//span[@itemprop='addressLocality']")[0].text.strip.capitalize
          store_zipcode = store_page.search("//span[@itemprop='postalCode']")[0].text.strip
          store_phone = store_page.search("//span[@itemprop='telephone']")[0].text.strip
          store_latitude = store_page.search("//meta[@property='place:location:latitude']/@content")[0]
                                            .value
          store_longitude = store_page.search("//meta[@property='place:location:longitude']/@content")[0]
                                            .value
          working_hours = store_page.search("//div[@class='headerB'][contains(text(),
                                            'Hours of Operation')]//following-sibling::div")[0].text
          .strip
          opening_hours = parse_hours(working_hours)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude
            store.longitude = store_longitude
            store.phone = store_phone
            store.origin = store_element.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}"
          puts  "STORE LATITUDE #{store_latitude}"
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{store_element.value}"
          @report.info  "*************STORE*********************"
          @report.info  "STORE NAME IS #{store_name}"
          @report.info  "STORE CITY IS #{store_city}"
          @report.info  "STORE ZIP CODE IS #{store_zipcode}"
          @report.info  "STORE ADDRESS IS #{store_address}"
          @report.info  "STORE LONGITUDE #{store_longitude}"
          @report.info  "STORE LATITUDE #{store_latitude}"
          @report.info  "STORE PHONE #{store_phone}"
          @report.info  "STORE HOURS #{opening_hours}"
          @report.info  "STORE URL #{store_element.value}"
          store_leaflet_href = store_page.search("//div[@class='service_icon']//a/@onclick")[0].value
          store_leaflet_url = URI.extract(store_leaflet_href)[-1].chomp("');")
          store_leaflet_url = "h#{store_leaflet_url}" if store_leaflet_url.start_with?("ttp://")
          store_leaflets = get_leaflets(store.id, store_leaflet_url)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
          allStoresIds << store.id
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def parse_hours(hours_text)
    store_hours = []
    week_hours = []
    hours_lines = hours_text
    puts "GOT #{hours_text} TO PARSE"
    @report.info "GOT #{hours_text} TO PARSE"
    unless (hours_text.downcase.include?("mon") || hours_text.downcase.include?("tue") || 
        hours_text.downcase.include?("wed") || hours_text.downcase.include?("thu") ||
        hours_text.downcase.include?("fri") || hours_text.downcase.include?("sat") ||
        hours_text.downcase.include?("sun"))
      if hours_text.downcase.include?("am") || hours_text.downcase.include?("pm")
        hours_text = "Mon-Sun #{hours_text}"
      end
    end
    if hours_text.downcase.eql?("open 24 hours")
      hours_text = "Mon-Sun 12am-11:59pm"
    end
    holiday_index = hours_lines.downcase.index("holiday hours")
    hours_text = hours_lines[0..holiday_index-1] if holiday_index
    christmas_index = hours_lines.downcase.index("christmas eve")
    hours_text = hours_lines[0..christmas_index-1] if christmas_index
    first_digit = /\d+/.match(hours_text).to_s
    breakpoint = hours_text.index(first_digit)
    temp_days = hours_text[0..breakpoint-1]
    temp_mixed = hours_text[breakpoint..-1]
    temp_indexes = []
    if temp_mixed.downcase.include?("mon") || temp_mixed.downcase.include?("tue") || 
            temp_mixed.downcase.include?("wed") || temp_mixed.downcase.include?("thu") ||
            temp_mixed.downcase.include?("fri") || temp_mixed.downcase.include?("sat") ||
            temp_mixed.downcase.include?("sun")
      temp_mixed = temp_mixed.downcase
      mon_index = temp_indexes << temp_mixed.index("mon") if temp_mixed.index("mon")
      tue_index = temp_indexes << temp_mixed.index("tue") if temp_mixed.index("tue")
      wed_index = temp_indexes << temp_mixed.index("wed") if temp_mixed.index("wed")
      thu_index = temp_indexes << temp_mixed.index("thu") if temp_mixed.index("thu")
      fri_index = temp_indexes << temp_mixed.index("fri") if temp_mixed.index("fri")
      sat_index = temp_indexes << temp_mixed.index("sat") if temp_mixed.index("sat")
      sun_index = temp_indexes << temp_mixed.index("sun") if temp_mixed.index("sun")
      temp_indexes = temp_indexes.sort
      if temp_indexes.length == 1
        store_hours << temp_mixed[temp_indexes[0]..-1]
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
      elsif temp_indexes.length == 2
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
        if temp_indexes[1] - temp_indexes[0] > 6
          store_hours << temp_mixed[temp_indexes[0]..temp_indexes[1]-1]
          store_hours << temp_mixed[temp_indexes[1]..-1]
        end
        if temp_indexes[1] - temp_indexes[0] <= 6
          store_hours << temp_mixed[temp_indexes[0]..-1]
        end
      elsif temp_indexes.length == 4
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
        if temp_indexes[1] - temp_indexes[0] <= 6
          store_hours << temp_mixed[temp_indexes[0]..temp_indexes[2]]
        end
        if temp_indexes[3] - temp_indexes[2] > 6
          store_hours << temp_mixed[temp_indexes[2]..temp_indexes[3]-1]
        end
        store_hours << temp_mixed[temp_indexes[3]..-1]
      end
    else
      store_hours << hours_text
    end
    
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase   
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
                                     !temp_days.include?("to") && !temp_days.include?("–")
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      if days.length == 3
        store_hours << "#{days[0]}-#{days[2]} #{open_hours}"
        next
      end
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
      if !hours[0].include?("am") && !hours[0].include?("pm")
        open_am_pm = get_am_pm(hours[0])
      end
      if !hours[1].include?("am") && !hours[1].include?("pm")
        close_am_pm = get_am_pm(hours[1])
      end
      if days.length == 2 # > 1(before)
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")||
                                                                                open_am_pm.eql?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")||
                                                                                open_am_pm.eql?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")||
                                                                                close_am_pm.eql?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")||
                                                                                close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")||
                                                                                open_am_pm.eql?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")||
                                                                                open_am_pm.eql?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")||
                                                                                close_am_pm.eql?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")||
                                                                                close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")||
          open_am_pm.eql?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")||
          open_am_pm.eql?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")||
          close_am_pm.eql?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")||
          close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end
    end
    week_hours
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_leaflets(store_id, leaflet_url)
    storeLeaflets = []
    base_url = "http://ctownsupermarkets.inserts2online.com/"
    mechanize = Mechanize.new
    mechanize.redirect_ok = false
    begin
      leaflet_base_page = mechanize.get(leaflet_url)
    rescue Exception => exception
      puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
      @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
      return storeLeaflets
    end
    code = leaflet_base_page.code
    begin
      leaflet_base_page = get_redirected_page(leaflet_base_page, code, mechanize) if code.eql?("302")
    rescue Exception => exception
      puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
      @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
      return storeLeaflets
    end
    if leaflet_base_page.uri.to_s.include?("unavailable.jsp")
      puts "LEAFLET UNAVAILABLE AT THIS TIME"
      @report.info "LEAFLET UNAVAILABLE AT THIS TIME"
      return storeLeaflets
    end
    if leaflet_base_page.uri.to_s.include?("I2O_MainFrame.jsp")
      begin
        leaflet = get_create_leaflet(leaflet_base_page, base_url, mechanize, store_id)
      rescue Exception => exception
        puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
        @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
        return storeLeaflets
      end
      storeLeaflets << leaflet
    elsif leaflet_base_page.uri.to_s.include?("availableAds.jsp")
      leaflet_hrefs = leaflet_base_page.search("//a[@class='aahref']/@href")
      leaflet_hrefs.each do |leaflet_href|
        leaflet_url = "#{base_url}#{leaflet_href.value}"
        begin
          leaflet_base_page = mechanize.get(leaflet_url)
        rescue Exception => exception
          puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
          @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace
                                                                                    .join("\n")}"
          next
        end  
        code = leaflet_base_page.code
        begin
          leaflet_base_page = get_redirected_page(leaflet_base_page, code, mechanize) if code.eql?("302")
        rescue Exception => exception
          puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
          @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace
                                                                                    .join("\n")}"
          next
        end
        if leaflet_base_page.uri.to_s.include?("unavailable.jsp")
          puts "LEAFLET UNAVAILABLE AT THIS TIME"
          @report.info "LEAFLET UNAVAILABLE AT THIS TIME"
          next
        elsif leaflet_base_page.uri.to_s.include?("I2O_MainFrame.jsp")
          begin
            leaflet = get_create_leaflet(leaflet_base_page, base_url, mechanize, store_id)
          rescue Exception => exception
            puts "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace.join("\n")}"
            @report.info "THE EXCEPTION CODE CAUGHT #{exception.message}\n--#{exception.backtrace
                                                                                .join("\n")}"
            next
          end  
          storeLeaflets << leaflet
        end
      end
    end
    storeLeaflets   
  end
  
  def get_create_leaflet(leaflet_base_page, base_url, mechanize, store_id)
    store_leaflet_name = "CTown_Supermarkets_leaflet"
    store_leaflet_src = leaflet_base_page.search("//frame[@id='Large']/@src")[0]  
    store_leaflet_href = store_leaflet_src.value
    leaflet_intermediate_url = "#{base_url}#{store_leaflet_href}"
    sleep(1)
    leaflet_inter_page = mechanize.get(leaflet_intermediate_url)
    pdf_base_url = "#{base_url}version_pdf.jsp"
    pdf_page = mechanize.get(pdf_base_url)
    code = pdf_page.code
    pdf_url = get_redirected_page(pdf_page, code, mechanize, true) if code.eql?("302")
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = pdf_url
      leaflet.name = store_leaflet_name
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    leaflet
  end
  
  def get_redirected_page(redirect_page, code, mechanize, pdf_flag=false)
    begin
      if code.eql?("302")
        redirect_url = redirect_page.response['location']
        if pdf_flag == true
          circular_start = "http://ctownsupermarkets.inserts2online.com/KrasdaleCTownWeeklyCircular"
          return redirect_url if redirect_url.start_with?(circular_start) && redirect_url.end_with?(".pdf")
        end
        mechanize.redirect_ok = false
        sleep(1)
        
        redirect_page = mechanize.get(redirect_url)
        code = redirect_page.code
      end
    end while !code.eql?("200")
    redirect_page
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '86af1acced64ccc24384101ebe300695fe5a975d8daf27562d9bfb9eb2ceed29'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://location.ctownsupermarkets.com/")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end