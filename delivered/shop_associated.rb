#e04a77d19baaa8f0dd13f53b168172e96c4c00666d3d1885eeb4507bc66bedb7 for shop associated
require 'pqsdk'
require 'mechanize'
require 'json'

class ShopAssociated
  
  def initialize
    @report = Logger.new("shop_associated#{Time.now.to_s}.log")
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue") 
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  
  def get_stores
    allStoresIds = []
    base_url = "http://www.shopassociated.com/wp-admin/admin-ajax.php?action=store_search&lat=&lng=&max_results=&radius=&autoload=1"
    mechanize = Mechanize.new
    stores_page = mechanize.get(base_url)
    store_jsons = stores_page.body
    stores_parsed = JSON.parse(store_jsons)
    stores_parsed.each do |store_item|
      store_name = store_item["store"]
      store_address = store_item["street"]
      store_city = store_item["city"]
      store_phone = store_item["phone"]
      store_zipcode = store_item["zip"]
      store_latitude = store_item["lat"]
      store_longitude = store_item["lng"]
      working_hours = store_item["hours"]
      stores_url = base_url
      opening_hours = parse_hours(working_hours)
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{stores_url}"
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
#      store.save
      allStoresIds << store
    end
    allStoresIds
  end
  
  def parse_hours(hours_text)
    hours_lines = hours_text.lines.map{|hr| hr.strip.gsub("<p>","").gsub("</p>","")}
    store_hours = []
    week_hours = []
    hours_lines.each do |hour_line|
      if hour_line.downcase.include?("sun") && hour_line.downcase.include?("m-sat") #Sunday 8a-7p M-Sat 8a-9p
        breakpoint = hour_line.downcase.index("m-sat")
        child_text1 = hour_line[0..breakpoint-1]
        child_text2 = hour_line[breakpoint..-1]
        store_hours << child_text1
        store_hours << child_text2.downcase.gsub("m-sat", "mon-sat")
      elsif hour_line.downcase.eql?("24 hours")
        store_hours << "mon-sun 12am-11:59pm"
      elsif (hour_line.downcase.include?("am") || hour_line.downcase.include?("pm")) &&  # for 7am-9pm
            !(hour_line.downcase.include?("mon") || hour_line.downcase.include?("tue") || 
            hour_line.downcase.include?("wed") || hour_line.downcase.include?("thu") ||
            hour_line.downcase.include?("fri") || hour_line.downcase.include?("sat") ||
            hour_line.downcase.include?("sun"))
        store_hours << "mon-sun #{hour_line}"
      else
        store_hours << hour_line
      end
    end
    
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase   
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")  if store_hour.include?("-")
      days = store_hour[0..breakpoint-1].split("to") if store_hour.include?("to")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a","")) if 
                                                                        hours[0].include?("am")||
                                                                        hours[0].include?("a")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p","")) if 
                                                                        hours[0].include?("pm")||
                                                                        hours[0].include?("p")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a","")) if 
                                                                        hours[1].include?("am")||
                                                                        hours[1].include?("a")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p","")) if 
                                                                        hours[1].include?("pm")||
                                                                        hours[1].include?("p")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a","")) if 
                                                                        hours[0].include?("am")||
                                                                        hours[0].include?("a")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p","")) if 
                                                                        hours[0].include?("pm")||
                                                                        hours[0].include?("p")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a","")) if 
                                                                        hours[1].include?("am")||
                                                                        hours[1].include?("a")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p","")) if 
                                                                        hours[1].include?("pm")||
                                                                        hours[1].include?("p")        
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a","")) if 
                                                                          hours[0].include?("am")||
                                                                          hours[0].include?("a")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p","")) if 
                                                                          hours[0].include?("pm")||
                                                                          hours[0].include?("p")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a","")) if 
                                                                          hours[1].include?("am")||
                                                                          hours[1].include?("a")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p","")) if 
                                                                          hours[1].include?("pm")||
                                                                          hours[1].include?("p")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour.eql?("12")
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_leaflets(store_ids, leaflets_url)
    allLeaflets = []
    mechanize = Mechanize.new
    leaflets_page = mechanize.get(leaflets_url)
    leaflet_images = leaflets_page.search("//div[@id='circular-pages']//img/@src")
    images_urls = []
    leaflet_images.each do |leaflet_image|
      mechanize = Mechanize.new
      mechanize.redirect_ok = false
      image_page = mechanize.get(leaflet_image.value)
      status_code = image_page.code
      if status_code.eql?("302")
        redirected_url = image_page["location"]
        mechanize = Mechanize.new
        mechanize.redirect_ok = false
        image_page = mechanize.get(redirected_url)
        status_code = image_page.code
      end
      if status_code.eql?("200")
        if redirected_url
          images_urls << redirected_url
        else
          images_urls << leaflet_image.value
        end
        @report.info "#{redirected_url} ADDED IN MAIN ARRAY"
      end
      sleep(1)
    end
    leaflet_name = "ShopAssociated_#{leaflets_page.search("//div[@class='main']/h1/text()")[0].text}"
    leaflet = PQSDK::Leaflet.find leaflets_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = leaflet_name
      leaflet.url = leaflets_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = Time.now.to_s        
      leaflet.image_urls = images_urls
      leaflet.store_ids = store_ids
#      leaflet.save   
      allLeaflets << leaflet     
      @report.info  "********LEAFLET***START******"
      @report.info  leaflet
      @report.info "********LEAFLET END********"
    end
    allLeaflets
    @report.info "ending the site data"
  end
  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'e04a77d19baaa8f0dd13f53b168172e96c4c00666d3d1885eeb4507bc66bedb7'
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.shopassociated.com/weekly-circular/")
    @report.info "JUST ENDED THE SITE DATA"
  end
end