#2bf27a63d181cd616b2edbbf99f725793322ac78caaae64f3cd7041d5a48355e for FOOD TOWN
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class FoodTown
  
  def get_stores(token_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
#    token_page = mechanize.get(token_url)
    mechanize.get("https://www.foodtown.com/my-store/store-locator")
    puts "GOT TOKEN PAGE"
    sleep(1)
    stores_url = "https://api.freshop.com/1/stores?is_selectable=true&has_address=true&token=082cf5639773ea61955d6bfd55830834&app_key=foodtown"
    stores_page = mechanize.get(stores_url)
    puts "GOT STORES PAGE"
    json_parsed = JSON.parse(stores_page.body)
    stores_list = json_parsed["items"]
    stores_list.each do |store_item|
#      store_item = json_parsed["items"].find {|h1| h1['name']=='Foodtown of Boston Post Road'}
      store_name = store_item["name"]
      store_address = store_item["address_1"]
      store_city = store_item["city"]
      store_zipcode = store_item["postal_code"]
      store_phone = store_item["phone"].lines[0].sub("Store:", "") if store_item["phone"]
      store_latitude = store_item["latitude"]
      store_longitude = store_item["longitude"]
      food_store_id = store_item["id"]
      opening_times = store_item['hours']
      opening_hours = parse_hours(opening_times)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_url}"
      puts "******"
      allStoresIds << store.id
      store_leaflets = get_leaflets(store.id, food_store_id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, food_store_id)
    leaflets = []
    store_leaflet_url = "https://api.freshop.com/1/circulars?limit=1&token=082cf5639773ea61955d6bfd55830834&store_id=#{food_store_id}&app_key=foodtown"
    mechanize = Mechanize.new
    sleep(2)
    token_page = mechanize.get("https://api.freshop.com/1/sessions?token=082cf5639773ea61955d6bfd55830834&app_key=foodtown")
    puts "GOT TOKEN PAGE "
    sleep(1)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT LEAFLETS PAGE AT #{store_leaflet_url}"
    json_items = JSON.parse(store_leaflet_page.body)["items"]
    unless (json_items && json_items[0] && json_items[0]["pages"])
      puts "NO LEAFLET PAGE FOUND FOR THE STORE #{food_store_id}"
      return leaflets
    end
    pages_list = JSON.parse(store_leaflet_page.body)["items"][0]["pages"]
    store_leaflet_name = JSON.parse(store_leaflet_page.body)["items"][0]["name"]
    leaflet_start_date = JSON.parse(store_leaflet_page.body)["items"][0]["start_date"]
    leaflet_end_date = JSON.parse(store_leaflet_page.body)["items"][0]["finish_date"]
    images_urls = []
    pages_list.each do |image_page|
      page_id = image_page["reference_id"]
      page_src = "https://circulars.freshop.com/#{page_id}_large.jpg"
      images_urls << page_src
      puts "#{page_src} ADDED IN THE MAIN ARRAY FOR LEAFLET"
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = store_leaflet_url
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date     
      leaflet.image_urls = images_urls 
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    #leaflet.save 
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(opening_times)
    opening_times = opening_times.downcase
    puts "GOT #{opening_times} TO PARSE"
    opening_times = opening_times.split("pharmacy")[0]
    week_hours = []
    store_hours = []
    if opening_times.include?("open everyday") || opening_times.include?("7 days a week")
      opening_times = opening_times.sub("open everyday", "mon-sun").sub("7 days a week", "mon-sun")
      store_hours << opening_times
    elsif opening_times.include?(",")
      opening_hours = opening_times.split(",")
      opening_hours.map{|oh| store_hours << oh}
    elsif opening_times.include?("\r\r") && !opening_times.include?(",")
      opening_hours = opening_times.split("\r\r")
      opening_hours.map{|oh| store_hours << oh}
    else
      unless opening_times.include?(",")
        if opening_times.start_with?("store: m-")
          opening_times = opening_times.sub("store:", "").sub("m-f", "mon-fri").sub("m-s", "mon-sun")
                                        .split("pharmacy")[0]
        elsif opening_times.start_with?("store: 6am") && opening_times.include?("everyday")
          opening_times = opening_times.sub("store:", "mon-sun").split("pharmacy")[0].sub("everyday", "")
        elsif opening_times.start_with?("everyday ")
          opening_times = opening_times.sub("everyday", "mon-sun")
        end
        hours_text = opening_times
        first_digit = /\d+/.match(hours_text).to_s
        breakpoint = hours_text.index(first_digit)
        temp_days = hours_text[0..breakpoint-1]
        temp_mixed = hours_text[breakpoint..-1]
        temp_indexes = []
        if temp_mixed.downcase.include?("mon") || temp_mixed.downcase.include?("tue") || 
            temp_mixed.downcase.include?("wed") || temp_mixed.downcase.include?("thu") ||
            temp_mixed.downcase.include?("fri") || temp_mixed.downcase.include?("sat") ||
            temp_mixed.downcase.include?("sun")
          temp_mixed = temp_mixed.downcase
          mon_index = temp_indexes << temp_mixed.index("mon") if temp_mixed.index("mon")
          tue_index = temp_indexes << temp_mixed.index("tue") if temp_mixed.index("tue")
          wed_index = temp_indexes << temp_mixed.index("wed") if temp_mixed.index("wed")
          thu_index = temp_indexes << temp_mixed.index("thu") if temp_mixed.index("thu")
          fri_index = temp_indexes << temp_mixed.index("fri") if temp_mixed.index("fri")
          sat_index = temp_indexes << temp_mixed.index("sat") if temp_mixed.index("sat")
          sun_index = temp_indexes << temp_mixed.index("sun") if temp_mixed.index("sun")
          temp_indexes = temp_indexes.sort
          if temp_indexes.length == 1
            store_hours << temp_mixed[temp_indexes[0]..-1]
            store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
          elsif temp_indexes.length == 2
            store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
            if temp_indexes[1] - temp_indexes[0] > 6
              store_hours << temp_mixed[temp_indexes[0]..temp_indexes[1]-1]
              store_hours << temp_mixed[temp_indexes[1]..-1]
            end
            if temp_indexes[1] - temp_indexes[0] <= 6
              store_hours << temp_mixed[temp_indexes[0]..-1]
            end
          end
        else
          store_hours << hours_text
        end
      end
    end
    
    store_hours.each do |store_hour|
      puts "GOING TO PARSE #{store_hour} NOW"
      if store_hour.include?("/")
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        days = store_hour[0..breakpoint-1].split("/")
        days.map{|d| store_hours << "#{d} #{store_hour[breakpoint..-1]}"}
        next
      end
      store_hour = store_hour.sub("thru", "-").sub("midnight", "11:59pm").split(";")[0].strip
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
              .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
            .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
            .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '2bf27a63d181cd616b2edbbf99f725793322ac78caaae64f3cd7041d5a48355e'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids, leaflet_ids = get_stores("https://api.freshop.com/1/sessions?token=082cf5639773ea61955d6bfd55830834&app_key=foodtown")
    puts "GOING TO COLLECT THE LEAFLETS"
    puts "JUST ENDED THE SITE DATA"
  end
end