#d985ca98464cfd13137dcae24ea6853a35e45cdb24ca190f919bfe2130f3df9e FOR PIONEER SUPERMARKET
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'


class PioneerSupermarket
  
  def get_stores(stores_list_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE"
    stores_script_text = stores_list_page.search("//script[contains(text(), 'var stores = ')]")[0].text
    stores_text = stores_script_text.match(/var stores =.+\;/).to_s
    stores_json = stores_text.sub("var stores =","").chomp(";").strip
    stores_list = JSON.parse(stores_json)
    stores_list.each do |store_item|
      store_name = store_item["retailerName"]
      store_address = store_item["address1"]
      store_city = store_item["city"]
      store_zipcode = store_item["zipCode"]
      store_phone = store_item["phone"]
      store_latitude = store_item["latitude"]
      store_longitude = store_item["longitude"]
      store_hours = store_item["hourInfo"]
      store_identifier = store_item["storeIdentifier"]
      puts "ON #{store_address} NOW"
      opening_hours = parse_hours(store_hours)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_list_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_list_url}"
      puts "******"
      allStoresIds << store.id
      store_leaflets = get_leaflets(store.id, store_identifier)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end 
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, circular_identifier)
    leaflets = []
    store_leaflet_url = "http://www.pioneersupermarkets.com/weekly-ad/?circularstoreidentifier=#{circular_identifier}"
    sleep(1)
    mechanize = Mechanize.new
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
    images_script_text = store_leaflet_page.search("//script[contains(text(), 'var circularPageDiv')]")[0]
                                                                                                    .text
    script_urls = URI.extract(images_script_text)                                                                                                
    images_urls = []
    script_urls.map{|ist| images_urls << ist if ist.start_with?("http") && ist.end_with?(".jpg")}
    store_leaflet_name = store_leaflet_page.search("//meta[@property='og:title']/@content")[0].value
    leaflet_dates = store_leaflet_page.search("//div[@class='stc-main-content']//h2/div[1]")[0].text
                                                                .gsub(store_leaflet_name, "").strip
    leaflet_start_date = leaflet_dates.split("-")[0].strip.to_s
    leaflet_end_date = leaflet_dates.split("-")[1].strip.to_s
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = store_leaflet_url
      leaflet.image_urls = images_urls
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.image_urls
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    puts "**********"
    leaflets << leaflet
    leaflets
  end  
  
  def parse_hours(store_hours_text)
    week_hours = []
    store_hours = store_hours_text.lines
    puts "GOT #{store_hours} TO PARSE NOW"
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase.strip
      if store_hour.include?("and")
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        open_hours = store_hour[breakpoint..-1]
        store_hours << store_hour.split("and")[1].strip
        store_hours << "#{store_hour.split("and")[0].strip} #{open_hours}"
        next
      end
      if store_hour.include?("&")
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        open_hours = store_hour[breakpoint..-1]
        store_hours << store_hour.split("&")[1].strip
        store_hours << "#{store_hour.split("&")[0].strip} #{open_hours}"
        next
      end
      if store_hour.include?("sun-thurs")
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        open_hours = store_hour[breakpoint..-1]
        store_hours << "sun #{open_hours}"
        store_hours << "mon #{open_hours}"
        store_hours << "tue #{open_hours}"
        store_hours << "wed #{open_hours}"
        store_hours << "thu #{open_hours}"
        next
      end
      puts "GOING TO PARSE #{store_hour} NOW"
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = store_hour[0..breakpoint-1].split("to") if store_hour[0..breakpoint-1].include?("to")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split("—") if open_hours.include?("—")
      hours = open_hours.split unless open_hours.include?("-") || open_hours.include?("—") || open_hours
                                .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours[0] = "#{hours[0].strip} #{get_am_pm(hours[0].strip)}" if !(hours[0].include?("am") || hours[0]
                                                            .include?("pm")) && !(hours[0]
                                                            .include?("closed")  || !hours[0].match(/\d+/))
      hours[1] = "#{hours[1].strip} #{get_am_pm(hours[1].strip)}" if hours[1] && !(hours[1]
                                .include?("am") || hours[1]
                                .include?("pm")) && !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end

  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'd985ca98464cfd13137dcae24ea6853a35e45cdb24ca190f919bfe2130f3df9e'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores("http://www.pioneersupermarkets.com/our-stores/find-your-store/")
    puts "JUST ENDED THE SITE DATA"
  end
end