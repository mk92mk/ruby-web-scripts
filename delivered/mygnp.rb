#427ba15c3ae42da12762f1f40539fbb9a0c6d8caf3864c7e24528140f8580f04 for MYGNP
require 'pqsdk'
require 'mechanize'
require 'uri'



class MyGnp
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.mygnp.com"
    mechanize = Mechanize.new
    states_page = mechanize.get(states_url)
    puts "GOT STATES PAGE"
    states_hrefs = states_page.search("//map[@id='usa_image_map']//area/@href")
    states_hrefs.each do |state_href|
      mechanize = Mechanize.new
      sleep(1)
      state_stores_url = "#{base_url}#{state_href.value}"
      state_stores_page = mechanize.get(state_stores_url)
      puts "GOT STATE STORES PAGE AT #{state_stores_url}"
      stores_hrefs = state_stores_page.search("//a[@id='aa']/@href")
      stores_hrefs.each do |store_href|
        mechanize = Mechanize.new
        sleep(2)
        store_details_url = "#{base_url}#{store_href.value}"
        store_details_page = mechanize.get(store_details_url)
        puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
        store_name = store_details_page.search("//span[@id='storeName']")[0].text.strip
        store_address = store_details_page.search("//p[@class='txt_16']//text()")[0].text.strip
        store_phone = store_details_page.search("//p[@class='txt_16']//text()")[-1].text.strip
        store_city = store_details_page.search("//p[@class='txt_16']//text()")[-2].text.strip.split(",")[0]
        store_zipcode = store_details_page.search("//p[@class='txt_16']//text()")[-2].text.strip
                                                                                .split(",")[-1].strip
        coord_href = store_details_page.search("//span[@class='link_learn_more mar_top45']
                                                    //a/@href")[0].value
        directions_url = "#{base_url}#{coord_href}"
        sleep(1)
        mechanize = Mechanize.new
        coord_page = mechanize.get(directions_url)
        puts "GOT COORDINATES PAGE AT #{directions_url}"
        store_lat_node = coord_page.search("//div[@id='lat']")[0]
        store_latitude = store_lat_node.text.strip if store_lat_node
        store_long_node = coord_page.search("//div[@id='long']")[0]
        store_longitude = store_long_node.text.strip if store_long_node
        opening_hours = []
        hours_text_node = store_details_page.search("//div[@class='hrs_tr']")[0]
        if hours_text_node
          hours_text = hours_text_node.text.strip
          opening_hours = parse_hours(hours_text)
        end
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_details_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_details_url}"
        puts "******"
        allStoresIds << store.id
        store_leaflets = get_leaflets(store.id, store_zipcode, store_name)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode, store_name)
    leaflets = []
    leaflets_list_url = "http://www.circularhub.com/flyers/goodneighborpharmacy?type=2&locale=en&force_store_selection=true&postal_code=#{store_zipcode}&filler=&is_postal_entry=true#!/flyers/goodneighborpharmacy-weeklycircular?flyer_run_id=127937"
    store_leaflet_url = "http://www.circularhub.com/flyers/goodneighborpharmacy?type=2&locale=en&postal_code=#{store_zipcode}&store_code=%s&is_store_selection=true&auto_flyer=&sort_by=#!/flyers/goodneighborpharmacy-weeklycircular?flyer_run_id=127937"
    mechanize = Mechanize.new
    sleep(2)
    leaflets_list_page = mechanize.get(leaflets_list_url)
    puts "GOT LEAFLETS LIST PAGE FOR #{leaflets_list_url}"
    leaflet_form = leaflets_list_page.search("//form[@class='store_select_form']
                    [contains(.//span//text(), '#{store_name.split[0].split("'")[0]}')]
                    [contains(.//input[@id='postal_code']/@value, '#{store_zipcode.split("-")[0]}')]")[0]
    leaflet_form = leaflets_list_page.search("//form[@class='store_select_form']
              [contains(.//span//text(), '#{store_name.split[0].split("'")[0].upcase}')]
              [contains(.//input[@id='postal_code']/@value, '#{store_zipcode.split("-")[0]}')]")[0] unless leaflet_form
    unless leaflet_form
      puts "NOT LEAFLET FOUND FOR #{store_name}"
      return leaflets
    end
    store_code = leaflet_form.search(".//input[@id='store_code']/@value")[0].value
    store_leaflet_url = store_leaflet_url %[store_code]
    mechanize = Mechanize.new
    sleep(1)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT LANDING PAGE AT #{store_leaflet_url}"
    leaflet_start_date = store_leaflet_page.search("//title")[0].text.strip.split("-")[1]
                                                             .split("to")[0].strip
    leaflet_end_date = store_leaflet_page.search("//title")[0].text.strip.split("-")[1]
                                                             .split("to")[1].strip
    leaflet_start_date = Time.parse(leaflet_start_date)
    leaflet_end_date = Time.parse(leaflet_end_date)
    script_text = store_leaflet_page.search("//script[contains(text(), 'pdf_url')]")[0].text
    script_urls = URI.extract(script_text)
    pdf_flags = script_urls.map{|u| u.start_with?("http") && u.end_with?(".pdf")}
    pdf_url = script_urls[pdf_flags.index(true)]
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = pdf_url
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date 
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(hours_text)
    puts "GOT #{hours_text} TO PARSE"
    week_hours = []
    store_hours = []
    store_hours = hours_text.downcase.split(";")
    store_hours.each do |store_hour|
      puts "STORE HOUR #{store_hour} TO PROCESS NOW"
      unless (store_hour.include?("mon") || store_hour.include?("tue") || 
            store_hour.include?("wed") || store_hour.include?("thu") ||
            store_hour.include?("fri") || store_hour.include?("sat") ||
            store_hour.include?("sun"))
        store_hour = "mon-sun #{store_hour}"
      end
      store_hour = store_hour.gsub("24 hours", "12am-11:59pm") if store_hour.include?("24 hours")
      store_hour = store_hour.gsub("daily", "") if store_hour.include?("daily")
      store_hour = store_hour.gsub("midnight", "11:59pm") if store_hour.include?("midnight")
      store_hour = store_hour.gsub("/7 days a week", "")
      store_hour = store_hour.gsub("-on call", "")
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}                                                                                
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end

  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_am_pm(timing)
    time_split = timing.split(":")
    hour = time_split[0].to_i
    if hour >=0 && hour <= 11
      day_part = "am"
    elsif hour >= 12 && hour <= 23
      day_part = "pm"
    end
    day_part
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '427ba15c3ae42da12762f1f40539fbb9a0c6d8caf3864c7e24528140f8580f04'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.mygnp.com/state-map")
    puts "JUST ENDED THE SITE DATA"
  end
end