#e05ddd26c742121bf2da9087f99f1e2c4cd0a1d02496101a73087cc4ac43cc08 FOR RED DEL HOGAR
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class RedDelHogar
  def get_stores
    allStoresIds = []
    form_page_url = "http://200.59.14.198/rh2015/SucursalesTool.asp"
    form_post_url = "http://200.59.14.198/rh2015/SucursalesList.asp"
    mechanize = Mechanize.new
    mechanize.retry_change_requests = true
    mechanize.idle_timeout = 4 
    form_page = mechanize.get(form_page_url)
    puts "GOT MAIN PAGE AND GOING FOR PROVINCES NOW"
    province_form_values = form_page.search("//form[@name='frmBuscar']//select[@name='mProvincias']
                                    //option/@value")
    province_form_values.each do |form_value|
      sleep(1)
      mechanize = Mechanize.new
      mechanize.retry_change_requests = true
      mechanize.idle_timeout = 4
      province_stores_page = mechanize.post(form_post_url,
                                           {"mProvincias" => form_value.value, "mZonas" => "-",
                                            "mLocalidades" => "-"})
      puts "GOT PROVINCE STORES PAGE FOR #{form_value.value}"                                   
      store_ids = fetch_stores(province_stores_page)
      store_ids.each do |store_id|
        allStoresIds << store_id
      end
      puts "GOING TO NEXT PROVINCE"
    end
    puts "GOING FOR ZONES NOW"
    zone_form_values = form_page.search("//form[@name='frmBuscar']//select[@name='mZonas']
                                    //option/@value")
    zone_form_values.each do |form_value|
      sleep(1)
      mechanize = Mechanize.new
      mechanize.retry_change_requests = true
      mechanize.idle_timeout = 4
      province_stores_page = mechanize.post(form_post_url,
                                           {"mProvincias" => "-", "mZonas" => form_value.value,
                                            "mLocalidades" => "-"})
      puts "GOT ZONES STORES PAGE FOR #{form_value.value}"                                   
      store_ids = fetch_stores(province_stores_page)
      store_ids.each do |store_id|
        allStoresIds << store_id
      end
      puts "GOING TO NEXT ZONE"
    end
    puts "GOING FOR LOCALITIES NOW"
    locality_form_values = form_page.search("//form[@name='frmBuscar']//select[@name='mLocalidades']
                                    //option/@value")
    locality_form_values.each do |form_value|
      sleep(1)
      mechanize = Mechanize.new
      mechanize.retry_change_requests = true
      mechanize.idle_timeout = 4
      province_stores_page = mechanize.post(form_post_url,
                                           {"mProvincias" => "-", "mZonas" => "-",
                                            "mLocalidades" => form_value.value})
      puts "GOT LOCALITIES STORES PAGE FOR #{form_value.value}"                                   
      store_ids = fetch_stores(province_stores_page)
      store_ids.each do |store_id|
        allStoresIds << store_id
      end
      puts "GOING TO NEXT LOCALITY"
    end
    allStoresIds
  end
  
  def fetch_stores(province_stores_page)
    store_ids = []
    stores_sections = province_stores_page.search("//table[@bgcolor='#FCF9EC']
                                                    //tr[contains(.//td/@bgcolor,'#EAEAEA')]")
    stores_sections.each do |store_section|
      store_name = store_section.search(".//td[1]")[0].text.strip
      store_address = store_section.search(".//td[2]")[0].text.strip
      store_city = store_section.search(".//td[3]")[0].text.strip
      store_zipcode = "00000"
      store_latitude = store_section.search(".//iframe/@src")[0].value.split("!2d")[1]
                                    .split("!3d")[0]
      store_longitude = store_section.search(".//iframe/@src")[0].value.split("!3d")[1]
                                     .split("!2m3!")[0]
      if store_longitude.length > 50
        store_longitude = store_section.search(".//iframe/@src")[0].value.split("!3d")[1]
                                       .split("!3m2!")[0]
      end
      store_hours = []
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.origin = "http://200.59.14.198/rh2015/SucursalesTool.asp"
      end
      store.opening_hours = store_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE IS #{store_longitude}"
      puts  "STORE LATITUDE IS #{store_latitude}"
      puts  "STORE HOURS #{store_hours}"
      puts  "STORE URL http://200.59.14.198/rh2015/SucursalesTool.asp"
      puts "******"
      store_ids << store.id
    end
    store_ids
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    leaflet_url = "http://www.reddelhogar.com.ar/index.php/catalogos"
    mechanize = Mechanize.new
    mechanize.retry_change_requests = true
    mechanize.idle_timeout = 4
    leaflets_page = mechanize.get(leaflet_url)
    leaflets_sections = leaflets_page.search("//table[contains(.//p/@data-redactor-inserted-image,'true')]//a[contains(.//@href,'.ar/index.php/download_file')]")
    leaflets_sections.each do |leaflet_section|
      store_leaflet_name = "Catalogo #{leaflet_section.search(".//source[1]/@srcset")[0].value
                            .split("/")[-1].sub(".jpg", "")}"
      leaflet_pdf_url = leaflet_section.search(".//@href")[0].value
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = leaflet_pdf_url
        leaflet.store_ids = store_ids
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      puts "**********"
      allStoresLeafletsIds << leaflet.id
    end
    allStoresLeafletsIds
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'e05ddd26c742121bf2da9087f99f1e2c4cd0a1d02496101a73087cc4ac43cc08'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end   
  
  
end
  