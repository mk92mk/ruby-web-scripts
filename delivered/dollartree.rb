#f7f898499fd536aa63c439971974e85b8d01d787f14fea79b7da79867591c3a7 for DOLLAR TREE
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'byebug'

class DollarTree
  
  
  def get_stores(states_url)
    allStoresIds = []
    mechanize = Mechanize.new
    states_page = mechanize.get(states_url)
    puts "GOT STATES PAGE"
    states_urls = states_page.search("//a[contains(@linktrack, 'State index page')]/@href")
    states_urls.each do |state_url|
      mechanize = Mechanize.new
      sleep(1)
      state_cities_page = mechanize.get(state_url.text)
      puts "GOT STATE CITIES PAGE #{state_url.text}"
      cities_urls = state_cities_page.search("//a[contains(@linktrack, 'City index page')]/@href")
      cities_urls.each do |city_url|
        mechanize = Mechanize.new
        sleep(1)
        stores_page = mechanize.get(city_url.text)
        puts "GOT STORES PAGE #{city_url.text}"
        stores_hrefs = stores_page.search("//a[contains(@linktrack, 'Location page')]/@href")
        stores_hrefs.each do |store_href|
          mechanize = Mechanize.new
          sleep(2)
          store_details_page = mechanize.get(store_href.value)
          puts "GOT STORE DETAILS PAGE AT #{store_href.value}"
          store_name = store_details_page.search("//div[@itemprop='address']//span[1]")[0].text.strip
          store_address = store_details_page.search("//span[@itemprop='streetAddress']")[0].text.strip
          store_city = store_details_page.search("//span[@itemprop='addressLocality']")[0].text.strip
          store_zipcode = store_details_page.search("//span[@itemprop='postalCode']")[0].text.strip
          store_phone = store_details_page.search("//div[@itemprop='telephone']")[0].text.strip
          store_latitude = store_details_page.search("//meta[@property='place:location:latitude']
                                                      /@content")[0].value
          store_longitude = store_details_page.search("//meta[@property='place:location:longitude']
                                                      /@content")[0].value
          opening_times = store_details_page.search("//time[@itemprop='openingHours']/@datetime")
          opening_hours = parse_hours(opening_times)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude
            store.longitude = store_longitude
            store.phone = store_phone
            store.origin = store_href.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}"
          puts  "STORE LATITUDE #{store_latitude}"
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{store_href.value}"
          puts "******"  
          allStoresIds << store.id
        end
        puts "GOING TO NEXT CITY"
      end
      puts "GOING TO NEXT STATE"
    end
    allStoresIds
  end

  
  def parse_hours(opening_times)
    week_hours = []
    opening_times.each do |opening_time|
      puts "GOING TO PARSE #{opening_time.value} NOW"
      hour_text = opening_time.value.downcase.gsub("closed-closed", "closed")
      first_digit = /\d+/.match(hour_text).to_s
      breakpoint = hour_text.index(first_digit)
      days = hour_text[0..breakpoint-1].split("-")
      days = hour_text[0..breakpoint-1].split("–") if hour_text[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = hour_text[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
                                  .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")   
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.start_with?("mo")
      index = 0
    elsif day.start_with?("tu")
      index = 1
    elsif day.start_with?("we")
      index = 2
    elsif day.start_with?("th")
      index = 3  
    elsif day.start_with?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_leaflets(store_ids, leaflets_list_url)
    leaflet_ids = []
    base_url = "http://ads.dollartree.com"
    leaflets_base_url = "#{base_url}/html5%s%s/html/"
    mechanize = Mechanize.new
    sleep(1)
    leaflets_page = mechanize.get(leaflets_list_url)
    puts "GOT LEAFLETS LIST PAGE"
    leaflets_sections = leaflets_page.search("book")
    leaflets_sections.each do |leaflet_sect|
      store_leaflet_name = leaflet_sect.search(".//@Title")[0].value
      store_leaflet_dates = leaflet_sect.search(".//description")[0].text
      leaflet_start_date = Time.parse(store_leaflet_dates.gsub("Valid", "")
                            .split("-")[0]) unless store_leaflet_dates.empty?
      leaflet_end_date = Time.parse(store_leaflet_dates.gsub("Valid", "")
                            .split("-")[1]) unless store_leaflet_dates.empty?
      path = leaflet_sect.search(".//path")[0].text
      path.slice!('.')
      section_id = leaflet_sect.search(".//section//@SectionGroupTypeID")[0].value
      leaflet_path = "#{path}#{section_id}"
      images_urls = []
      leaflet_url = leaflets_base_url %[path, section_id]
      mechanize = Mechanize.new
      sleep(1)
      leaflet_page = mechanize.get(leaflet_url)
      puts "GOT LEAFLET PAGE AT #{leaflet_url}"
      leaflet_pdf_href = leaflet_page.search("//a[contains(@href, 'export.pdf')]/@href")[0].value
      leaflet_pdf_url = "#{base_url}#{leaflet_pdf_href}"
      puts "DOWNLOAD FROM #{leaflet_pdf_url}"
      leaflet = PQSDK::Leaflet.find leaflet_pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = leaflet_pdf_url
        leaflet.start_date = leaflet_start_date if leaflet_start_date
        leaflet.end_date = leaflet_end_date if leaflet_end_date
        leaflet.store_ids = store_ids
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      puts leaflet.start_date
      puts leaflet.end_date 
      puts "*******"
      leaflet_ids << leaflet.id
    end
    leaflet_ids
  end
    
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'f7f898499fd536aa63c439971974e85b8d01d787f14fea79b7da79867591c3a7'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    begin
      store_ids = get_stores("http://locations.dollartree.com/")
      puts "GOING TO COLLECT THE LEAFLETS"
      leaflet_ids = get_leaflets(store_ids, "http://ads.dollartree.com/html5/ServiceGetBookList.php")
    rescue => e
      puts e.message
      puts e.backtrace.join("\n")
    end
    puts "JUST ENDED THE SITE DATA"
  end
  
end