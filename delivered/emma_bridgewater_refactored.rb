require 'pqsdk'

class Crawler::Uk::EmmaBridgewater < Crawler::Base

  def get_stores
    allStores = []
    base_stores_url1 = "http://www.emmabridgewater.co.uk/scat/"
    base_stores_url2 = "&temp=storesjson&layout=blank"
    start_url = "http://www.emmabridgewater.co.uk/en/us/page/storelocator"
    mechanize = Mechanize.new
    start_page = mechanize.get(start_url)
    uk_regions = start_page.search("//div[@class='hide reflistukregions reflist']//li")
    uk_regions.each do |region|
      stores_url = "#{base_stores_url1}#{region.text}#{base_stores_url2}"
      stores_page = open(stores_url).read
      @report.info << "GOT THE PAGE FOR #{region}"
      stores_json = "[#{stores_page}]"
      stores_parsed = JSON.parse(stores_json)
      stores_parsed.each do |store|
        store_name = store["StoreName"]
        store_address = ""
        store_address << store["AddressName"] unless store["AddressName"].empty?
        store_address << ", #{store["Address"]}" unless store["Address"].empty?
        store_address << ", #{store["Address2"]}" unless store["Address2"].empty?
        store_zipcode = store["PostCode"].blank? ? '00000' : store['PostCode']
        store_phone = store["Phone"]
        store_latitude = store['Latitude'].blank? ? 0 : store['Latitude']
        store_longitude = store['Longitude'].blank? ? 0 : store['Longitude']     
        opening_hours = []
        opening_hours << parse_hours(store["Mon"], 0) unless store["Mon"].empty?
        opening_hours << parse_hours(store["Tue"], 1) unless store["Tue"].empty? 
        opening_hours << parse_hours(store["Wed"], 2) unless store["Wed"].empty?
        opening_hours << parse_hours(store["Thu"], 3) unless store["Thu"].empty?
        opening_hours << parse_hours(store["Fri"], 4) unless store["Fri"].empty?
        opening_hours << parse_hours(store["Sat"], 5) unless store["Sat"].empty?
        opening_hours << parse_hours(store["Sun"], 6) unless store["Sun"].empty?

        store_city = nil
        city1 = PQSDK::City.find(store['City'])
        city2 = PQSDK::City.find(store['Address'])
        if !city1.blank? and city1.state=='confirmed'
          store_city = store['City']
        elsif !city2.blank? and city2.state == 'confirmed'
          store_city = store['Address']
        else
          unless store['Address'].blank?
            store_city = store['Address']
          else
            store_city = store['City']
          end
        end
            
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        store.save
        allStores << store.id
        @report.info << "*************STORE*********************"
        @report.info << "STORE NAME #{store_name}"
        @report.info << "STORE ADDRESS: #{store_address}"
        @report.info << "STORE CITY #{store_city}"
        @report.info << "STORE ZIPCODE #{store_zipcode}"
        @report.info << "STORE LONGITUDE #{store_longitude}"
        @report.info << "STORE LATITUDE #{store_latitude}"
        @report.info << "STORE PHONE #{store_phone}"
        @report.info << "STORE HOURS #{opening_hours}"
        @report.info << "STORE URL #{stores_url}"
      end
    end
    allStores
  end
  
  def parse_hours(opening_hours, day_index)
    hours = {}
    hours["weekday"] = day_index
    if opening_hours.empty?
      return
    elsif opening_hours.downcase.include?("closed")
      hours["closed"] = true
    else
      hours_split = opening_hours.split(" - ")
      opening = hours_split[0]
      closing = hours_split[1]
      opening["."] = ":"
      closing["."] = ":"
      open_hour = opening.split(".")[0]
      open_hour = open_hour.to_i
      am_pm = get_am_pm(opening)
      hours["open_am"] = opening if am_pm.eql?("am")
      hours["open_pm"] = opening if am_pm.eql?("pm")
      am_pm = get_am_pm(closing)
      hours["close_am"] = closing if am_pm.eql?("am")      
      hours["close_pm"] = closing if am_pm.eql?("pm") 
    end
    hours
  end
  
  def get_leaflets(store_ids, downloads_url)
#    mechanize = Mechanize.new
    mechanize = reset_mechanize
    mechanize.user_agent_alias = 'Windows Chrome'
    pdf_page = mechanize.get(downloads_url)
    pdf_url = pdf_page.search('//h3[contains(text(), "brochure EMMA BRIDGEWATER")]/
                                      following-sibling::p/a')[0]["href"]
    pdf_name = "EmmaBridgeWater_leaflet"
    pdf_start = Time.now.to_s
    pdf_end = Time.now.to_s
    @report.info << "Download from --> #{pdf_url}"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = pdf_name
      leaflet.start_date = pdf_start
      leaflet.end_date = pdf_end
      leaflet.url = pdf_url
      leaflet.store_ids = store_ids
      leaflet.save
    end
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
        am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
        am_pm = "pm"
    end
    am_pm
  end

  def reset_mechanize
    mechanize ||= Mechanize.new do |m|
     m=m.set_proxy("107.151.152.210", 80)
    end
    mechanize
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.ilikesales.co.uk'
    PQSDK::Settings.app_secret = '9593afe35e5ff31087094347681575f8af80e2891ac6ad27fc3ef28824909948'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.pine-apple.com/pagina/52/Downloads.html")
    @report.info << "JUST ENDED THE SITE DATA"
  end

end