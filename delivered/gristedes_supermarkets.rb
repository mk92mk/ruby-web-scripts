#a762f90e98d0698c1faa0ea597f15c17c246949d75fb0c4daa3574c2545da400 for GristedesSuperMarkets
require 'pqsdk'
require 'mechanize'
require 'json'


class GristedesSupermarkets
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def get_stores
    allStoresIds = []
    base_url = "http://www.gristedessupermarkets.com/test/wp-admin/admin-ajax.php?action=store_search&lat=40.7127837&lng=-74.00594130000002&max_results=1000&radius=40"
    mechanize = Mechanize.new
    stores_page = mechanize.get(base_url)
    store_jsons = stores_page.body
    stores_parsed = JSON.parse(store_jsons)
    stores_parsed.each do |store_item|
      store_name = store_item["store"]
      store_address = store_item["address"]
      store_address << " #{store_item['address2']}" unless store_item['address2'].empty?
      store_city = store_item["city"]
      store_phone = store_item["phone"]
      store_zipcode = store_item["zip"]
      store_latitude = store_item["lat"]
      store_longitude = store_item["lng"]
      working_hours = store_item["hours"]
      stores_url = base_url
      opening_hours = parse_hours(working_hours)
      store_zipcode = "00000" if store_zipcode.nil? || store_zipcode.empty?
      store_latitude = "0" if store_latitude.nil? || store_latitude.empty?
      store_longitude = "0" if store_longitude.nil? || store_longitude.empty?
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = stores_url
      end
      store.opening_hours = opening_hours
      #      store.save
      allStoresIds << store#.id
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{stores_url}"
    end
    allStoresIds
  end
  
  def parse_hours(hours_text)
    pharmacy_index = hours_text.downcase.index("pharmacy hours")
    hours_text = hours_text[0..pharmacy_index-1] if pharmacy_index
    hours_text = hours_text.gsub(",", "\n")
    hours_lines = hours_text.lines.map{|hr| hr.strip.gsub("<p>","").gsub("</p>","")
                                            .gsub("<br />", "").gsub("12MID","11:59pm")
                                            .gsub("11MID","11pm")
                                            .gsub("24 HOURS","Monday-Sunday 12am-11:59pm")
                                            .squeeze(" ")}
    store_hours = []
    week_hours = []
    hours_lines.each do |hour_line|
      next if hour_line.empty? || hour_line.downcase.include?("call store for holiday hours")
      if (hour_line.downcase.include?("am") || hour_line.downcase.include?("pm")) &&  # for 7am-9pm
            !(hour_line.downcase.include?("mon") || hour_line.downcase.include?("tue") || 
            hour_line.downcase.include?("wed") || hour_line.downcase.include?("thu") ||
            hour_line.downcase.include?("fri") || hour_line.downcase.include?("sat") ||
            hour_line.downcase.include?("sun"))
        store_hours << "mon-sun #{hour_line}"
      else
        store_hours << hour_line
      end
    end
    
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase   
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")       
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour.eql?("12")
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_leaflets(store_ids, leaflets_url)
    allLeaflets = []
    pdf_url = ""
    mechanize = Mechanize.new 
    begin
      leaflets_page = mechanize.get(leaflets_url)
    rescue Exception => exception
      if exception.response_code == '403' 
        leaflets_page = exception.page
        puts "EXCEPTION CAUGHT BUT WILL SEARCH FOR LEAFLETS"
        pdf_url = get_pdf_url(leaflets_page)
      end
    else
      puts "EXCEPTION NOT COMING SO WILL SEARCH FOR LEAFLETS"
      pdf_url = get_pdf_url(leaflets_page)
    end
    uploads_index = pdf_url.index("/uploads/")
    leaflet_name = "GristedesSuperMarkets_#{pdf_url.split("/uploads/")[-1].gsub('.pdf', '_leaflet')}"
    puts "Download from #{pdf_url}"
    leaflet = PQSDK::Leaflet.find pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = leaflet_name
      leaflet.url = pdf_url
      leaflet.start_date = Time.now.to_s
      leaflet.end_date = Time.now.to_s        
      leaflet.store_ids = store_ids
#      leaflet.save     
      puts "********LEAFLET******"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.store_ids
      puts "********LEAFLET END********"
      allLeaflets << leaflet
    end
  end  
  
  def get_pdf_url(page)
    pdf_url = page.search("//em[contains(text(),'to download our weekly specials')]//preceding-sibling::a/@href")[0].value
    pdf_url
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'a762f90e98d0698c1faa0ea597f15c17c246949d75fb0c4daa3574c2545da400'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.gristedessupermarkets.com/weekly-specials/")
    puts "JUST ENDED THE SITE DATA"
  end  
end