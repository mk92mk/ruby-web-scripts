#955437db233fadf7c2615f1430cc90bbf7ea8f7dccf9f446698a8c5f4c33de5f FOR MICR CENTER
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class MicroCenter
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.microcenter.com"
    stores_list_url = "http://www.microcenter.com/site/stores/default.aspx"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE"
    stores_hrefs = stores_list_page.search("//div[@class='STOREbrandWRAP']//a/@href")
    stores_hrefs.each do |store_href|
      store_details_url = "#{base_url}#{store_href.value}"
      sleep(1)
      mechanize = Mechanize.new
      store_details_page = mechanize.get(store_details_url)
      puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
      store_name = store_details_page.search("//div[@class='store-address-detail']//h3")[0].text.strip
      store_address = store_details_page.search("//div[@class='store-address-detail']
                                                 //div")[0].text.strip
      store_city = store_details_page.search("//div[@class='store-address-detail']
                                                 //div")[2].text.split(",")[0].strip
      store_zipcode = store_details_page.search("//div[@class='store-address-detail']
                                                 //div")[2].text.split(",")[1].match(/\d+/).to_s
      store_phone = store_details_page.search("//div[@class='store-address-detail']
                                                 //div")[-1].text.strip 
      coord_text = store_details_page.search("//script[contains(text(), 
                                             'var latlng = new google.maps.LatLng')]")[0].text
                                             .match(/var latlng = new google.maps.LatLng\(.+\);/).to_s
      store_latitude = coord_text.split(",")[0].match(/\d+.\d+|-\d+.\d+/).to_s
      store_longitude = coord_text.split(",")[1].match(/\d+.\d+|-\d+.\d+/).to_s
      store_hours_rows = store_details_page.search("//div[@class='store-address-hours']
                                                    //div[contains(@class,'hours')]")
      opening_hours = parse_hours(store_hours_rows)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude if store_latitude
        store.longitude = store_longitude if store_longitude
        store.phone = store_phone
        store.origin = store_details_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE IS #{store_longitude}" if store_longitude
      puts  "STORE LATITUDE IS #{store_latitude}" if store_latitude
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_details_url}"
      puts "******"
      allStoresIds << store.id
      store_micro_id = store_details_page.search("//div[@class='selectThisStore']//a/@href")[0]
                                                                    .value.split("storeId=")[-1]
      store_leaflets = get_leaflets(store.id, store_micro_id)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_micro_id)
    leaflets = []
    base_leaflet_url = "http://www.microcenter.com/site/specials/catalog/catalog.aspx"
    sleep(1)
    mechanize = Mechanize.new
    base_leaflet_page = mechanize.post(base_leaflet_url, 
                                        {"storeID" => store_micro_id})
    puts "GOT BASE LEAFLET PAGE"
    store_leaflet_name = base_leaflet_page.search("//li[@id='left2']//h1")[0].text
    leaflet_pub_url = base_leaflet_page.search("//li[@id='left2']//a/@href")[0].value
    sleep(1)
    mechanize = Mechanize.new
    leaflet_pub_page = mechanize.get(leaflet_pub_url)
    puts "GOT LEAFLET PUBLICATION PAGE AT #{leaflet_pub_url}"
    store_leaflet_dates = leaflet_pub_page.search("//meta[@name='title']/@content")[0].value
                                          .split("Prices Good")[-1].split("-")
    leaflet_start_date = Time.parse(store_leaflet_dates[0].strip)
    leaflet_end_date = Time.parse(store_leaflet_dates[1].strip)
    leaflet_pub_id = leaflet_pub_url.split("/")[-1]
    leaflet_pdf_url = "http://viewer.zmags.com/services/DownloadPDF?publicationID=#{leaflet_pub_id}&selectedPages=all"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    puts "**********"
    leaflets << leaflet
    leaflets
  end
  
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      store_hour = store_hour_node.text.downcase
      puts "GOT #{store_hour} TO PARSE NOW"
#      @logger.info "GOT #{store_hour} TO PARSE NOW"
      if store_hour.include?("closed")
        breakpoint = store_hour.index("closed")
        days = store_hour[0..breakpoint-1].split("-")
      else
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        days = store_hour[0..breakpoint-1].split("-")
        days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      end
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split("to") if open_hours.include?("to")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '955437db233fadf7c2615f1430cc90bbf7ea8f7dccf9f446698a8c5f4c33de5f'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
#    @logger.info "IN RUN METHOD"
#    @logger.info "GOING TO COLLECT THE STORES AND THE LEAFLETS"    
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
#    @logger.info "JUST ENDED THE SITE DATA"
    
  end
end
