# 9ae2cd95a17d834f45993c44a8603e660446340416e115f1e6f598a303d760fd for TOPSMARKETS
require 'pqsdk'
require 'mechanize'
require 'uri'


class TopsMarkets
  
  def initialize
    @report = Logger.new("TopsMarkets_#{Time.now.to_s}.log")
  end
  
  def get_stores(start_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.topsmarkets.com"
    mechanize = Mechanize.new
    start_page = mechanize.get(start_url)
    puts "GOT START PAGE #{start_url}"
    @report.info "GOT START PAGE #{start_url}"
    states_list = start_page.search("//div[@class='container -fluid']//li//a/@href")
    states_list.each do |state_item|
      state_stores_url = "#{base_url}#{state_item.value.gsub("..", "")}"
      mechanize = Mechanize.new
      sleep(1)
      state_stores_page = mechanize.get(state_stores_url)
      puts "GOT STORES LIST PAGE OF STATE #{state_stores_url}"
      @report.info "GOT STORES LIST PAGE OF STATE #{state_stores_url}"
      stores_hrefs = state_stores_page.search("//div[@id='StoreLocator']//td[1]/a/@href")
      stores_hrefs.each do |store_href|
        store_details_url = store_href.value
        mechanize = Mechanize.new
        sleep(2)
        store_details_page = mechanize.get(store_details_url)
        puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
        @report.info "GOT STORE DETAILS PAGE AT #{store_details_url}"
        store_info_div = store_details_page.search("//div[@id='Content']")
        store_name_node = store_info_div.search(".//h3")[0]
        store_name = store_name_node.text if store_name_node
        address_parts = store_details_page.search("//p[@class='Address']/text()").text.strip.lines
                                                             .map{|el| el.strip}
        address_parts.delete("")
        store_address = address_parts[0].strip
        store_city = address_parts[1].strip.split(",")[0]
        store_name = store_city unless store_name
        store_zipcode = address_parts[1].strip.split(",")[1].scan(/\d+/).first
        store_phone = store_details_page.search("//p[@class='PhoneNumber']/text()")[0].text.strip
        coord_script = store_details_page.search("//script[contains(text(), 'initializeMap')]")[0].text
        coords = coord_script.scan(/\d+.\d+|-\d+.\d+/)
        store_latitude = coords[0]
        store_longitude = coords[1]
        opening_hours = []
        hours_text_node = store_details_page.search("//table[@id='hours_info-BS']//dl//dt[contains(text(),
                                                 'Hours of Operation')]//following-sibling::dd[1]")[0]
        if hours_text_node
          hours_text = hours_text_node.text.strip
          opening_hours = parse_hours(hours_text) 
        end
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_details_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_details_url}"
        puts "******"
        @report.info  "*************STORE*********************"
        @report.info  "STORE NAME IS #{store_name}"
        @report.info  "STORE CITY IS #{store_city}"
        @report.info  "STORE ZIP CODE IS #{store_zipcode}"
        @report.info  "STORE ADDRESS IS #{store_address}"
        @report.info  "STORE LONGITUDE #{store_longitude}"
        @report.info  "STORE LATITUDE #{store_latitude}"
        @report.info  "STORE PHONE #{store_phone}"
        @report.info  "STORE HOURS #{opening_hours}"
        @report.info  "STORE URL #{store_details_url}"
        @report.info "******"
        sleep(3)
        allStoresIds << store.id
        store_leaflet_id = store_details_page.search("//div[@class='address_wrapper']
                           //strong[contains(text(), 'Store Number')]//following-sibling::text()")
                                                    .text.strip
        puts "GOT THE STORE LEAFLET ID #{store_leaflet_id}"
        @report.info "GOT THE STORE LEAFLET ID #{store_leaflet_id}"
        store_leaflets = get_leaflets(store_leaflet_id, store.id)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_leaflet_id, store_id)
    leaflets = []
    store_leaflet_url = "http://www.topsmarkets.com/WeeklyAd/Store/#{store_leaflet_id}/"
    mechanize = Mechanize.new
    sleep(4)
    leaflet_main_page = mechanize.get(store_leaflet_url)
    leaflet_info_div = leaflet_main_page.search("//div[contains(@class,'RightCol  First')]")
    leaflet_pages_list = leaflet_info_div.search(".//ul")[0]
    unless leaflet_pages_list
      puts "NO LEAFLET FOUND FOR #{store_leaflet_id}"
      @report.info "NO LEAFLET FOUND FOR #{store_leaflet_id}"
      return leaflets
    end
    leaflet_pages_hrefs = leaflet_pages_list.search(".//li//a/@href")
    store_leaflet_name = leaflet_info_div.search(".//span[@class='AdName']/text()")[0].text.strip
                                                                                      .gsub("!","")                         
    leaflet_dates = leaflet_info_div.search(".//span[@class='AdDatesCurrent']")
    leaflet_start_date = leaflet_dates[0].text.strip
    leaflet_end_date = leaflet_dates[1].text.strip
    images_urls = []
    leaflet_pages_hrefs.each do |page_href|
      image_page = mechanize.get(page_href.value)
      
      image_url = image_page.search("//div[@class='ADSC']//img//@src")[0].value
      images_urls << image_url
      puts "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      @report.info "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      sleep(2)
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.image_urls = images_urls
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    @report.info leaflet.start_date
    @report.info leaflet.end_date
    @report.info "*****"
    leaflets << leaflet
    leaflets
  end
  
  def parse_hours(hours_text)
    hours_text = hours_text.downcase
    puts "GOT #{hours_text} FOR PROCESSING"
    @report.info "GOT #{hours_text} FOR PROCESSING"
    store_hours = []
    unless (hours_text.include?("mon") || hours_text.include?("tue") || 
            hours_text.include?("wed") || hours_text.include?("thu") ||
            hours_text.include?("fri") || hours_text.include?("sat") ||
            hours_text.include?("sun"))
      store_hours << "mon-sun #{hours_text}"
    else 
      if hours_text.include?(";")
        hours_operation = hours_text.split(";")
        store_hours = hours_operation
      end  
    end
    week_hours = []
    store_hours.each do |store_hour|
      puts "STORE HOUR #{store_hour} TO PROCESS NOW"
      @report.info "STORE HOUR #{store_hour} TO PROCESS NOW"
      store_hour = store_hour.gsub("24 hours", "12am-11:59pm") if store_hour.include?("24 hours")
      store_hour = store_hour.gsub("daily", "") if store_hour.include?("daily")
      store_hour = store_hour.gsub("midnight", "11:59pm") if store_hour.include?("midnight")
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      hours[1] = "11:59pm" if hours[1].include?("midnight") || hours[1].include?("12mid")
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '9ae2cd95a17d834f45993c44a8603e660446340416e115f1e6f598a303d760fd'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.topsmarkets.com/StoreLocator/Store_S.las")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end