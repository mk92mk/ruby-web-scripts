# 658e0282c5b60579999d9bca533d08e029bebad016327473cc5206d132c103f7 app secret for silverdale
require 'pqsdk'
require 'byebug'
require 'geocoder'
require 'mechanize'

class Crawler::Uk::SilverDaleBathrooms < Crawler::Base
  
  def get_stores
    allStoresIds = []
    base_url = "http://www.silverdalebathrooms.co.uk/locations.xml"
    mechanize = Mechanize.new
    stores_url = base_url
    Geocoder::Configuration.timeout = 15
    Geocoder::Configuration.lookup = :google
    stores_page = mechanize.get(base_url)
    cities = PQSDK::City.all.map{|c| c.name.strip.downcase}
    stores_list = stores_page.search("//markers//marker")
    stores_list.each do |store_item|
      store_name = store_item["name"]
      store_latitude = store_item["lat"]
      store_longitude = store_item["lng"]
      store_phone = store_item["phone"]
      store_zipcode = store_item["postal"]
      store_address = store_item["address"]
      if store_latitude.empty?
        geocode = Geocoder.search(store_address)
        @report.info "USING THE GEOCODER NOW"
        store_latitude = get_coord_from_address(geocode, "lat")
        if store_latitude.nil?
          geocode = Geocoder.search(store_address.split(",")[1..-1].join(","))
          store_latitude = get_coord_from_address(geocode, "lat")
          if store_latitude.nil?
            store_latitude = "0"
          end
        end
      end
      if store_longitude.empty?
        if geocode.nil?
          geocode = Geocoder.search(store_address)
          @report.info "USING THE GEOCODER NOW"
        end
        store_longitude = get_coord_from_address(geocode, "lng")
        if store_longitude.nil?
          store_longitude = "0"
        end
      end
      store_city = store_item["city"]
      opening_hours = []
      if store_city.empty? && store_zipcode.empty? #
        if store_address.count(",") >= 2
          address_parts = store_item["address"].split(",")
          temp_zipcode = address_parts[-1].strip
          zipcode_parts = temp_zipcode.split
          if zipcode_parts.length == 2
            if zipcode_parts[0].length <= 4 && zipcode_parts[1].length <= 4
              if zipcode_parts[0].upcase.eql?(zipcode_parts[0]) && 
                  zipcode_parts[1].upcase.eql?(zipcode_parts[1]) && zipcode_parts[0] =~ /\d/ &&
                                                                    zipcode_parts[1] =~ /\d/
                store_temp_zipcode = temp_zipcode
              end
            elsif zipcode_parts[1].length > 4 && zipcode_parts[1].upcase.eql?(zipcode_parts[1]) &&
                !(zipcode_parts[1] =~ /\d/)
              temp_city = zipcode_parts[1]
            elsif zipcode_parts[0].length > 4 && zipcode_parts[0].upcase.eql?(zipcode_parts[0]) &&
                !(zipcode_parts[0] =~ /\d/)
              store_temp_city = zipcode_parts[0]
            end
          elsif zipcode_parts.length > 2
            if temp_zipcode.include?("Tel: ")
              tel_index = temp_zipcode.index("Tel: ")
              address_tel_index = store_address.index("Tel:")
              store_address = store_address[0..address_tel_index-1]
              temp_zipcode = temp_zipcode[0..tel_index-1].strip
              zipcode_parts = temp_zipcode.split
              if zipcode_parts.length == 2
                if zipcode_parts[0].length <= 4 && zipcode_parts[1].length <= 4
                  if zipcode_parts[0].upcase.eql?(zipcode_parts[0]) && 
                      zipcode_parts[1].upcase.eql?(zipcode_parts[1]) && zipcode_parts[0] =~ /\d/ &&
                      zipcode_parts[1] =~ /\d/
                    store_temp_zipcode = temp_zipcode
                  end
                end
              end
            elsif zipcode_parts[-1].length <= 4 && zipcode_parts[-2].length <= 4 && 
                zipcode_parts[-1].upcase.eql?(zipcode_parts[-1]) && 
                zipcode_parts[-2].upcase.eql?(zipcode_parts[-2]) && 
                zipcode_parts[-1] =~ /\d/ &&
                zipcode_parts[-2] =~ /\d/
                store_temp_zipcode = zipcode_parts[-2..-1].join(" ")
                store_temp_city = zipcode_parts[0..-3].join(" ")
            end
          end
          store_zipcode = store_temp_zipcode unless store_temp_zipcode.nil?
          store_city = store_temp_city unless store_temp_city.nil?
        elsif store_address.count(",") < 2
          store_zipcode, store_city = get_zipcode_city(store_address)
        end
        store_zipcode = "00000" if store_zipcode.empty?
        if store_city.nil? || store_city.empty?
          address_parts.reverse.each do |address_part|
            if cities.include?(address_part.strip.downcase)
              store_city = address_part
              break
            end
          end
          if store_city.nil? || store_city.empty?
            store_city = address_parts[-2].strip
          end
        end
        if store_zipcode.nil? || store_zipcode.empty?
          store_zipcode = "00000"
        end
        if store_latitude.nil? || store_latitude.to_s.empty?
          store_latitude = "00000"
        end
        if store_longitude.nil? || store_longitude.to_s.empty?
          store_longitude = "00000"
        end
        store_address = store_address.gsub(store_city, "")
        store_address = store_address.gsub(store_zipcode, "")
        temp_address = store_address.split(",").map{|a| a.strip} if store_address.count(",") >= 2
        if temp_address
          temp_address.delete("")
          store_address = temp_address.join(",")
        end
        @report.info << "TRYING TO REMOVE #{store_city} AND #{store_zipcode} from address"
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = stores_url
        end
        store.opening_hours = opening_hours
        store.save
        allStoresIds << store.id
        @report.info << "*************STORE*********************"
        @report.info << "STORE NAME IS #{store_name}"
        @report.info << "STORE CITY IS #{store_city}"
        @report.info << "STORE ZIP CODE IS #{store_zipcode}"
        @report.info << "STORE ADDRESS IS #{store_address}"
        @report.info << "STORE LONGITUDE #{store_longitude}"
        @report.info << "STORE LATITUDE #{store_latitude}"
        @report.info << "STORE PHONE #{store_phone}"
        @report.info << "STORE HOURS #{opening_hours}"
        @report.info << "STORE URL #{stores_url}"            
      end
    end
    allStoresIds
  end
  
  
    def get_zipcode(address)
    geocode = Geocoder.search(address)
    if geocode.size == 0
      return nil
    end
    zip_code = geocode[0].postal_code
    zip_code
  end
  
  def reset_mechanize
    mechanize ||= Mechanize.new do |m|
     m=m.set_proxy("107.151.152.210", 80)
    end
    mechanize
  end
  
  def get_leaflets(store_ids, leaflets_url)
    base_url = "http://www.silverdalebathrooms.co.uk/"
    allLeaflets = []
    mechanize = Mechanize.new
    brochure_page = mechanize.get(leaflets_url)
    brochure_hrefs = brochure_page.search("//div[@class='col-sm-4']//li/a[contains(@href, '.pdf')]")
    brochure_hrefs.each do |brochure_href|
      brochure_href = brochure_href["href"]
      pdf_url = "#{base_url}#{brochure_href}"
      leaflet_name =  pdf_url.split("/")[-1].gsub(".pdf", "_leaflet")
      @report.info << "Download from #{pdf_url}"
      leaflet = PQSDK::Leaflet.find pdf_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = leaflet_name
        leaflet.url = pdf_url
        leaflet.start_date = Time.now.to_s
        leaflet.end_date = Time.now.to_s        
        leaflet.store_ids = store_ids
        leaflet.save     
        @report.info << "********LEAFLET******"
        @report.info << leaflet.name
        @report.info << "********LEAFLET END********"
        allLeaflets << leaflet
      end
    end
    allLeaflets
  end
  
  def get_zipcode_city(address)
    @report.info << "Going to use geocoder for #{address}"
    geocode = Geocoder.search(address)
    @report.info << "USING THE GEOCODER NOW"
    if geocode.size == 0
      return nil
    end
    zip_code = geocode[0].postal_code
    geocode = geocode.first.data['address_components']
    city = nil
    geocode.each do |element|
      if element['types'].first == 'locality'
        city = element['long_name']
      end
    end
    return zip_code, city
  end
  
  def get_coord_from_address(geocode, coord)
    if geocode.size == 0
      return nil
    end
    latitude = geocode[0].data["geometry"]["location"]["lat"]
    longitude = geocode[0].data["geometry"]["location"]["lng"]
    return latitude if coord.eql?("lat")
    return longitude if coord.eql?("lng")
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '658e0282c5b60579999d9bca533d08e029bebad016327473cc5206d132c103f7'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "http://www.silverdalebathrooms.co.uk/brochure.asp")
    @report.info << "JUST ENDED THE SITE DATA"
  end
end