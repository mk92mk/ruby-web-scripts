#fc2825859431d25faaf0d2e1da7c215cffbab98f48622b51c60c625bcdbcdb8e FOR joann
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class Joann
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    states_url = "http://stores.joann.com/"
    mechanize = Mechanize.new
    mechanize.retry_change_requests=true
    states_page = mechanize.get(states_url)
    puts "GOT STATES URL PAGE"
    states_hrefs = states_page.search("//div[@class='itemlist']//a/@href")
    states_hrefs.each do |state_href|
      sleep(1)
      mechanize = Mechanize.new
      mechanize.retry_change_requests=true
      state_cities_page = mechanize.get(state_href.value)
      puts "GOT STATE CITIES PAGE AT #{state_href.value}"
      state_cities_hrefs = state_cities_page.search("//div[@class='itemlist']//a/@href")
      state_cities_hrefs.each do |state_city_href|
        sleep(1)
        mechanize = Mechanize.new
        mechanize.retry_change_requests=true
        state_city_stores = mechanize.get(state_city_href.value)
        puts "GOT STATE CITY STORES PAGE AT #{state_city_href.value}"
        store_details_hrefs = state_city_stores.search("//div[contains(@class, 'itemlist')]//a/@href")
        store_details_hrefs.each do |store_details_href|
          sleep(1)
          mechanize = Mechanize.new
          mechanize.retry_change_requests=true
#          store_details_page = mechanize.get("http://stores.joann.com/nj/paramus/2217/")
          store_details_page = mechanize.get(store_details_href.value)
          puts "GOT STORE DETAILS PAGE AT #{store_details_href.value}"
          store_name = store_details_page.search("//span[@itemprop='name']")[0].text.strip
          store_address = store_details_page.search("//div[@itemprop='streetAddress']")[0].text.strip
          next if store_address.empty?
          store_city = store_details_page.search("//span[@itemprop='addressLocality']")[0].text.strip
          store_zipcode = store_details_page.search("//span[@itemprop='postalCode']")[0].text.strip
          store_phone = store_details_page.search("//div[@itemprop='telephone']")[0].text.strip
          store_latitude = store_details_page.search("//meta[@property='place:location:latitude']
                                                      /@content")[0].value
          store_longitude = store_details_page.search("//meta[@property='place:location:longitude']
                                                      /@content")[0].value
          store_hours_nodes = store_details_page.search("//time[@itemprop='openingHours']/@datetime")
          opening_hours = parse_hours(store_hours_nodes)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude if store_latitude
            store.longitude = store_longitude if store_longitude
            store.phone = store_phone
            store.origin = store_details_href.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}" if store_longitude
          puts  "STORE LATITUDE #{store_latitude}" if store_latitude
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{store_details_href.value}"
          puts "******"
          allStoresIds << store.id          
          store_leaflets = get_leaflets(store.id, store_zipcode)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
        end
        puts "GOING TO NEXT CITY"
      end
      puts "GOING TO NEXT STATE"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode)
    leaflets = []
    base_url = "http://joann.shoplocal.com"
    leaflets_list_url = "http://joann.shoplocal.com/Joann?NuepRequest=true&RedirectUrl=&CityStateZip=#{store_zipcode}"
    sleep(1)
    mechanize = Mechanize.new
    mechanize.retry_change_requests=true
    leaflets_list_page = mechanize.get(leaflets_list_url)
    puts "GOT LEAFLETS LIST PAGE AT #{leaflets_list_page.uri.to_s}"
    store_leaflets_hrefs = leaflets_list_page.search("//div[contains(@class,'landingSubUnitA')]//a/@href")
    store_leaflets_hrefs.each do |store_leaflet_href|
      sleep(1)
      store_leaflet_url = "#{base_url}#{store_leaflet_href.value.sub("promotionviewmode=2", 
                                                                      "promotionviewmode=1")}"      
      mechanize = Mechanize.new
      mechanize.retry_change_requests=true
      store_leaflet_page = mechanize.get(store_leaflet_url)
      puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
      store_leaflet_name = store_leaflet_page.search("//div[@class='viewTitle']")[0].text.strip
      store_leaflet_dates = store_leaflet_page.search("//div[@class='validDatesContain']")[0].text
                                                                          .gsub("valid", "").strip
      leaflet_start_date = Time.parse(store_leaflet_dates.split("-")[0])
      leaflet_end_date = Time.parse(store_leaflet_dates.split("-")[1])
      script_text = store_leaflet_page.search("//script[contains(text(), 'pagesArray.push')]")[0].text
      script_imgs_txt = script_text.split("var pagesArray = [];")[-1].gsub("//akimages.shoplocal.com", 
                                                                        "http://akimages.shoplocal.com")
      urls = URI.extract(script_imgs_txt)
      images_urls = []
      urls.map{|u| images_urls << u if u.start_with?("http://akimages") && u.end_with?(".jpg")}
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.image_urls
      puts leaflet.store_ids
      puts "**********"
      leaflets << leaflet
    end
    leaflets
  end
  

  def parse_hours(hours_rows)
    week_hours = []
    hours_rows.each do |hour_row|
      store_hour = hour_row.value.downcase
#      if store_hour_node
#        store_hour = store_hour_node.value.downcase 
#      else
#        if hour_row["class"].include?("closed")
#          store_hour = hour_row.text.strip.downcase
#        end
#      end
      puts "GOT #{store_hour} TO PARSE NOW"
      if store_hour.include?("closed")
        breakpoint = store_hour.index("closed")
        days = store_hour[0..breakpoint-1].split("-")
      else
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        days = store_hour[0..breakpoint-1].split("-")
        days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      end
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mo")
      index = 0
    elsif day.include?("tu")
      index = 1
    elsif day.include?("we")
      index = 2
    elsif day.include?("th")
      index = 3  
    elsif day.include?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end
  
  

  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'fc2825859431d25faaf0d2e1da7c215cffbab98f48622b51c60c625bcdbcdb8e'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
    
  end

end

