#23844a92df3fe3535699d68c14a8cfd4727330a1786f690f7b4fafe2c313f853 for aarons
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'

class Aarons
  
  def get_stores(stores_list_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "https://www.aarons.com"
    mechanize = Mechanize.new
    sleep(1)
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST MAIN PAGE"
    stores_hrefs = stores_list_page.search("//div[@class='storeLoc']//a/@href")
    stores_hrefs.each do |store_href|
      store_url = "#{base_url}#{store_href.value}"
      sleep(2)
      mechanize = Mechanize.new
      begin
        store_details_page = mechanize.get(store_url)
      rescue Exception => e
        case e.message
        when /404/ then puts "404! STORE NOT FOUND AT #{store_url}"
          puts "GOING TO NEXT STORE"
          next
        end
      end
      puts "GOT STORE DETAILS PAGE AT #{store_url}"
      store_name = store_details_page.search("//h2[@itemprop='name']")[0].text.strip
      store_address = store_details_page.search("//span[@itemprop='street-address']")[0].text.strip
      store_city = store_details_page.search("//span[@itemprop='locality']")[0].text.strip
      store_zipcode = store_details_page.search("//span[@itemprop='postal-code']")[0].text.strip
      store_phone = store_details_page.search("//span[@id='ctl00_PageContent_phonespan']")[0].text.strip
      store_latitude = store_details_page.search("//input[@id='ctl00_PageContent_GoogleMaplat']/@value")[0]
                                                                                                    .value
      store_longitude = store_details_page.search("//input[@id='ctl00_PageContent_GoogleMaplng']/@value")[0]
                                                                                                    .value
      store_hours_nodes = store_details_page.search("//div[@class='weekdaystable']
                                                    //div[contains(@class,'day')]")
      opening_hours = parse_hours(store_hours_nodes)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      puts "******"
      allStoresIds << store.id
      store_leaflets = get_leaflets(store.id, store_zipcode)
      store_leaflets.each do |store_leaflet|
        allStoresLeafletsIds << store_leaflet.id
      end
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode)
    leaflets = []
    stores_list_url = "http://aarons.shoplocal.com/smartcircular2/scapi.aspx?action=browsestorelocation&siteid=1241&pretailerid=-98053&format=xmlsc&languageid=1&campaignid=766238e05b971393&citystatezip=#{store_zipcode}&startrow=1&count=4"
    sleep(1)
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE AT #{stores_list_url}"
    store_item = stores_list_page.search("//data[contains(@zipcode,'#{store_zipcode}')]")[0]
    unless store_item
      puts "NO LEAFLET FOUND FOR #{store_zipcode}"
      return leaflets
    end
    store_shop_id = store_item["value"]
    store_promotion_url = "http://aarons.shoplocal.com/smartcircular2/scapi.aspx?action=entry&siteid=1241&pretailerid=-98053&format=xmlsc&languageid=1&campaignid=766238e05b971393&pageimagewidth=152&listingimagewidth=137&storeid=#{store_shop_id}&brandmode=2"
    sleep(1)
    mechanize = Mechanize.new
    store_promotion_page = mechanize.get(store_promotion_url)
    puts "GOT STORE LEAFLET INFO PAGE AT #{store_promotion_url}"
    store_promotion_node =  store_promotion_page.search("//data[@vo='promotion']")[0]
    unless store_promotion_node
      puts "NO LEAFLET FOUND FOR #{store_zipcode}"
      return leaflets
    end
    store_promotion_code = store_promotion_node["value"]
    store_leaflet_name = store_promotion_page.search("//data[@vo='promotion']")[0]["name"]
    leaflet_start_date = store_promotion_page.search("//data[@vo='promotion']")[0]["startdate"]
    leaflet_end_date = store_promotion_page.search("//data[@vo='promotion']")[0]["enddate"]
    store_leaflet_url = "http://aarons.shoplocal.com/aarons/scapi.aspx?action=browsepageall&siteid=1241&pretailerid=-98053&format=xmlsc&languageid=1&campaignid=766238e05b971393&storeid=#{store_shop_id}&promotioncode=#{store_promotion_code}&pageimagewidth=152&pagecount=1000"
    mechanize = Mechanize.new
    sleep(2)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE AT #{store_leaflet_url}"
    images_urls = store_leaflet_page.search("//data/@imageurl")
    image_urls = []
    images_urls.each do |image_url|
      image_url = image_url.value.sub("152.0.90.0", "800.0.400.0")
      image_urls << image_url
      puts "#{image_url} ADDED IN THE MAIN ARRAY FOR LEAFLET"
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = store_leaflet_url
      leaflet.image_urls = images_urls
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.start_date
    puts leaflet.end_date
    puts leaflet.store_ids
    puts "********"
    leaflets << leaflet
    leaflets
  end
  
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      day_name = store_hour_node.search(".//span[@class='dayname']")[0].text.strip.downcase
      open_hour = store_hour_node.search("./text()")[0].text.strip.downcase
      close_hour = store_hour_node.search("./text()")[-1].text.strip.downcase
      puts "GOING TO PARSE #{day_name} AND #{open_hour} TO #{close_hour} NOW"
      day_index = get_day_index(day_name)
      day_hours = {}
      day_hours["weekday"] = day_index
      if open_hour.include?("closed")
        day_hours["closed"] = true
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
        next
      end
      formatted_time_open_am = convert_hours_am(open_hour.sub("am", "")) if open_hour.include?("am")
      formatted_time_open_pm = convert_hours_pm(open_hour.sub("pm", "")) if open_hour.include?("pm")
      formatted_time_close_am = convert_hours_am(close_hour.sub("am", "")) if close_hour.include?("am")
      formatted_time_close_pm = convert_hours_pm(close_hour.sub("pm", "")) if close_hour.include?("pm")
      day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
      day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
      day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
      day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
      week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '23844a92df3fe3535699d68c14a8cfd4727330a1786f690f7b4fafe2c313f853'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores("https://www.aarons.com/storesitemap.aspx")
    puts "JUST ENDED THE SITE DATA"
  end
end

