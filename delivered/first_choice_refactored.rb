require 'mechanize'
require 'json'
require 'byebug'
require 'pqsdk'
require 'geocoder'

class Crawler::Uk::FirstChoice < Crawler::Base
  
  def get_stores
    allStores = []
    start_url = "http://www.firstchoice.co.uk/holiday/shop-finder"
    base_url = "http://www.firstchoice.co.uk/holiday/**/shop-finder"   
    PQSDK::City.all.each do |city|
#    cities = ["aberdeen","cardiff","wales"]
#    cities.each do |city|
    coordinates = get_coord_from_address(city.name)
    latitude = coordinates["lat"]
    longitude = coordinates["lng"]
    mechanize = Mechanize.new
    sleep(10)
    stores_page = mechanize.post(base_url,
                                {"lat" => latitude,
                                  "lng" => longitude})
    @report.info << "GOT THE PAGE FOR #{city.name}"
    stores_parsed = JSON.parse(stores_page.body)
      stores_parsed.each do |store|
        store_name = store["name"]
        store_address = ""
        store_address << store["building"] if store["building"]
        store_address << " #{store["street"]}" if store["street"]
        store_address << " #{store["town"]}" if store["town"]
        store_city = store["city"]
        store_zipcode = store["zip"]
        store_phone = store["phone"]
        store_latitude = store["latitude"]
        store_longitude = store["longitude"]
        opening_hours = []
        opening_hours << parse_hours(store["timings"]["MONDAY"], 0) unless store["timings"]["MONDAY"].empty?        
        opening_hours << parse_hours(store["timings"]["TUESDAY"], 1) unless store["timings"]["TUESDAY"].empty? 
        opening_hours << parse_hours(store["timings"]["WEDNESDAY"], 2) unless store["timings"]["WEDNESDAY"].empty?
        opening_hours << parse_hours(store["timings"]["THURSDAY"], 3) unless store["timings"]["THURSDAY"].empty?
        opening_hours << parse_hours(store["timings"]["FRIDAY"], 4) unless store["timings"]["FRIDAY"].empty?
        opening_hours << parse_hours(store["timings"]["SATURDAY"], 5) unless store["timings"]["SATURDAY"].empty?
        opening_hours << parse_hours(store["timings"]["SUNDAY"], 6) unless store["timings"]["SUNDAY"].empty?
        opening_hours = opening_hours.compact
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = start_url
        end
        store.opening_hours = opening_hours
        store.save
        allStores << store.id
        @report.info << "*************STORE*********************"
        @report.info << "STORE NAME #{store_name}"
        @report.info << "STORE ADDRESS: #{store_address}"
        @report.info << "STORE CITY #{store_city}"
        @report.info << "STORE ZIPCODE #{store_zipcode}"
        @report.info << "STORE LONGITUDE #{store_longitude}"
        @report.info << "STORE LATITUDE #{store_latitude}"
        @report.info << "STORE PHONE #{store_phone}"
        @report.info << "STORE HOURS #{opening_hours}"
        @report.info << "STORE URL #{base_url}"
      end
    end
    allStores
  end
  
  def get_coord_from_address(address)
    geocode = Geocoder.search(address)
    if geocode.size == 0
      return nil
    end
    latitude = geocode[0].data["geometry"]["location"]["lat"]
    longitude = geocode[0].data["geometry"]["location"]["lng"]
    coord = {}
    coord["lat"] = latitude
    coord["lng"] = longitude
    return coord
  end
  
  def parse_hours(opening_hours, day_index)
    hours = {}
    hours["weekday"] = day_index
    if opening_hours.empty?
      return
    elsif opening_hours.downcase.include?("closed")
      hours["closed"] = true
    elsif opening_hours =~ /\d/ 
      hours_split = opening_hours.split("-")
      opening = hours_split[0]
      closing = hours_split[1]
      opening["."] = ":" if opening["."]
      closing["."] = ":"
      open_hour = opening.split(".")[0]
      open_hour = open_hour.to_i
      am_pm = get_am_pm(opening)
      hours["open_am"] = opening if am_pm.eql?("am")
      hours["open_am"] = convert_hours_am(opening) if hours["open_am"]
      hours["open_pm"] = opening if am_pm.eql?("pm")
      hours["open_pm"] = convert_hours_pm(opening) if hours["open_pm"]
      am_pm = get_am_pm(closing)
      hours["close_am"] = closing if am_pm.eql?("am")
      hours["close_am"] = convert_hours_am(closing) if hours["close_am"]      
      hours["close_pm"] = closing if am_pm.eql?("pm") 
      hours["close_pm"] = convert_hours_pm(closing) if hours["close_pm"]
    else
      return
    end
    hours
  end
  
  
  def get_leaflets(store_ids, downloads_url)
    allLeaflets = []
    base_leaflet_url = "https://firstchoice.inbro.net"
    mechanize = Mechanize.new
    sleep(10)
    brochures_downloads_page = mechanize.get(downloads_url)
    brochures_sections = brochures_downloads_page.search("//div[@class='contentColumn']")
    downloaded_brochures = []
    brochures_sections.each do |brochure_section|
      leaflet_name = brochure_section.search(".//h2").text
      brochure_section_urls = brochure_section.search(".//a/@href")
      brochure_section_urls.each do |brochure_section_url|
        brochure_url = brochure_section_url.value
        mechanize = Mechanize.new
        mechanize.redirect_ok = false
        sleep(10)
        begin
        leaflet_main_page = mechanize.get(brochure_url)
        leaflet_main_page, redirected_brochure_url = get_redirected_page(leaflet_main_page) unless (leaflet_main_page.code.eql?("200") || 
              leaflet_main_page.code.eql?("304"))
        rescue Exception => e
          case e.message
          when /404/ then @report.info << "404! THAT WAS FAKE PAGE AND URL #{brochure_url}"
            @report.info << "SO GOING TO NEXT ELEMENT"
            next
          else @report.info << 'right'
          end
        end
        brochure_pages = get_brochure_pages(leaflet_main_page, base_leaflet_url)
        leaflet = PQSDK::Leaflet.find redirected_brochure_url
        if leaflet.nil?
          leaflet = PQSDK::Leaflet.new
          leaflet.name = leaflet_name
          leaflet.url = redirected_brochure_url
          leaflet.start_date = Time.now.to_s
          leaflet.end_date = Time.now.to_s        
          leaflet.image_urls = brochure_pages
          leaflet.store_ids = store_ids
          leaflet.save   
          allLeaflets << leaflet     
          @report.info << "********LEAFLET***START******"
          @report.info << leaflet.name
          @report.info << "********LEAFLET END********"
        end
        @report.info << "given lealfet created"
      end
    end
    @report.info << "COLLECTED ALL LEAFLETS . CHECK PLZ"
    @report.info << "HURRAAAAAAAAAAAAAAAAH"
    @report.info << "TOTAL LEAFLETS #{allLeaflets.count}"
    allLeaflets
  end
  
  def get_redirected_page(main_page)
    begin
      redirected_location =	main_page.response['location']
      mechanize = Mechanize.new
      mechanize.redirect_ok = false
      sleep(2)
      main_page = mechanize.get(redirected_location)
      @report.info << " REDIRECTED TO #{redirected_location}"
    end while(!(main_page.code.eql?("304") || main_page.code.eql?("200")))
    return main_page, redirected_location
  end
  
  def get_brochure_pages(leaflet_page, base_leaflet_url)
    brochure_images = []
    brochure_pages_urls = leaflet_page.search("//div[@id='toc']//a/@href").map{|a| a.value}
    starting_page_number = get_page_number(brochure_pages_urls[0]).to_i
    ending_page_number = get_page_number(brochure_pages_urls[-1]).to_i
    starting_page_href = leaflet_page.search("//map[contains(@id, 'livelinks1')]/area/@href")
    starting_page_url = "#{base_leaflet_url}#{starting_page_href}"
    mechanize = Mechanize.new
    sleep(10)
    starting_page = mechanize.get(starting_page_url)
    image_url = starting_page.search("//img[contains(@usemap,'livelinks')]/@src")[0].value
    image_url_splits = image_url.split("-")
    (starting_page_number..ending_page_number).each do |page_number|
      image_number = "#{page_number}"
      image_id = image_number.rjust(6, "0")
      image_url_splits[1] = image_id
      image_url = image_url_splits.join("-")
      brochure_images << image_url
      @report.info << "#{image_url} ADDED IN MAIN ARRAY AND ON #{page_number} NOW "
    end
    brochure_images
  end
  
  
  def get_page_number(page_url)
    page_split = page_url.split("/")[-1]
    page_number_splits = page_split.split("_")[-1]
    page_number = page_number_splits.split("-")[-1]
    page_number
  end
  
  def get_am_pm(times)
    am_pm = ""
    if times.include?("am")
      am_pm = "am"
    elsif times.include?("pm")
      am_pm = "pm"
    end
    am_pm
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour.eql?("12")
      formatted_hour = "00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    else
      return times
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = "12"
    elsif hour == 1
      formatted_hour = "13"
    elsif hour == 2
      formatted_hour = "14"
    elsif hour == 3
      formatted_hour = "15"
    elsif hour == 4
      formatted_hour = "16"
    elsif hour == 5
      formatted_hour = "17"
    elsif hour == 6
      formatted_hour = "18"
    elsif hour == 7
      formatted_hour = "19"
    elsif hour == 8
      formatted_hour = "20"
    elsif hour == 9
      formatted_hour = "21"
    elsif hour == 10
      formatted_hour = "22"
    elsif hour == 11
      formatted_hour = "23"      
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end

  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '66c3a64ee6803b97d47ead67d6461f5a8c3128280b5caee83782f5b26ace6dd6'
    @report.info << "IN RUN METHOD"
    @report.info << "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    @report.info << "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids, "https://www.firstchoice.co.uk/about-us/brochures/")
    @report.info << "JUST ENDED THE SITE DATA"
  end
  

end