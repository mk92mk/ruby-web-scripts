#fbffb6ee9c84a27656bcf0c4ab747eb442c2c1b85dda9a167964beafe1ea6080 for KeyFOOD
#69.74.29.206	80 for KEYFOOD
#107.151.152.210	80 for KEYFOOD
#107.151.142.126	80 for KEYFOOD

require 'pqsdk'
require 'mechanize'
require 'uri'


class KeyFood
  
  def initialize
    @report = Logger.new("KeyFood#{Time.now.to_s}.log")
  end
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.keyfood.com"
    mechanize = Mechanize.new
    states_list_page = mechanize.get(states_url)
    puts "GOT STATES LIST PAGE"
    @report.info "GOT STATES LIST PAGE"
    states_hrefs = states_list_page.search("//section[@class='storeListWrap']//a/@href")
    states_hrefs.each do |state_href|
      mechanize = Mechanize.new
      sleep(1)
      state_stores_url = "#{base_url}#{state_href.value}"
      state_stores_page = mechanize.get(state_stores_url)
      puts "GOT STATE STORES PAGE #{state_stores_url}"
      @report.info "GOT STATE STORES PAGE #{state_stores_url}"
      stores_hrefs = state_stores_page.search("//a[@data-logcategory='storeLink']/@href")
      stores_hrefs = [state_stores_page.uri.to_s] if stores_hrefs.empty?
      stores_hrefs.each do |store_href|
        mechanize = Mechanize.new
        sleep(2)
        store_details_page = mechanize.get(store_href.value) unless store_href.is_a?(String)
        store_details_page = mechanize.get(store_href) if store_href.is_a?(String)
        puts "GOT STORE DETAILS PAGE #{store_href.value}" unless store_href.is_a?(String)
        puts "GOT STORE DETAILS PAGE #{store_href}" if store_href.is_a?(String)
        @report.info "GOT STORE DETAILS PAGE #{store_href.value}" unless store_href.is_a?(String)
        @report.info "GOT STORE DETAILS PAGE #{store_href}" if store_href.is_a?(String)
        store_info_article = store_details_page.search("//article[@id='content']")
        store_name = store_info_article.search(".//span[@itemprop='name']")[0].text.strip
        store_address = store_info_article.search("///span[@itemprop='streetAddress']")[0].text.strip
        store_city = store_info_article.search(".//span[@itemprop='addressLocality']")[0].text.strip
        store_zipcode = store_info_article.search(".//span[@itemprop='postalCode']")[0].text.strip
        store_phone_node = store_info_article.search(".//span[@itemprop='telephone']")[0]
        store_phone = store_phone_node.text.strip if store_phone_node
        store_latitude = store_details_page.search("//meta[@property='og:latitude']/@content")[0].value
        store_longitude = store_details_page.search("//meta[@property='og:longitude']/@content")[0].value
        hours_text_node = store_details_page.search("//meta[@itemprop='openingHours']/@content")[0]
        opening_hours = []
        if hours_text_node
          hours_text = hours_text_node.value 
          opening_hours = parse_hours(hours_text)
        end
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = store_href.value unless store_href.is_a?(String)
          store.origin = store_href if store_href.is_a?(String)
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_href.value}" unless store_href.is_a?(String)
        puts  "STORE URL #{store_href}" if store_href.is_a?(String)
        puts "******"
        @report.info  "*************STORE*********************"
        @report.info  "STORE NAME IS #{store_name}"
        @report.info  "STORE CITY IS #{store_city}"
        @report.info  "STORE ZIP CODE IS #{store_zipcode}"
        @report.info  "STORE ADDRESS IS #{store_address}"
        @report.info  "STORE LONGITUDE #{store_longitude}"
        @report.info  "STORE LATITUDE #{store_latitude}"
        @report.info  "STORE PHONE #{store_phone}"
        @report.info  "STORE HOURS #{opening_hours}"
        @report.info  "STORE URL #{store_href.value}" unless store_href.is_a?(String)
        @report.info  "STORE URL #{store_href}" if store_href.is_a?(String)
        @report.info "******"
        allStoresIds << store.id
        store_leaflets = get_leaflets(store.id, store_name, store_zipcode, store_address)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
    end
  end
  
  def get_leaflets(store_id, store_name, store_zipcode, store_address)
    leaflets = []
#    mechanize = Mechanize.new
    mechanize = reset_mechanize
    home_page = mechanize.get("http://keyfood.mywebgrocer.com/StoreLocator.aspx?f=cir#key-food")
    puts "GOT HOME PAGE"
    @report.info "GOT HOME PAGE"
    store_form_url = home_page.search("//form[@id = 'frmStoreLocator']/@action")[0].value
    stores_list_page = mechanize.post(store_form_url, {"postBack" => "1", "action" => "GL", 
                        "stateSellIndex" => "0", "citySellIndex" => "0", "selStates" => "",
                        "selCities" => "-1", "txtZipCode" => store_zipcode, "selZipCodeRadius" => "20"})
    puts "GOT STORES LIST PAGE FOR CIRCULARS"
    @report.info "GOT STORES LIST PAGE FOR CIRCULARS"
    current_store_div = stores_list_page.search("//p[@class='tInfo'][contains(text(), 
                                            '#{store_address}')]/ancestor::div[@class='StoreBox']")
    store_leaflet_node = current_store_div.search(".//a[@class='SingleItemLinkText'][contains(text(), 
                                                 'Weekly Circular')]/@href")[0]
    unless store_leaflet_node
      puts "NO LEAFLET FOUND"
      @report.info "NO LEAFLET FOUND"
      return leaflets
    end
    store_leaflet_url = store_leaflet_node.value
    sleep(2)
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT STORE LEAFLET PAGE #{store_leaflet_url}"
    @report.info "GOT STORE LEAFLET PAGE #{store_leaflet_url}"
    leaflet_pages_hrefs = store_leaflet_page.search("//div[@class='pagination']//a/@href")
    leaflet_dates = store_leaflet_page.search("//p[@class='CircularValidDates']")[0].text.strip
    leaflet_start_date = leaflet_dates.scan(/\d+\/\d+\/\d+/)[0]
    leaflet_end_date = leaflet_dates.scan(/\d+\/\d+\/\d+/)[1]
    images_urls = []
    leaflet_pages_hrefs.each do |leaflet_page_href|
#      mechanize = Mechanize.new
      mechanize = reset_mechanize
      sleep(1)
      image_page = mechanize.get(leaflet_page_href.value)
      image_src = image_page.search("//img[@id='PageImage']/@src")[0].value
      images_urls << image_src
      puts "#{image_src} ADDED IN THE ARRAY FOR LEAFLET"
      @report.info "#{image_src} ADDED IN THE ARRAY FOR LEAFLET"
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = "#{store_name}_leaflet"
      leaflet.image_urls = images_urls
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    @report.info leaflet.start_date
    @report.info leaflet.end_date
    @report.info "*****"
    leaflets << leaflet
    leaflets
  end
  
  def reset_mechanize
    mechanize ||= Mechanize.new do |m|
      m=m.set_proxy("107.151.152.210", 80)
    end
    mechanize
  end
  
  def parse_hours(hours_text)
    puts "GOT #{hours_text} to parse"
    @report.info "GOT #{hours_text} to parse"
    week_hours = []
    store_hours = []
    store_hours = hours_text.downcase.split("<br")
    store_hours.each do |store_hour|
      puts "STORE HOUR #{store_hour} TO PROCESS NOW"
      @report.info "STORE HOUR #{store_hour} TO PROCESS NOW"
      unless (store_hour.include?("mon") || store_hour.include?("tue") || 
            store_hour.include?("wed") || store_hour.include?("thu") ||
            store_hour.include?("fri") || store_hour.include?("sat") ||
            store_hour.include?("sun"))
        store_hour = "mon-sun #{store_hour}"
      end
      store_hour = store_hour.gsub("24 hours", "12am-11:59pm") if store_hour.include?("24 hours")
      store_hour = store_hour.gsub("daily", "") if store_hour.include?("daily")
      store_hour = store_hour.gsub("midnight", "11:59pm") if store_hour.include?("midnight")
      store_hour = store_hour.gsub("/7 days a week", "")
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
                .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      hours[1] = "11:59pm" if hours[1].include?("midnight") || hours[1].include?("12mid")
      hours[0] = "#{hours[0]}#{get_am_pm(hours[0])}" unless hours[0].include?("am") || hours[0].
                                                                                  include?("pm")
      hours[1] = "#{hours[1]}#{get_am_pm(hours[1])}" unless hours[1].include?("am") || hours[1].
                                                                                  include?("pm")                                                                                
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            #            day_hours["opening_time"] = hours[0]
            #            day_hours["closing_time"] = hours[1]
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(timing)
    time_split = timing.split(":")
    hour = time_split[0].to_i
    if hour >=0 && hour <= 11
      day_part = "am"
    elsif hour >= 12 && hour <= 23
      day_part = "pm"
    end
    day_part
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = 'fbffb6ee9c84a27656bcf0c4ab747eb442c2c1b85dda9a167964beafe1ea6080'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://www.keyfood.com/pd/stores")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end