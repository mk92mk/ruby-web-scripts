#980ef1ed79a909d3961327573868d7950745ca42cffc835baa2d39c4609abbe1 FOR SHOP JUSTICE
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'byebug'


class ShopJustice
  
  def get_stores
    allStoresIds = []
    base_url = "http://maps.shopjustice.com/api/getAsyncLocations?template=search&level=search&radius=100&search="
    mechanize = Mechanize.new
    cities_page = mechanize.get("http://api.promoqui.eu/v1/cities")
    puts "GOT PROMOQUI CITIES PAGE"
    cities_list = JSON.parse(cities_page.body)
    cities_list.each do |city|
      next unless (city["state"].eql?("confirmed") && city["country"].eql?("USA"))
      city_stores_url = "#{base_url}#{city["name"]}"
      sleep(1)
      mechanize = Mechanize.new
      city_stores_page = mechanize.get(city_stores_url)
      puts "GOT CITY STORES PAGE FOR #{city["name"]} AT #{city_stores_url}"
      stores_parsed = JSON.parse(city_stores_page.body)
      stores_html = stores_parsed["maplist"]
      mechanize = Mechanize.new
      stores_page = Mechanize::Page.new(nil,{'content-type'=>'text/html'},stores_html,nil,mechanize)
      stores_list = stores_page.search("//div[@class='mapListItem']")
      stores_list.each do |store_item|
        store_name = store_item.search(".//div[@class='loc-name']")[0].text
        store_address = store_item.search(".//div[@class='addr em-indent']")[0].text
        store_city = store_item.search(".//div[@class='csz em-indent']")[0].text.strip.split(",")[0]
        store_zipcode = store_item.search(".//div[@class='csz em-indent']")[0].text.split(",")[1]
                                                                          .match(/\d+/).to_s
        store_phone = store_item.search(".//div[@class='phone em-indent']")[0].text
        store_loc_id = store_item.search("./@id")[0].value
        store_json = stores_parsed["markers"].find {|marker| marker['locationId'] == store_loc_id}
        store_latitude = store_json["lat"]
        store_longitude = store_json["lng"]
        hours_rows = store_item.search(".//div[contains(@class, 'day-hour-row')]")
        puts "AT #{store_address} NOW"
        opening_hours = parse_hours(hours_rows)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude
          store.longitude = store_longitude
          store.phone = store_phone
          store.origin = city_stores_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE #{store_longitude}"
        puts  "STORE LATITUDE #{store_latitude}"
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{city_stores_url}"
        puts "******"
        allStoresIds << store.id
      end
      puts "GOING TO NEXT CITY"
      sleep(2)
    end
    allStoresIds
  end
  
  def parse_hours(hours_rows)
    week_hours = []
    hours_rows.each do |hour_row|
      store_hour_node = hour_row.search(".//meta[@itemprop='openingHours']/@content")[0]
      if store_hour_node
        store_hour = store_hour_node.value.downcase 
      else
        if hour_row["class"].include?("closed")
          store_hour = hour_row.text.strip.downcase
        end
      end
      puts "GOT #{store_hour} TO PARSE NOW"
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mo")
      index = 0
    elsif day.include?("tu")
      index = 1
    elsif day.include?("we")
      index = 2
    elsif day.include?("th")
      index = 3  
    elsif day.include?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end
  
  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    sleep(1)
    leaflet_data_url = "http://cdn-api.syndeca.com/v1/rest/guide/C6BD10C2-57CA-7D5B-4FCC-BCAAC34A0E1E/catalog/red-white-and-boho?coord_units=percent&precision=5"
    mechanize = Mechanize.new
    leaflets_data_page = mechanize.get(leaflet_data_url)
    puts "GOT LEAFLETS PAGE AT #{leaflet_data_url}"
    leaflet_parsed = JSON.parse(leaflets_data_page.body)
    leaflet_pdf_url = leaflet_parsed["pdfUrl"]
    store_leaflet_name = "ShopJustice Girls Clothing Catalog"
    puts "DOWNLOAD FROM #{leaflet_pdf_url}"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
      leaflet.store_ids = store_ids
      allStoresLeafletsIds << leaflet.id   
    end
    #leaflet.save 
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    allStoresLeafletsIds
  end
    
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '980ef1ed79a909d3961327573868d7950745ca42cffc835baa2d39c4609abbe1'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end
  
end