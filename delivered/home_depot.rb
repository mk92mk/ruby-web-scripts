#66930d99de424e462ccee641917972492c71d4e3da9382c54f2549c78c62045d FOR HOME DEPOT
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
require 'time'
require 'byebug'


class HomeDepot
  
  def get_stores
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://www.homedepot.com"
    states_url = "http://www.homedepot.com/StoreFinder/storeDirectory"
    mechanize = Mechanize.new
    states_page = mechanize.get(states_url)
    puts "GOT STATES URL PAGE"
    states_hrefs = states_page.search("//div[@id='stateListing']//a/@href")
    states_hrefs.each do |state_href|
      sleep(1)
      mechanize = Mechanize.new
      state_stores_url = "#{base_url}#{state_href.value}"
      state_stores_page = mechanize.get(state_stores_url)
      puts "GOT STATE STORES PAGE AT #{state_stores_url}"
      stores_hrefs = state_stores_page.search("//div[@id='localListings']//a/@href")
      stores_hrefs.each do |store_href|
        store_details_url = "#{base_url}#{store_href.value}"
        sleep(1)
        mechanize = Mechanize.new
        store_details_page = mechanize.get(store_details_url)
        puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
        store_name = store_details_page.search("//h3//span[@itemprop='name']")[0].text.strip
        store_address = store_details_page.search("//span[@itemprop='streetAddress']")[0].text.strip
        store_city = store_details_page.search("//span[@itemprop='addressLocality']")[0].text.strip
        store_zipcode = store_details_page.search("//span[@itemprop='postalCode']")[0].text.strip
        store_phone = store_details_page.search("//span[@itemprop='telephone']")[0].text.strip
        store_latitude = store_details_page.search("//meta[@itemprop='latitude']/@content")[0].value
        store_longitude = store_details_page.search("//meta[@itemprop='longitude']/@content")[0].value
        store_hours_nodes = store_details_page.search("//li[@itemprop='openingHours']/@content")
        opening_hours = parse_hours(store_hours_nodes)
        store = PQSDK::Store.find store_address, store_zipcode
        if store.nil?
          store = PQSDK::Store.new
          store.name = store_name
          store.address = store_address
          store.zipcode = store_zipcode
          store.city = store_city
          store.latitude = store_latitude if store_latitude
          store.longitude = store_longitude if store_longitude
          store.phone = store_phone
          store.origin = store_details_url
        end
        store.opening_hours = opening_hours
        #store.save
        puts  "*************STORE*********************"
        puts  "STORE NAME IS #{store_name}"
        puts  "STORE CITY IS #{store_city}"
        puts  "STORE ZIP CODE IS #{store_zipcode}"
        puts  "STORE ADDRESS IS #{store_address}"
        puts  "STORE LONGITUDE IS #{store_longitude}" if store_longitude
        puts  "STORE LATITUDE IS #{store_latitude}" if store_latitude
        puts  "STORE PHONE #{store_phone}"
        puts  "STORE HOURS #{opening_hours}"
        puts  "STORE URL #{store_details_url}"
        puts "******"
        allStoresIds << store.id
        store_leaflet_url = store_details_page.search("//a[contains(.//text(), 'Local')]/@href")[0].value
        store_home_id = store_leaflet_url.match(/storeId=\d+/).to_s.gsub("storeId=", "").to_s
        store_leaflets = get_leaflets(store.id, store_zipcode)
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      puts "GOING TO NEXT STATE"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode)
    leaflets = []
    base_leaflet_url = "http://localad.homedepot.com/HomeDepotSD2?listingid=0&NuepRequest=true&RedirectUrl=%2FHomeDepotSD2%2FEntry%3FstoreId%3D10051%26langId%3D-1%26catalogId%3D10053&CityStateZip=#{store_zipcode}"
    sleep(1)
    mechanize = Mechanize.new
    base_leaflet_page = mechanize.get(base_leaflet_url)
    puts "GOT BASE LEAFLET PAGE AT #{base_leaflet_url}"
    store_home_id = base_leaflet_page.uri.to_s.match(/storeid=\d+/).to_s.gsub("storeid=", "").to_s
    next_leaflet_url = URI.extract(base_leaflet_page.body.to_s)[-1]
##    sleep(1)
#    byebug
##    mechanize = Mechanize.new
#    next_leaflet_page = mechanize.get(next_leaflet_url)
#    puts "GOT REDIRECTED URL AT #{next_leaflet_page.uri.to_s}"
#    store_id_url = next_leaflet_page.uri.to_s
#    byebug
#    store_home_id = store_id_url.match(/storeid=\d+/).to_s.gsub("storeid=", "").to_s
    leaflet_promotion_url = "http://api2.shoplocal.com/retail/72a704d3673c0ec9/2013.1/json/Promotions?storeid=#{store_home_id}&preferredlanguage=1&siteid=1718&callback=angular.callbacks._1268026380&sneakpeek=n&promotionsortmode=3"
    sleep(1)
    mechanize = Mechanize.new
    leaflet_promotion_page = mechanize.get(leaflet_promotion_url)
    puts "GOT LEAFLET PROMOTION PAGE AT #{leaflet_promotion_url}"
    promotions_parsed = JSON.parse(leaflet_promotion_page.body.gsub("angular.callbacks._1268026380(", 
                                                            "").chomp(");"))
    leaflets_list = promotions_parsed["Results"]
    leaflets_list.each do |leaflet_item|
      store_leaflet_name = leaflet_item["Title"]  
      leaflet_start_date = leaflet_item["SaleStartDateString"]
      leaflet_end_date = leaflet_item["SaleEndDateString"]
      promotion_code = leaflet_item["Code"]
      leaflet_images_url = "http://api2.shoplocal.com/retail/72a704d3673c0ec9/2013.1/json/PromotionPages?storeid=#{store_home_id}&promotioncode=#{promotion_code}&preferredlanguage=1&siteid=1718&callback=angular.callbacks._280164390&sneakpeek=n"
      store_leaflet_url = leaflet_images_url
      sleep(2)
      mechanize = Mechanize.new
      leaflet_images_page = mechanize.get(leaflet_images_url)
      puts "GOT LEAFLET IMAGES PAGE AT #{leaflet_images_url}"
      images_parsed = JSON.parse(leaflet_images_page.body.to_s.
                                 gsub("angular.callbacks._280164390(", "").chomp(");"))["Results"]
      images_urls = []
      images_parsed.each do |image_parsed|
        image_loc = image_parsed["ImageLocation"].sub("200.0.90.0", "820.0.90.0")
        images_urls << image_loc
        puts "#{image_loc} ADDED IN THE MAIN ARRAY FOR LEAFLET"
      end
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.start_date = leaflet_start_date
        leaflet.end_date = leaflet_end_date
      end
      if !leaflet.store_ids.include?(store_id)
        leaflet.store_ids << store_id
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.start_date
      puts leaflet.end_date
      puts leaflet.image_urls
      puts leaflet.store_ids
      puts "**********"
      leaflets << leaflet
    end
    leaflets
  end
  
  
  def parse_hours(store_hours_nodes)
    week_hours = []
    store_hours_nodes.each do |store_hour_node|
      store_hour = store_hour_node.value.downcase
      puts "GOT #{store_hour} TO PARSE NOW"
      if store_hour.include?("closed")
        breakpoint = store_hour.index("closed")
        days = store_hour[0..breakpoint-1].split("-")
      else
        first_digit = /\d+/.match(store_hour).to_s
        breakpoint = store_hour.index(first_digit)
        days = store_hour[0..breakpoint-1].split("-")
        days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      end
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                                .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
    
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '66930d99de424e462ccee641917972492c71d4e3da9382c54f2549c78c62045d'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores
    puts "JUST ENDED THE SITE DATA"
  end
  
end