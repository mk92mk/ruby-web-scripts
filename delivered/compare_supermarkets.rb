# 3776337d7d85e27915e30a2e98996c4301d0a2fb8513b0ff7d65ee7bcaf16b16 for compare super markets
require 'pqsdk'
require 'mechanize'
require 'geocoder'

class CompareSuperMarkets
  
  def initialize
    @report = Logger.new("CompareSuperMarkets#{Time.now.to_s}.log")
  end
  
  def get_stores(stores_list_url, weekly_circulars_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    base_url = "http://comparesupermarkets.com"
    Geocoder::Configuration.timeout = 15
    mechanize = Mechanize.new
    weekly_circulars_page = mechanize.get(weekly_circulars_url)
    puts "GOT WEEKLY CIRCULARS PAGE"
    @report.info "GOT WEEKLY CIRCULARS PAGE"
    sleep(2)
    mechanize = Mechanize.new
    stores_list_page = mechanize.get(stores_list_url)
    puts "GOT STORES LIST PAGE"
    @report.info "GOT STORES LIST PAGE"
    stores_items = stores_list_page.search("//markers/marker")
    stores_items.each do |store_item|
      store_name = store_item.search(".//name")[0].text
      store_address = store_item.search(".//address")[0].text
      
      store_city = ""
      store_zipcode = "00000"
      address_parts = store_address.split(",").map{|ad| ad.strip}
      address_parts.delete("")
      if store_address.include?(",")
        address_parts = store_address.split(",").map{|ad| ad.strip}
        address_parts.delete("")
        store_city = address_parts[-2]
        store_zipcode = address_parts[-1].strip.scan(/\d+/).first.to_s if store_zipcode.eql?("00000")
      end
      store_latitude = store_item.search(".//lat")[0].text
      store_longitude = store_item.search(".//lng")[0].text
      store_phone = store_item.search(".//phone")[0].text
      store_url = store_item.search(".//url")[0].text.strip
      opening_hours = []
      store_leaflets_imgs = []
      unless store_url.empty?
        mechanize = Mechanize.new
        store_details_page = mechanize.get(store_url) 
        puts "GOT STORE DETAILS PAGE #{store_url}"
        @report.info "GOT STORE DETAILS PAGE #{store_url}"
        store_hours_element = store_details_page.search("//div[@class='item-page']
                                                    //p[contains(.//strong, 'Store Hours')]/text()")[0]
        store_hours_text = store_hours_element.text.strip if store_hours_element
        if store_hours_text.to_s.length < 5
          store_hours_element = store_details_page.search("//div[@class='item-page']
                            //p[contains(.//strong, 'Store Hours')]")[0]
          store_hours_text = store_hours_element
                             .text.gsub("Store Hours", "") if store_hours_element
        end
        if store_hours_text.to_s.length < 5
          store_hours_element = store_details_page.search("//div[@class='item-page']
                                            //div[contains(.//strong//text(), 'Store Hours')]")
          store_hours_text = store_hours_element
                              .text.gsub("Store Hours", "") if store_hours_element
        end
        store_address_node = store_details_page.search("//div[@class='item-page']//
                                                    p[contains(.//strong/text(), 'Address')]")[0]
        store_address = store_address_node.text.gsub("Address", "").gsub(":","") if store_address_node
        if store_address.include?(",")
          address_parts = store_address.split(",").map{|ad| ad.strip}
          address_parts.delete("")
          store_city = address_parts[-2]
          store_zipcode = address_parts[-1].strip.scan(/\d+/).first if store_zipcode.eql?("00000")
        end                                                                                         
        opening_hours = parse_hours(store_hours_text)
        store_leaflets_imgs = store_details_page.search("//a[@class='sigProLink']/@href")
      else
        store_url = stores_list_url
        store_details_url = weekly_circulars_page.search("//div[@class='item-page']
                                                         //a[contains(.//text(),
                                                       '#{address_parts[0][0..15]}')]/@href")[0]
        if store_details_url
          store_url = "#{base_url}#{store_details_url.value}"
          puts "GOT STORE DETAILS PAGE #{store_url}"
          @report.info "GOT STORE DETAILS PAGE #{store_url}"
          mechanize = Mechanize.new
          sleep(1)
          store_details_page = mechanize.get(store_url)
          store_address = store_details_page.search("//div[@class='item-page']//
                                                    p[contains(.//strong/text(), 'Address')]")[0].text
                                                                    .gsub("Address", "").gsub(":","")
          if store_address.include?(",")
            address_parts = store_address.split(",").map{|ad| ad.strip}
            address_parts.delete("")
            store_city = address_parts[-2]
            store_zipcode = address_parts[-1].strip.scan(/\d+/).first if store_zipcode.eql?("00000")
          end
          store_hours_text = store_details_page.search("//div[@class='item-page']
                                                    //p[contains(.//strong, 'Store Hours')]/text()")[0]
                                                                                          .text.strip
          store_hours_text = store_details_page.search("//div[@class='item-page']
                            //p[contains(.//strong, 'Store Hours')]")[0]
                            .text.gsub("Store Hours", "") if store_hours_text.length < 5   
          opening_hours = parse_hours(store_hours_text)
          store_leaflets_imgs = store_details_page.search("//a[@class='sigProLink']/@href")
        end
      end
      store_zipcode = "00000" if store_zipcode.to_s.empty?
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        if store_city.empty?
          geocode = Geocoder.search(store_address)
          store_city = get_city_from_address(geocode)
        end
        store_address = store_address.gsub(store_city, "").gsub(store_zipcode, "").gsub(",", "").squeeze(" ")
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_url}"
      @report.info  "*************STORE*********************"
      @report.info  "STORE NAME IS #{store_name}"
      @report.info  "STORE CITY IS #{store_city}"
      @report.info  "STORE ZIP CODE IS #{store_zipcode}"
      @report.info  "STORE ADDRESS IS #{store_address}"
      @report.info  "STORE LONGITUDE #{store_longitude}"
      @report.info  "STORE LATITUDE #{store_latitude}"
      @report.info  "STORE PHONE #{store_phone}"
      @report.info  "STORE HOURS #{opening_hours}"
      @report.info  "STORE URL #{store_url}"
      unless store_leaflets_imgs.empty?
        store_leaflets = get_leaflets(store_leaflets_imgs, store_url, 
                                      store.id, "#{store_address}_leaflet")
        store_leaflets.each do |store_leaflet|
          allStoresLeafletsIds << store_leaflet.id
        end
      end
      allStoresIds << store.id
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_leaflets_imgs, store_leaflet_url, store_id, store_leaflet_name)
    leaflets = []
    images_urls = []
    base_url = "http://comparesupermarkets.com"
    store_leaflets_imgs.each do |leaflet_img|
      images_urls << "#{base_url}#{leaflet_img.value}"
      puts "#{base_url}#{leaflet_img.value} ADDED IN THE MAIN ARRAY FOR LEAFLET"
    end
    leaflet = PQSDK::Leaflet.find store_leaflet_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.url = store_leaflet_url
      leaflet.name = store_leaflet_name
      leaflet.image_urls = images_urls
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    # leaflet.save
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    @report.info "************************LEAFLET***"
    @report.info leaflet.name
    @report.info leaflet.url
    @report.info leaflet.store_ids
    leaflets << leaflet
    leaflets
  end
  
  def get_city_from_address(geocode)
    if geocode.size == 0
      return nil
    end
    city = geocode[0].city
    puts "#{city} FOUND BY GEOCODE"
    @report.info "#{city} FOUND BY GEOCODE"
    city
  end
  
  def parse_hours(hours_text)
    store_hours = []
    week_hours = []
    hours_lines = hours_text
    puts "GOT #{hours_text} TO PARSE"
    @report.info "GOT #{hours_text} TO PARSE"
    unless (hours_text.downcase.include?("mon") || hours_text.downcase.include?("tue") || 
          hours_text.downcase.include?("wed") || hours_text.downcase.include?("thu") ||
          hours_text.downcase.include?("fri") || hours_text.downcase.include?("sat") ||
          hours_text.downcase.include?("sun"))
      if hours_text.downcase.include?("am") || hours_text.downcase.include?("pm")
        hours_text = "Mon-Sun #{hours_text}"
        hours_text = hours_text.downcase.gsub("7 days a week", "")
      end
    end
    if hours_text.downcase.eql?("open 24 hours")
      hours_text = "Mon-Sun 12am-11:59pm"
    end
    holiday_index = hours_lines.downcase.index("holiday hours")
    hours_text = hours_lines[0..holiday_index-1] if holiday_index
    christmas_index = hours_lines.downcase.index("christmas eve")
    hours_text = hours_lines[0..christmas_index-1] if christmas_index
    first_digit = /\d+/.match(hours_text).to_s
    breakpoint = hours_text.index(first_digit)
    temp_days = hours_text[0..breakpoint-1]
    temp_mixed = hours_text[breakpoint..-1]
    temp_indexes = []
    if temp_mixed.downcase.include?("mon") || temp_mixed.downcase.include?("tue") || 
        temp_mixed.downcase.include?("wed") || temp_mixed.downcase.include?("thu") ||
        temp_mixed.downcase.include?("fri") || temp_mixed.downcase.include?("sat") ||
        temp_mixed.downcase.include?("sun")
      temp_mixed = temp_mixed.downcase
      mon_index = temp_indexes << temp_mixed.index("mon") if temp_mixed.index("mon")
      tue_index = temp_indexes << temp_mixed.index("tue") if temp_mixed.index("tue")
      wed_index = temp_indexes << temp_mixed.index("wed") if temp_mixed.index("wed")
      thu_index = temp_indexes << temp_mixed.index("thu") if temp_mixed.index("thu")
      fri_index = temp_indexes << temp_mixed.index("fri") if temp_mixed.index("fri")
      sat_index = temp_indexes << temp_mixed.index("sat") if temp_mixed.index("sat")
      sun_index = temp_indexes << temp_mixed.index("sun") if temp_mixed.index("sun")
      temp_indexes = temp_indexes.sort
      if temp_indexes.length == 1
        store_hours << temp_mixed[temp_indexes[0]..-1]
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
      elsif temp_indexes.length == 2
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
        if temp_indexes[1] - temp_indexes[0] > 6
          store_hours << temp_mixed[temp_indexes[0]..temp_indexes[1]-1]
          store_hours << temp_mixed[temp_indexes[1]..-1]
        end
        if temp_indexes[1] - temp_indexes[0] <= 6
          store_hours << temp_mixed[temp_indexes[0]..-1]
        end
      elsif temp_indexes.length == 4
        store_hours << "#{temp_days} #{temp_mixed[0..temp_indexes[0]-1]}"
        if temp_indexes[1] - temp_indexes[0] <= 6
          store_hours << temp_mixed[temp_indexes[0]..temp_indexes[2]]
        end
        if temp_indexes[3] - temp_indexes[2] > 6
          store_hours << temp_mixed[temp_indexes[2]..temp_indexes[3]-1]
        end
        store_hours << temp_mixed[temp_indexes[3]..-1]
      end
    else
      store_hours << hours_text
    end
    
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase   
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split(".") if temp_days.include?(".") && !temp_days.include?("-") &&
                                     !temp_days.include?("to") && !temp_days.include?("–")
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip} unless days
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      if days.length == 3
        store_hours << "#{days[0]}-#{days[2]} #{open_hours}"
        next
      end
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
      if days.length == 2 # > 1(before)
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".", "")) if  hours[0]
                          .include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
                          .include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".", "")) if hours[1]
                          .include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                          .include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m","").sub(".","")) if  hours[0]
              .include?("am")||hours[0].include?("a.m")||hours[0].include?("a.m.")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.","")) if hours[0]
              .include?("pm")||hours[0].include?("p.m.")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m","").sub(".","")) if hours[1]
              .include?("am")||hours[1].include?("a.m")||hours[1].include?("a.m.")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
              .include?("pm")||hours[1].include?("p.m.")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("Closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "").sub("a.m", "").sub(".","")) if  hours[0]
                        .include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "").sub("p.m.", "")) if hours[0]
                        .include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "").sub("a.m", "").sub(".","")) if hours[1]
                        .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "").sub("p.m.","")) if hours[1]
                        .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("Closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index} 
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mon")
      index = 0
    elsif day.include?("tue")
      index = 1
    elsif day.include?("wed")
      index = 2
    elsif day.include?("thu")
      index = 3  
    elsif day.include?("fri")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun")
      index = 6     
    end
    index
  end
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '3776337d7d85e27915e30a2e98996c4301d0a2fb8513b0ff7d65ee7bcaf16b16'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    @report.info "IN RUN METHOD"
    @report.info "GOING TO COLLECT THE STORES AND LEAFLETS TOGETHER"
    store_ids, leaflet_ids = get_stores("http://comparesupermarkets.com/index.php?option=com_storelocator&format=feed&searchall=1&Itemid=516&catid=-1&tagid=-1&featstate=0",
                                        "http://comparesupermarkets.com/weekly-sales")
    puts "JUST ENDED THE SITE DATA"
    @report.info "JUST ENDED THE SITE DATA"
  end
end