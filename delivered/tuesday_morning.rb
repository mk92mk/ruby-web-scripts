#413ee9702a6c88acdd7bc727ee0cebe9b29dd83796cdb5e389131f7c2e2341f9 for Tuesday Morning
require 'pqsdk'
require 'mechanize'
require 'uri'
require 'json'
#load './mechanized_rapper.rb'
require 'byebug'

class TuesdayMorning
  
  def get_stores(states_url)
    allStoresIds = []
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    mechanize.retry_change_requests=true
    mechanize.idle_timeout=4
    mechanize.content_encoding_hooks << lambda{|httpagent, uri, response, body_io| response['Content-Encoding'] = 'none' if response['Content-Encoding'].to_s == 'UTF-8' }
    states_page = mechanize.get(states_url)
    puts "GOT STATES PAGE"
    states_hrefs = states_page.search("//a[@data-gaq='Domain Map List, Region']/@href")
    states_hrefs.each do |state_href|
      sleep(1)
      mechanize = Mechanize.new
      mechanize.retry_change_requests=true
      mechanize.idle_timeout=4      
      mechanize.content_encoding_hooks << lambda{|httpagent, uri, response, body_io| response['Content-Encoding'] = 'none' if response['Content-Encoding'].to_s == 'UTF-8' }
      state_cities_page = mechanize.get(state_href.value)
      puts "GOT STATE CITIES PAGE AT #{state_href.value}"
      state_cities_hrefs = state_cities_page.search("//a[@data-gaq='Region Map List, City']/@href")
      state_cities_hrefs.each do |state_city_href|
        sleep(1)
        mechanize = Mechanize.new
        mechanize.retry_change_requests=true
        mechanize.idle_timeout=4        
        mechanize.content_encoding_hooks << lambda{|httpagent, uri, response, body_io| response['Content-Encoding'] = 'none' if response['Content-Encoding'].to_s == 'UTF-8' }
        city_stores_page = mechanize.get(state_city_href.value)
        puts "GOT CITY STORES PAGE AT #{state_city_href.value}"
        stores_list = city_stores_page.search("//div[@class='city-list-right']")
        stores_list.each do |store_item|
          store_name = store_item.search("///div[@class='city-list-right']
                                          //a[contains(@data-gaq, 'Location Name')]")[0].text.strip
          store_address = store_item.search(".//div[@class='addr']")[0].text.strip
          store_cityzipcode = store_item.search(".//div[@class='csz']")[0].text.strip
          store_city = store_cityzipcode.split(",")[0].strip
          store_zipcode = store_cityzipcode.match(/\d+/).to_s
          store_phone = store_item.search(".//div[@class='phone']")[0].text.strip
          store_details_url = store_item.search(".//a[@data-gaq = 'Indy Map List, Location Name']
                                                 /@href")[0].value
          sleep(2)
          mechanize = Mechanize.new
          mechanize.retry_change_requests=true
          mechanize.idle_timeout=4
          mechanize.content_encoding_hooks << lambda{|httpagent, uri, response, body_io| response['Content-Encoding'] = 'none' if response['Content-Encoding'].to_s == 'UTF-8' }
          store_details_page = mechanize.get(store_details_url)
          puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
          script_text = store_details_page.search("//script[contains(text(), 'RLS.centerLat')]")[0].text
          latitude_text = script_text.match(/RLS.centerLat = \d+.\d+|RLS.centerLat = -\d+.\d+/).to_s
          longitude_text = script_text.match(/RLS.centerLng = \d+.\d+|RLS.centerLng = -\d+.\d+/).to_s
          store_latitude = latitude_text.match(/\d+.\d+|-\d+.\d+/).to_s
          store_longitude = longitude_text.match(/\d+.\d+|-\d+.\d+/).to_s
          hours_rows = store_item.search(".//div[contains(@class, 'day-hour-row')]")
          puts "ON #{store_address} NOW"
          opening_hours = parse_hours(hours_rows)
          store = PQSDK::Store.find store_address, store_zipcode
          if store.nil?
            store = PQSDK::Store.new
            store.name = store_name
            store.address = store_address
            store.zipcode = store_zipcode
            store.city = store_city
            store.latitude = store_latitude
            store.longitude = store_longitude
            store.phone = store_phone
            store.origin = state_city_href.value
          end
          store.opening_hours = opening_hours
          #store.save
          puts  "*************STORE*********************"
          puts  "STORE NAME IS #{store_name}"
          puts  "STORE CITY IS #{store_city}"
          puts  "STORE ZIP CODE IS #{store_zipcode}"
          puts  "STORE ADDRESS IS #{store_address}"
          puts  "STORE LONGITUDE #{store_longitude}"
          puts  "STORE LATITUDE #{store_latitude}"
          puts  "STORE PHONE #{store_phone}"
          puts  "STORE HOURS #{opening_hours}"
          puts  "STORE URL #{state_city_href.value}"
          puts "******"
          allStoresIds << store.id
          store_leaflets = get_leaflets(store.id, store_zipcode, store_name)
          store_leaflets.each do |store_leaflet|
            allStoresLeafletsIds << store_leaflet.id
          end
        end
        puts "\nGOING TO NEXT CITY NOW"
      end
      puts "\nGOING TO NEXT STATE"
    end
    return allStoresIds, allStoresLeafletsIds
  end
  
  def get_leaflets(store_id, store_zipcode, store_name)
    leaflets = []
    store_leaflet_url = "http://flyers.tuesdaymorning.com/flyers/tuesdaymorning?type=2&auto_store=true&force_store_selection=true&postal_code=#{store_zipcode}&filler=&is_postal_entry=true#!/flyers/tuesdaymorning-onlinead?flyer_run_id=117534"
    sleep(2)
    mechanize = Mechanize.new
    mechanize.retry_change_requests=true
    mechanize.idle_timeout=4    
    store_leaflet_page = mechanize.get(store_leaflet_url)
    puts "GOT LEAFLETS PAGE AT #{store_leaflet_url}"
    leaflet_pdf_url = store_leaflet_page.search("//div[contains(@class, 'flatsheettopbar-pdf')]
                                                  /@data-href")[0].value
    leaflet_dates = store_leaflet_page.search("//div[contains(@class, 'flatsheettopbar-menu-item-title')]
                                              //p[contains(text(), 'Valid')]")[0].text
    leaflet_start_date = Time.parse(leaflet_dates.split("-")[0].sub("Valid", "").strip)
    leaflet_end_date = Time.parse(leaflet_dates.split("-")[1].strip)
    store_leaflet_name = "#{store_name}_leaflet"
    leaflet = PQSDK::Leaflet.find leaflet_pdf_url
    if leaflet.nil?
      leaflet = PQSDK::Leaflet.new
      leaflet.name = store_leaflet_name
      leaflet.url = leaflet_pdf_url
      leaflet.start_date = leaflet_start_date
      leaflet.end_date = leaflet_end_date
    end
    if !leaflet.store_ids.include?(store_id)
      leaflet.store_ids << store_id
    end
    #leaflet.save 
    puts "************************LEAFLET***"
    puts leaflet.name
    puts leaflet.url
    puts leaflet.store_ids
    puts leaflet.start_date
    puts leaflet.end_date
    puts "*****"
    leaflets << leaflet
    leaflets
  end
  
  
  def parse_hours(hours_rows)
    week_hours = []
    hours_rows.each do |hour_row|
      store_hour_node = hour_row.search(".//meta[@itemprop='openingHours']/@content")[0]
      if store_hour_node
        store_hour = store_hour_node.value.downcase 
      else
        if hour_row["class"].include?("closed")
          store_hour = hour_row.text.strip.downcase
        end
      end
      puts "GOT #{store_hour} TO PARSE NOW"
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      days = store_hour[0..breakpoint-1].split("-")
      days = store_hour[0..breakpoint-1].split("–") if store_hour[0..breakpoint-1].include?("–")
      days = days.map{|d| d.strip}
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = open_hours.split unless open_hours.include?("-") || open_hours
      .include?("to") || open_hours.include?("–") || open_hours.include?("-")
      hours = hours.map{|h| h.strip.gsub(" ", "")}
      open_am_pm = get_am_pm(hours[0]) if !(hours[0].include?("closed")  || !hours[0].match(/\d+/))
      close_am_pm = get_am_pm(hours[1]) if !(hours[1].include?("closed")  || !hours[1].match(/\d+/))
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
            formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
            formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
            formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")        
            day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = "#{hours[0]}" if open_am_pm && open_am_pm.eql?("am")
        formatted_time_open_pm ="#{hours[0]}" if open_am_pm && open_am_pm.eql?("pm")
        formatted_time_close_am = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("am")
        formatted_time_close_pm = "#{hours[1]}" if close_am_pm && close_am_pm.eql?("pm")
        day_hours["open_am"] = formatted_time_open_am.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(":")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end
  
  def get_day_index(day)
    if day.include?("mo")
      index = 0
    elsif day.include?("tu")
      index = 1
    elsif day.include?("we")
      index = 2
    elsif day.include?("th")
      index = 3  
    elsif day.include?("fr")
      index = 4      
    elsif day.include?("sa")
      index = 5     
    elsif day.include?("su")
      index = 6     
    end
    index
  end
  
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '413ee9702a6c88acdd7bc727ee0cebe9b29dd83796cdb5e389131f7c2e2341f9'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES AND THE LEAFLETS"
    store_ids, leaflet_ids = get_stores("http://stores.tuesdaymorning.com")
    puts "JUST ENDED THE SITE DATA"
  end
end

#class Mechanize::HTTP::Agent
#  MAX_RESET_RETRIES = 10
#
#  # We need to replace the core Mechanize HTTP method:
#  #
#  #   Mechanize::HTTP::Agent#fetch
#  #
#  # with a wrapper that handles the infamous "too many connection resets"
#  # Mechanize bug that is described here:
#  #
#  #   https://github.com/sparklemotion/mechanize/issues/123
#  #
#  # The wrapper shuts down the persistent HTTP connection when it fails with
#  # this error, and simply tries again. In practice, this only ever needs to
#  # be retried once, but I am going to let it retry a few times
#  # (MAX_RESET_RETRIES), just in case.
#  #
#  def fetch_with_retry(
#    uri,
#    method    = :get,
#    headers   = {},
#    params    = [],
#    referer   = current_page,
#    redirects = 0
#  )
#    action      = "#{method.to_s.upcase} #{uri.to_s}"
#    retry_count = 0
#
#    begin
#      fetch_without_retry(uri, method, headers, params, referer, redirects)
#    rescue Net::HTTP::Persistent::Error => e
#      # Pass on any other type of error.
#      raise unless e.message =~ /too many connection resets/
#
#      # Pass on the error if we've tried too many times.
#      if retry_count >= MAX_RESET_RETRIES
#        puts "**** WARN: Mechanize retried connection reset #{MAX_RESET_RETRIES} times and never succeeded: #{action}"
#        raise
#      end
#
#      # Otherwise, shutdown the persistent HTTP connection and try again.
#      puts "**** WARN: Mechanize retrying connection reset error: #{action}"
#      retry_count += 1
#      self.http.shutdown
#      retry
#    end
#  end
#
#  # Alias so #fetch actually uses our new #fetch_with_retry to wrap the
#  # old one aliased as #fetch_without_retry.
#  alias_method :fetch_without_retry, :fetch
#  alias_method :fetch, :fetch_with_retry
#end
