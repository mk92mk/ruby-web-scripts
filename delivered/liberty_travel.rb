#060843bcf92b333b4191c4ef7a9d7a94859404b387d8b6f258deafe03ae8c736 FOR LIBERTY TRAVEL
require 'pqsdk'
require 'mechanize'
require "resolv-replace.rb"
require 'uri'
require 'json'
require 'byebug'


class LibertyTravel
  
  def get_stores
    allStoresIds = []
    base_url = "http://www.libertytravel.com"
    mechanize = Mechanize.new
    stores_list_page = mechanize.get("http://www.libertytravel.com/stores")
    puts "GOT STORES DIRECTORIES LIST"
    stores_hrefs = stores_list_page.search("//a[@class='storeListUrl']/@href")
    sleep(1)
    stores_hrefs.each do |store_href|
      store_details_url = "#{base_url}#{store_href.value}" unless store_href.value.start_with?("http")
      store_details_url = store_href.value if store_href.value.start_with?("http")
      mechanize = Mechanize.new
      store_details_page = mechanize.get(store_details_url)
#      store_details_page = mechanize.get("http://www.libertytravel.com/stores/liberty-travel-woodbridge")
      puts "GOT STORE DETAILS PAGE AT #{store_details_url}"
      store_name = store_details_page.search("//h1[@id='page-title']")[0].text
      store_address = store_details_page.search("//div[@class='thoroughfare']")[0].text.strip
      store_city = store_details_page.search("//span[@class='locality']")[0].text.strip
      store_zipcode = store_details_page.search("//span[@class='postal-code']")[0].text.strip
      store_phone = store_details_page.search("//span[@itemprop='telephone left']")[0].text.strip
      store_latitude = store_details_page.search("//meta[@itemprop='latitude']/@content")[0].value
      store_longitude = store_details_page.search("//meta[@itemprop='longitude']/@content")[0].value
      store_hours = store_details_page.search("//span[@class='bold left'][contains(text(),
                                               'Business Hours')]//following-sibling::div
                                              //div[@class='field-item even']")[0].text.strip
      opening_hours = parse_hours(store_hours)
      store = PQSDK::Store.find store_address, store_zipcode
      if store.nil?
        store = PQSDK::Store.new
        store.name = store_name
        store.address = store_address
        store.zipcode = store_zipcode
        store.city = store_city
        store.latitude = store_latitude
        store.longitude = store_longitude
        store.phone = store_phone
        store.origin = store_details_url
      end
      store.opening_hours = opening_hours
      #store.save
      puts  "*************STORE*********************"
      puts  "STORE NAME IS #{store_name}"
      puts  "STORE CITY IS #{store_city}"
      puts  "STORE ZIP CODE IS #{store_zipcode}"
      puts  "STORE ADDRESS IS #{store_address}"
      puts  "STORE LONGITUDE #{store_longitude}"
      puts  "STORE LATITUDE #{store_latitude}"
      puts  "STORE PHONE #{store_phone}"
      puts  "STORE HOURS #{opening_hours}"
      puts  "STORE URL #{store_details_url}"
      puts "******"
      allStoresIds << store.id
      sleep(3)
    end
    allStoresIds
  end
  
  def parse_hours(hours_text)
    store_hours = []
    week_hours = []
    hours_text = hours_text.downcase.strip
    puts "GOT #{hours_text} TO PARSE"
    temp_store_hours = hours_text.split(",")
    temp_store_hours.each_with_index do |temp_hour, index|
      next unless /\d+/.match(temp_hour) || temp_hour.include?("closed") || temp_store_hours[index+1]
      unless /\d+/.match(temp_hour) || temp_hour.include?("am") || temp_hour.include?("pm") ||
            temp_hour.include?("closed")
          temp_pair = ""
        next unless temp_store_hours[index+1]
        (index+1..temp_store_hours.count-1).each do |temp_next|
          if /\d+/.match(temp_store_hours[temp_next])
            temp_pair = temp_store_hours[temp_next]
            break
          end
        end
        first_digit = /\d+/.match(temp_pair).to_s
        breakpoint = temp_pair.index(first_digit)
        open_hours = temp_pair[breakpoint..-1]
        store_hours << "#{temp_hour.strip} #{open_hours.strip}"
      else
        store_hours << temp_hour
      end
    end
    store_hours.each do |store_hour|
      store_hour = store_hour.downcase
#      if store_hour.include?(",") && !(store_hour.include?("-"))
#        first_digit = /\d+/.match(store_hour).to_s
#        breakpoint = store_hour.index(first_digit)
#        temp_days = store_hour[0..breakpoint-1].split(",")
#        open_hours = store_hour[breakpoint..-1]
#        temp_days.each do |temp_day|
#          store_hours << "#{temp_day} #{open_hours}"
#        end
#        next
#      end
      first_digit = /\d+/.match(store_hour).to_s
      breakpoint = store_hour.index(first_digit)
      temp_days = store_hour[0..breakpoint-1]
      days = temp_days.split("-")  if temp_days.include?("-")
      days = temp_days.split("to") if temp_days.include?("to")
      days = temp_days.split("–") if temp_days.include?("–")
      days = temp_days.split.map{|d| d.strip.gsub("closed", "")} unless days
      days.delete("")
      open_hours = store_hour[breakpoint..-1]
      hours = open_hours.split("-")
      hours = open_hours.split(" - ") if open_hours.include?(" - ")
      hours = open_hours.split(" to ") if open_hours.include?(" to ")
      hours = open_hours.split("–") if open_hours.include?("–")
      hours = hours.map{|h| h.strip}
      return hours if hours.empty?
#      if !hours[0].include?("am") && !hours[0].include?("pm")
#        open_am_pm = get_am_pm(hours[0])
#      end
#      if hours[1] && !hours[1].include?("am") && !hours[1].include?("pm")
#        close_am_pm = get_am_pm(hours[1])
#      end
      if days.length > 1
        starting_day = days[0]
        ending_day = days[1]
        starting_day_index = get_day_index(starting_day)
        ending_day_index = get_day_index(ending_day)
        if starting_day_index - ending_day_index == 1
          (0..6).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end              
        else
          (starting_day_index..ending_day_index).each do |n|
            day_hours = {}
            day_hours["weekday"] = n
            formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
            formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
            formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1].include?("am")
            formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1].include?("pm")
            day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
            day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
            day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
            day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
            day_hours["closed"] = true if open_hours.include?("closed")
            week_hours << day_hours unless week_hours.find{|day| day["weekday"] == n}
          end
        end
      elsif days.length == 1
        day_index = get_day_index(days[0])
        day_hours = {}
        day_hours["weekday"] = day_index
        formatted_time_open_am = convert_hours_am(hours[0].sub("am", "")) if  hours[0].include?("am")
        formatted_time_open_pm = convert_hours_pm(hours[0].sub("pm", "")) if hours[0].include?("pm")
        formatted_time_close_am = convert_hours_am(hours[1].sub("am", "")) if hours[1] && hours[1]
                                                                                   .include?("am")
        formatted_time_close_pm = convert_hours_pm(hours[1].sub("pm", "")) if hours[1] && hours[1]
                                                                              .include?("pm")
        day_hours["open_am"] = formatted_time_open_am.strip.rjust(5, "0") if formatted_time_open_am
        day_hours["open_pm"] = formatted_time_open_pm.strip.rjust(5, "0") if formatted_time_open_pm
        day_hours["close_am"] = formatted_time_close_am.strip.rjust(5, "0") if formatted_time_close_am
        day_hours["close_pm"] = formatted_time_close_pm.strip.rjust(5, "0") if formatted_time_close_pm
        day_hours["closed"] = true if open_hours.include?("closed")
        week_hours << day_hours unless week_hours.find{|day| day["weekday"] == day_index}
      end
    end
    week_hours
  end
  
  def get_day_index(day)
    if day.include?("mon") || day.include?("m")
      index = 0
    elsif day.include?("tue") || day.include?("tu") || day.strip.eql?("t")
      index = 1
    elsif day.include?("wed") || day.include?("w")
      index = 2
    elsif day.include?("thu") || day.include?("th")
      index = 3  
    elsif day.include?("fri") || day.include?("f")
      index = 4      
    elsif day.include?("sat") || day.include?("sa")
      index = 5     
    elsif day.include?("sun") || day.include?("su")
      index = 6     
    end
    index
  end  
  
  def convert_hours_am(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour.eql?("12")
      formatted_hour = times.include?(":") ? "00" : "00:00"
      time_split[0] = formatted_hour
      formatted_time = time_split.join(":")
      return formatted_time
    elsif times.include?(":")
      return times
    else
      return "#{times}:00"
    end
  end
  
  def convert_hours_pm(times)
    time_split = times.split(":")
    hour = time_split[0].to_i
    if hour == 12
      formatted_hour = times.include?(":") ? "12" : "12:00"
    elsif hour == 1
      formatted_hour = times.include?(":") ? "13" : "13:00"
    elsif hour == 2
      formatted_hour = times.include?(":") ? "14" : "14:00"
    elsif hour == 3
      formatted_hour = times.include?(":") ? "15" : "15:00"
    elsif hour == 4
      formatted_hour = times.include?(":") ? "16" : "16:00"
    elsif hour == 5
      formatted_hour = times.include?(":") ? "17" : "17:00"
    elsif hour == 6
      formatted_hour = times.include?(":") ? "18" : "18:00"
    elsif hour == 7
      formatted_hour = times.include?(":") ? "19" : "19:00"
    elsif hour == 8
      formatted_hour = times.include?(":") ? "20" : "20:00"
    elsif hour == 9
      formatted_hour = times.include?(":") ? "21" : "21:00"
    elsif hour == 10
      formatted_hour = times.include?(":") ? "22" : "22:00"
    elsif hour == 11
      formatted_hour = times.include?(":") ? "23" : "23:00"  
    end
    time_split[0] = formatted_hour
    formatted_time = time_split.join(":")
    return formatted_time
  end
  
  def get_am_pm(times)
    am_pm = ""
    hours = times.split(".")[0].to_i
    if (hours >= 0 && hours <= 11)
      am_pm = "am"
    elsif (hours >= 12 && hours <= 23)
      am_pm = "pm"
    end
    am_pm
  end

  def get_leaflets(store_ids)
    allStoresLeafletsIds = []
    mechanize = Mechanize.new
    leaflet_count_page = mechanize.get("https://issuu.com/query?pageSize=0&access=public&documentUsername=libertytravel&documentStates=A&action=issuu.documents.list_anonymous&format=json&_=1465238763697")
    puts "GOT LEAFLETS TOTAL PAGE"
    leaflets_count = JSON.parse(leaflet_count_page.body)["rsp"]["_content"]["result"]["totalCount"]
    sleep(1)
    mechanize = Mechanize.new
    leaflets_ids_page = mechanize.get("https://issuu.com/call/stream/api/profile/2/0/initial?ownerUsername=libertytravel&seed=4921&pageSize=#{leaflets_count}&format=json")
    puts "GOT LEAFLETS IDS PAGE"
    leaflets_ids_sect = JSON.parse(leaflets_ids_page.body)
    leaflets_pub_sects = leaflets_ids_sect["rsp"]["_content"]["stream"]
    leaflets_pub_sects.each do |leaflet_sect_id|
      leaflet_page_count = leaflet_sect_id["content"]["pageCount"]
      leaflet_pub_id = leaflet_sect_id["content"]["publicationId"]
      leaflet_rev_id = leaflet_sect_id["content"]["revisionId"]
      leaflet_pub_name = leaflet_sect_id["content"]["publicationName"]
      store_leaflet_url = "https://issuu.com/libertytravel/docs/#{leaflet_pub_name}"
      store_leaflet_name = leaflet_sect_id["content"]["title"]
      images_urls = []
      (1..leaflet_page_count).each do |page_number|
        image_url = "https://image.issuu.com/#{leaflet_rev_id}-#{leaflet_pub_id}/jpg/page_#{page_number}_thumb_large.jpg"
        images_urls << image_url
        puts "#{image_url} ADDED IN THE MAIN ARRAY"
      end
      leaflet = PQSDK::Leaflet.find store_leaflet_url
      if leaflet.nil?
        leaflet = PQSDK::Leaflet.new
        leaflet.name = store_leaflet_name
        leaflet.url = store_leaflet_url
        leaflet.image_urls = images_urls
        leaflet.store_ids = store_ids
      end
      # leaflet.save
      puts "************************LEAFLET***"
      puts leaflet.name
      puts leaflet.url
      puts leaflet.image_urls
      puts leaflet.store_ids
      puts "**********"
      allStoresLeafletsIds << leaflet.id
    end
    allStoresLeafletsIds
  end
  
  def run
    PQSDK::Token.reset!
    PQSDK::Settings.host = 'api.promoqui.eu'
    PQSDK::Settings.app_secret = '060843bcf92b333b4191c4ef7a9d7a94859404b387d8b6f258deafe03ae8c736'
    puts "IN RUN METHOD"
    puts "GOING TO COLLECT THE STORES"
    store_ids = get_stores
    puts "GOING TO COLLECT THE LEAFLETS"
    leaflet_ids = get_leaflets(store_ids)
    puts "JUST ENDED THE SITE DATA"
  end 
end
